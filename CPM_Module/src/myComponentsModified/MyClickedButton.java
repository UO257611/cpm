package myComponentsModified;

import javax.swing.JButton;
import javax.swing.JFrame;

import gui.MainWindow;
import logic.CartObject;
import logic.TypesOfBuy;

public class MyClickedButton extends JButton
{
	private TypesOfBuy clickedObject;
	private MainWindow mainWindow;
	private CartObject cartObject;
	
	public CartObject getCartObject() {
		return cartObject;
	}

	public void setCartObject(CartObject cartObject) {
		this.cartObject = cartObject;
	}

	public MyClickedButton(String name, MainWindow mainWindow)
	{
		super(name);
		this.mainWindow = mainWindow;
	}
	
	public MyClickedButton( MainWindow mainWindow)
	{
		super();
		this.mainWindow = mainWindow;
	}

	public TypesOfBuy getClicked() {
		return clickedObject;
	}

	public MainWindow getMainWindow() {
		return mainWindow;
	}
	public void setMainWindow(MainWindow mainWindow) {
		this.mainWindow = mainWindow;
	}
	public void setClicked(TypesOfBuy clickedObjectS) {
		this.clickedObject = clickedObjectS;
	}
	
	
	
	

}
