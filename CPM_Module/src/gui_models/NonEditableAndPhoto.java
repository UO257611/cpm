package gui_models;

import javax.swing.Icon;
import javax.swing.table.*;

public class NonEditableAndPhoto extends DefaultTableModel {
	private static final long serialVersionUID = 1L;
	private static final int ICONS_COLUMN = 1;


	public NonEditableAndPhoto() 
	{

		super(); 
 
	}
	
	    @Override
	    public Class<?> getColumnClass(int columnIndex) {
	        return columnIndex == ICONS_COLUMN ? Icon.class 
	                                           : super.getColumnClass(columnIndex);
	    }
	
	@Override
	public boolean isCellEditable(int row, int column) 
	{
		
        return false;
    
	}
	
}
