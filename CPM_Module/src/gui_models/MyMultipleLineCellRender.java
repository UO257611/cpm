package gui_models;

import java.awt.Component;

import javax.swing.JList;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.table.TableCellRenderer;

public class MyMultipleLineCellRender extends JTextArea implements TableCellRenderer {

public MyMultipleLineCellRender() {
setLineWrap(true);
setWrapStyleWord(true);
}

public Component getTableCellRendererComponent(JTable jTable, Object obj, boolean isSelected, boolean hasFocus, int row, int column) 
{
	setText((String)obj);
	int a = getText().length()*2/3;
	setEditable(false);
	int height_wanted = (int)getPreferredSize().getHeight()*2;
	
	
	if (a > jTable.getRowHeight(row))
			jTable.setRowHeight(row, a);
	
		
	
return this;
}
	}

