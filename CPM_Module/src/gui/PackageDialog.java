package gui;

import java.awt.EventQueue;

import javax.swing.JDialog;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Toolkit;
import java.awt.GridLayout;
import javax.swing.JSpinner;
import javax.swing.JCheckBox;
import javax.swing.border.TitledBorder;

import logic.Accommodation;
import logic.CartObject;
import logic.Package;
import logic.Park;
import logic.TypesOfBuy;

import javax.swing.UIManager;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.border.EmptyBorder;
import javax.swing.SpinnerDateModel;
import java.util.Date;
import java.util.Calendar;
import javax.swing.SpinnerNumberModel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import java.awt.FlowLayout;
import java.awt.Font;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.border.BevelBorder;
import javax.swing.SwingConstants;
import javax.swing.border.CompoundBorder;
import javax.swing.border.LineBorder;

public class PackageDialog extends JDialog implements MyCreatedDialogs
{
	private JPanel pnGeneralPane;
	private JPanel pnUserInfo;
	private JPanel pnPackageInfo;
	private JPanel pnControlButtons;
	private JPanel pnData;
	private JPanel pnButtonCheckTxtField;
	private JPanel pnAdults;
	private JPanel pnChildren;
	private JPanel pnDateArrival;
	private JPanel pnDateDeparture;
	private JSpinner spDateArriv;
	private JSpinner spDateDeparture;
	private JSpinner spAdults;
	private JSpinner spChildren;
	private JPanel pnTittle;
	private JPanel pnInfo;
	private JPanel pnAccomodation;
	private JPanel pnPark;
	private JPanel pnPhotoAccom;
	private JPanel pnPhotoPark;
	private JPanel pnAccomDescription;
	private JPanel pnParkDescription;
	private JLabel lblCalendar1;
	private JLabel lblCalendar2;
	private MainWindow mainWindow;
	private Package actualPackage;
	private JButton btnBack;
	private JButton btnContinueShopping;
	private JButton btnGoCartGo;
	private JPanel pnButton;
	private JPanel pnPrice;
	private JButton btnCalculate;
	private JTextField txtPrice;
	private JLabel lblPhotoAccomodation;
	private JLabel lblPhotoPark;
	private JScrollPane scAccomDescription;
	private JTextArea textAccomDescription;
	private JScrollPane scParkDescription;
	private JTextArea textParkDescription;
	private JLabel lblPackageInformationForm;
	private JLabel lblFormIcon;
	private JPanel pnBack;
	private JPanel pnContinue;
	private JLabel lblFinalPriceTo;
	private JDialog myDialog = this;
	private JPanel pnAddons;
	private JCheckBox chckbxBreakfast;
	private JLabel lblAdults;
	private JLabel lblKids;
	private JLabel lblArrivalDate;
	private JPanel pnArrivalDateSpiner;
	private JPanel pnDateDepartureSpinner;
	private JLabel lblDepartureDate;
	private JPanel pnTitleBorder;

	/**
	 * Create the dialog.
	 * @param theClickedObject 
	 * @param mainWindow 
	 * @wbp.parser.constructor
	 */
	public PackageDialog(MainWindow mainWindow, Package theClickedObject) {
		this.mainWindow = mainWindow;
		this.actualPackage = theClickedObject;
		setTitle(mainWindow.localizar("66"));
		setIconImage(Toolkit.getDefaultToolkit().getImage(PackageDialog.class.getResource("/img/roller-coaster.jpg")));
		setBounds(100, 100, 781, 515);
		getContentPane().add(getPnGeneralPane(), BorderLayout.CENTER);
		prepareDialog(false,"","",null,null);


	}
	
	public PackageDialog(MainWindow mainWindow, CartObject editinObject)
	{
		setTitle(mainWindow.localizar("48"));
		setIconImage(Toolkit.getDefaultToolkit().getImage(PackageDialog.class.getResource("/img/roller-coaster.jpg")));
		setBounds(100, 100, 781, 515);
		
		this.mainWindow = mainWindow;
		this.actualPackage = (Package)editinObject.getShoppedObject();
		
		getContentPane().add(getPnGeneralPane(), BorderLayout.CENTER);
		prepareDialog(true, editinObject.getAdults(), editinObject.getChilds(), editinObject.getSpInValue(), editinObject.getSpOutValue());
	}
	private void prepareDialog(boolean editing, String adults, String children, Date dateIn, Date dateOut) 
	{
		mainWindow.enableHelp(this, "packForm");
		if(!editing) 
		{
			spChildren.setValue(mainWindow.getChilds());
			spAdults.setValue(mainWindow.getAdults());
			spDateArriv.setValue(mainWindow.getCheckIn());
			syncDates((Date)spDateArriv.getValue());
			
			if( mainWindow.getAccomodationReader().searchAccomoadtion(actualPackage.getAccommodationCode()).getType().equals("HO"))
			{
				chckbxBreakfast.setEnabled(true);
			}
			else
			{
				chckbxBreakfast.setEnabled(false);
			}
			calculateMoney();
			
		}
		else
		{
			spChildren.setValue(Integer.valueOf(children));
			spAdults.setValue(Integer.valueOf(adults));
			spDateArriv.setValue(dateIn);
			syncDates((Date)spDateArriv.getValue());
			if( mainWindow.getAccomodationReader().searchAccomoadtion(actualPackage.getAccommodationCode()).getType().equals("HO"))
			{
				chckbxBreakfast.setEnabled(true);
			}
			else
			{
				chckbxBreakfast.setEnabled(false);
			}

			calculateMoney();
			
			
		}
		
		
	}
	
	private Date getFinalDate()
	{
		Calendar c = Calendar.getInstance();
		c.setTime((Date)spDateArriv.getValue());
		c.add(Calendar.DAY_OF_YEAR, actualPackage.getDays());
		return c.getTime();
	}

	private JPanel getPnGeneralPane() {
		if (pnGeneralPane == null) {
			pnGeneralPane = new JPanel();
			pnGeneralPane.setLayout(new BorderLayout(0, 0));
			pnGeneralPane.add(getPnUserInfo(), BorderLayout.WEST);
			pnGeneralPane.add(getPnPackageInfo(), BorderLayout.CENTER);
			pnGeneralPane.add(getPnControlButtons(), BorderLayout.SOUTH);
		}
		return pnGeneralPane;
	}
	private JPanel getPnUserInfo() {
		if (pnUserInfo == null) {
			pnUserInfo = new JPanel();
			pnUserInfo.setLayout(new GridLayout(0, 1, 0, 0));
			pnUserInfo.add(getPnData());
			pnUserInfo.add(getPnButtonTextField());
		}
		return pnUserInfo;
	}
	private JPanel getPnPackageInfo() {
		if (pnPackageInfo == null) {
			pnPackageInfo = new JPanel();
			pnPackageInfo.setLayout(new BorderLayout(0, 0));
			pnPackageInfo.add(getPnTittle(), BorderLayout.NORTH);
			pnPackageInfo.add(getPnInfo(), BorderLayout.CENTER);
		}
		return pnPackageInfo;
	}
	private JPanel getPnControlButtons() {
		if (pnControlButtons == null) {
			pnControlButtons = new JPanel();
			pnControlButtons.setLayout(new GridLayout(0, 2, 0, 5));
			pnControlButtons.add(getPnBack());
			pnControlButtons.add(getPnContinue());
		}
		return pnControlButtons;
	}
	private JPanel getPnData() {
		if (pnData == null) {
			pnData = new JPanel();
			pnData.setBorder(new EmptyBorder(30, 0, 0, 0));
			pnData.setLayout(new GridLayout(0, 2, 0, 0));
			pnData.add(getPnAdults());
			pnData.add(getPnChildren());
			pnData.add(getPnDateArrival());
			pnData.add(getPnDateDeparture());
		}
		return pnData;
	}
	private JPanel getPnButtonTextField() {
		if (pnButtonCheckTxtField == null) {
			pnButtonCheckTxtField = new JPanel();
			pnButtonCheckTxtField.setLayout(new GridLayout(3, 1, 0, 0));
			pnButtonCheckTxtField.add(getPnAddons());
			pnButtonCheckTxtField.add(getPnButton());
			pnButtonCheckTxtField.add(getPnPrice());
		}
		return pnButtonCheckTxtField;
	}
	private JPanel getPnAdults() {
		if (pnAdults == null) {
			pnAdults = new JPanel();
			pnAdults.setBorder(new CompoundBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)), new EmptyBorder(25, 0, 0, 0)));
			pnAdults.add(getLblAdults());
			pnAdults.add(getSpAdults());
		}
		return pnAdults;
	}
	private JPanel getPnChildren() {
		if (pnChildren == null) {
			pnChildren = new JPanel();
			pnChildren.setBorder(new CompoundBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)), new EmptyBorder(25, 0, 0, 0)));
			pnChildren.add(getLblKids());
			pnChildren.add(getSpChildren());
		}
		return pnChildren;
	}
	private JPanel getPnDateArrival() {
		if (pnDateArrival == null) {
			pnDateArrival = new JPanel();
			pnDateArrival.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			pnDateArrival.setLayout(new GridLayout(0, 1, 0, 0));
			pnDateArrival.add(getLblArrivalDate());
			pnDateArrival.add(getPnArrivalDateSpiner());
		}
		return pnDateArrival;
	}
	private JPanel getPnDateDeparture() {
		if (pnDateDeparture == null) {
			pnDateDeparture = new JPanel();
			pnDateDeparture.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			pnDateDeparture.setLayout(new GridLayout(0, 1, 0, 0));
			pnDateDeparture.add(getLblDepartureDate());
			pnDateDeparture.add(getPnDateDepartureSpinner());
		}
		return pnDateDeparture;
	}
	private JSpinner getSpDateArriv() {
		if (spDateArriv == null) {
			spDateArriv = new JSpinner();
			
			Date currentDate = mainWindow.getCurrentDay();
			
			Calendar c = Calendar.getInstance();
			c.setTime(currentDate);
			c.add(Calendar.DATE, -1);
			Date min = c.getTime();
			
			spDateArriv.setModel(new SpinnerDateModel(currentDate, min, null, Calendar.DAY_OF_YEAR));
			spDateArriv.setEditor(new JSpinner.DateEditor(spDateArriv, "dd/MM/yyyy"));
			spDateArriv.setPreferredSize(new Dimension(80, 20));
		}
		return spDateArriv;
	}
	private JSpinner getSpDateDeparture() {
		if (spDateDeparture == null) {
			spDateDeparture = new JSpinner();
			spDateDeparture.setEnabled(false);
			spDateDeparture.setToolTipText(mainWindow.localizar("212"));
			spDateDeparture.setModel(new SpinnerDateModel(new Date(1514070000000L), new Date(1514070000000L), null, Calendar.DAY_OF_YEAR));
			spDateDeparture.setEditor(new JSpinner.DateEditor(spDateDeparture, "dd/MM/yyyy"));
			spDateDeparture.setPreferredSize(new Dimension(80, 20));
		}
		return spDateDeparture;
	}
	private JSpinner getSpAdults() {
		if (spAdults == null) {
			spAdults = new JSpinner();
			spAdults.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));
			spAdults.setPreferredSize(new Dimension(50,20));
		}
		return spAdults;
	}
	private JSpinner getSpChildren() {
		if (spChildren == null) {
			spChildren = new JSpinner();
			spChildren.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));
			spChildren.setPreferredSize(new Dimension(50, 20));
		}
		return spChildren;
	}
	private JPanel getPnTittle() {
		if (pnTittle == null) {
			pnTittle = new JPanel();
			pnTittle.add(getPnTitleBorder());
		}
		return pnTittle;
	}
	private JPanel getPnInfo() {
		if (pnInfo == null) {
			pnInfo = new JPanel();
			pnInfo.setBorder(new EmptyBorder(10, 0, 10, 0));
			pnInfo.setLayout(new GridLayout(0, 1, 10, 10));
			pnInfo.add(getPnAccomodation());
			pnInfo.add(getPnPark());
		}
		return pnInfo;
	}
	private JPanel getPnAccomodation() {
		if (pnAccomodation == null) {
			pnAccomodation = new JPanel();
			pnAccomodation.setBorder(new EmptyBorder(0, 10, 0, 10));
			pnAccomodation.setLayout(new GridLayout(1, 2, 10, 0));
			pnAccomodation.add(getPnPhotoAccom());
			pnAccomodation.add(getPnAccomDescription());
		}
		return pnAccomodation;
	}
	private JPanel getPnPark() {
		if (pnPark == null) {
			pnPark = new JPanel();
			pnPark.setBorder(new EmptyBorder(0, 10, 0, 10));
			pnPark.setLayout(new GridLayout(1, 2, 10, 0));
			pnPark.add(getPnPhotoPark());
			pnPark.add(getPnParkDescription());
		}
		return pnPark;
	}
	private JPanel getPnPhotoAccom() {
		if (pnPhotoAccom == null) {
			pnPhotoAccom = new JPanel();
			pnPhotoAccom.setLayout(new GridLayout(0, 1, 0, 0));
			pnPhotoAccom.add(getLblPhotoAccomodation());
		}
		return pnPhotoAccom;
	}
	private JPanel getPnPhotoPark() {
		if (pnPhotoPark == null) {
			pnPhotoPark = new JPanel();
			pnPhotoPark.setLayout(new GridLayout(0, 1, 0, 0));
			pnPhotoPark.add(getLblPhotoPark());
		}
		return pnPhotoPark;
	}
	private JPanel getPnAccomDescription() {
		if (pnAccomDescription == null) {
			pnAccomDescription = new JPanel();
			pnAccomDescription.setLayout(new BorderLayout(0, 0));
			pnAccomDescription.add(getScAccomDescription());
		}
		return pnAccomDescription;
	}
	private JPanel getPnParkDescription() {
		if (pnParkDescription == null) {
			pnParkDescription = new JPanel();
			pnParkDescription.setLayout(new BorderLayout(0, 0));
			pnParkDescription.add(getScParkDescription());
		}
		return pnParkDescription;
	}
	private JLabel getLblCalendar1() {
		if (lblCalendar1 == null) {
			lblCalendar1 = new JLabel("");
			lblCalendar1.setPreferredSize(new Dimension(30, 30));
			mainWindow.scaledImage(lblCalendar1,"/img/calendar.jpg" );
			
		}
		return lblCalendar1;
	}
	private JLabel getLblCalendar2() {
		if (lblCalendar2 == null) {
			lblCalendar2 = new JLabel("");
			lblCalendar2.setPreferredSize(new Dimension(30, 30));
			mainWindow.scaledImage(lblCalendar2,"/img/calendar.jpg" );
		}
		return lblCalendar2;
	}
	private JButton getBtnBack() {
		if (btnBack == null) {
			btnBack = new JButton(mainWindow.localizar("141"));
			btnBack.setMnemonic(mainWindow.localizar("180").charAt(0));
			btnBack.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) 
				{
					dispose();
				}
			});
		}
		return btnBack;
	}
	private JButton getBtnContinueShopping() {
		if (btnContinueShopping == null) {
			btnContinueShopping = new JButton(mainWindow.localizar("44"));
			btnContinueShopping.setMnemonic(mainWindow.localizar("181").charAt(0));
			try {
				btnContinueShopping.addActionListener(mainWindow.getMyCartButtonListner("continue", this));
			} catch (Exception e) {
				System.out.println(mainWindow.localizar("142"));
			}
			
		}
		return btnContinueShopping;
	}
	
	
	private void syncDates(Date in)
	{
		Calendar c = Calendar.getInstance();
		
		c.setTime(in);
		c.add(Calendar.DATE, actualPackage.getDays());
		
		spDateDeparture.setValue(c.getTime());
		
	}
	
	
	private JButton getBtnGoCartGo() {
		if (btnGoCartGo == null) {
			btnGoCartGo = new JButton(mainWindow.localizar("45"));
			btnGoCartGo.setMnemonic(mainWindow.localizar("182"
					+ "").charAt(0));
			try {
				btnGoCartGo.addActionListener(mainWindow.getMyCartButtonListner("cart", this));
			} catch (Exception e) {
				System.out.println(mainWindow.localizar("142"));
			}
		}
		return btnGoCartGo;
	}
	private JPanel getPnButton() {
		if (pnButton == null) {
			pnButton = new JPanel();
			pnButton.setBorder(new EmptyBorder(15, 0, 0, 0));
			FlowLayout flowLayout = (FlowLayout) pnButton.getLayout();
			pnButton.add(getBtnCalculate());
		}
		return pnButton;
	}
	private JPanel getPnPrice() {
		if (pnPrice == null) {
			pnPrice = new JPanel();
			pnPrice.setBorder(new EmptyBorder(15, 0, 0, 0));
			pnPrice.add(getLblFinalPriceTo());
			pnPrice.add(getTxtPrice());
		}
		return pnPrice;
	}
	private JButton getBtnCalculate() {
		if (btnCalculate == null) {
			btnCalculate = new JButton(mainWindow.localizar("41"));
			btnCalculate.setToolTipText(mainWindow.localizar("210"));
			btnCalculate.setMnemonic(mainWindow.localizar("179").charAt(0));
			btnCalculate.setFont(new Font("Tahoma", Font.PLAIN, 14));
			btnCalculate.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) 
				{
					checkButton();
				}

				
			});
		}
		return btnCalculate;
	}
	private JTextField getTxtPrice() {
		if (txtPrice == null) {
			txtPrice = new JTextField();
			txtPrice.setPreferredSize(new Dimension(400, 30));
			txtPrice.setEditable(false);
			txtPrice.setColumns(10);
		}
		return txtPrice;
	}
	
	private void checkButton()
	{
		if(spAdults.getValue().toString().equals("0") && spChildren.getValue().toString().equals("0"))
		{
			btnContinueShopping.setEnabled(false);
			btnGoCartGo.setEnabled(false);
			JOptionPane.showMessageDialog( myDialog , mainWindow.localizar("143"));
			
		}
		else
		{
			calculateMoney();
			btnContinueShopping.setEnabled(true);
			btnGoCartGo.setEnabled(true);
		}
	}
	
	private void calculateMoney() {
		
		int childs = Integer.valueOf(spChildren.getValue().toString());
		int adults = Integer.valueOf(spAdults.getValue().toString());
		double finalPrice = actualPackage.getFinalPrice(adults, childs);
		
		int nights = actualPackage.getDays();
		Calendar c = Calendar.getInstance();
		c.setTime((Date)spDateArriv.getValue());
		c.add(Calendar.DATE, nights);
		spDateDeparture.setValue(c.getTime());
		
		if(chckbxBreakfast.isSelected())
		{
			txtPrice.setText(String.valueOf(finalPrice+((finalPrice*10)/100)+mainWindow.localizar("203")));
		}
		else
		{
			txtPrice.setText(String.valueOf(finalPrice)+mainWindow.localizar("203"));
		}
	}
	
	private JLabel getLblPhotoAccomodation() {
		if (lblPhotoAccomodation == null) {
			lblPhotoAccomodation = new JLabel("");

			lblPhotoAccomodation.setPreferredSize(new Dimension(300,305));
			
			mainWindow.scaledImage(lblPhotoAccomodation, getAccomodation().getPhotoPath());
		}
		return lblPhotoAccomodation;
	}
	
	private Park getPark()
	{
		return mainWindow.getParkReader().searchPark(actualPackage.getThemeParkCode());
	}
	private JLabel getLblPhotoPark() {
		if (lblPhotoPark == null) {
			lblPhotoPark = new JLabel("");
			lblPhotoPark.setPreferredSize(new Dimension(300,305));
			
			mainWindow.scaledImage(lblPhotoPark, getPark().getPhotoPath());
	
		}
		return lblPhotoPark;
	}
	private JScrollPane getScAccomDescription() {
		if (scAccomDescription == null) {
			scAccomDescription = new JScrollPane();
			scAccomDescription.setViewportView(getTextAccomDescription());
		}
		return scAccomDescription;
	}
	private JTextArea getTextAccomDescription() {
		if (textAccomDescription == null) {
			textAccomDescription = new JTextArea();
			textAccomDescription.setLineWrap(true);
			textAccomDescription.setWrapStyleWord(true);
			textAccomDescription.setText(getAccomodation().getDataDialogPrepared(mainWindow.getTicketsReader(), mainWindow.getParkReader(),mainWindow.getMyLocale()));
		}
		return textAccomDescription;
	}
	
	
	private Accommodation getAccomodation ()
	{
		return mainWindow.getAccomodationReader().searchAccomoadtion(actualPackage.getAccommodationCode());
	}
	private JScrollPane getScParkDescription() {
		if (scParkDescription == null) {
			scParkDescription = new JScrollPane();
			scParkDescription.setViewportView(getTextParkDescription());
		}
		return scParkDescription;
	}
	private JTextArea getTextParkDescription() {
		if (textParkDescription == null) {
			textParkDescription = new JTextArea();
			textParkDescription.setWrapStyleWord(true);
			textParkDescription.setLineWrap(true);
			textParkDescription.setText(getPark().getDescription());
		}
		return textParkDescription;
	}
	private JLabel getLblPackageInformationForm() {
		if (lblPackageInformationForm == null) {
			lblPackageInformationForm = new JLabel(mainWindow.localizar("48"));
			lblPackageInformationForm.setFont(new Font("Tahoma", Font.BOLD, 24));
		}
		return lblPackageInformationForm;
	}
	private JLabel getLblFormIcon() {
		if (lblFormIcon == null) {
			lblFormIcon = new JLabel("");
			lblFormIcon.setPreferredSize(new Dimension(30, 30));
			mainWindow.scaledImage(lblFormIcon, "/img/form.jpg");
		}
		return lblFormIcon;
	}
	private JPanel getPnBack() {
		if (pnBack == null) {
			pnBack = new JPanel();
			pnBack.setBorder(null);
			FlowLayout flowLayout = (FlowLayout) pnBack.getLayout();
			flowLayout.setHgap(35);
			flowLayout.setAlignment(FlowLayout.LEFT);
			pnBack.add(getBtnBack());
		}
		return pnBack;
	}
	private JPanel getPnContinue() {
		if (pnContinue == null) {
			pnContinue = new JPanel();
			pnContinue.setBorder(null);
			FlowLayout flowLayout = (FlowLayout) pnContinue.getLayout();
			flowLayout.setHgap(0);
			flowLayout.setAlignment(FlowLayout.LEFT);
			pnContinue.add(getBtnContinueShopping());
			pnContinue.add(getBtnGoCartGo());
		}
		return pnContinue;
	}
	private JLabel getLblFinalPriceTo() {
		if (lblFinalPriceTo == null) {
			lblFinalPriceTo = new JLabel(mainWindow.localizar("42"));
			lblFinalPriceTo.setFont(new Font("Tahoma", Font.PLAIN, 17));
		}
		return lblFinalPriceTo;
	}

	@Override
	public String getAdults() {
		
		return spAdults.getValue().toString();
	}

	@Override
	public String getChilds() {
		
		return spChildren.getValue().toString();
	}

	@Override
	public String getCheckIn() {

		Date in =(Date)spDateArriv.getValue();
		 
		Calendar c = Calendar.getInstance();
		c.setTime(in);
		String x = ""+c.get(Calendar.DATE)+"/"+(c.get(Calendar.MONTH)+1)+"/"+c.get(Calendar.YEAR);
		return x;
	}

	@Override
	public String getCheckOut() {
		

		Date in =(Date)spDateDeparture.getValue();
		 
		Calendar c = Calendar.getInstance();
		c.setTime(in);
		String x = ""+c.get(Calendar.DATE)+"/"+(c.get(Calendar.MONTH)+1)+"/"+c.get(Calendar.YEAR);
		return x;
	}

	@Override
	public boolean isBreakFast() {
		
		return false;
	}

	@Override
	public String getTypeOfBuy() {
		
		return "Package";
	}

	@Override
	public String getPrice() {
		
		return txtPrice.getText();
	}

	@Override
	public TypesOfBuy getTheCurrentObject() {
		
		return actualPackage;
	}

	@Override
	public String getDescription() {
		
		StringBuilder info = new StringBuilder();
		
		info.append(mainWindow.localizar("149")+" "+ getAccomodation().getFacilityName()+".\n");
		info.append(mainWindow.localizar("150")+" "+ getPark().getNamePark()+".\n");
		return info.toString();
		
	}

	@Override
	public CartObject createCartObject() {
		return new CartObject(getAdults(),getChilds(), getTheCurrentObject(), getCheckIn(),
				getCheckOut(), getPrice(), getDescription(), getTypeOfBuy(), getSpInActualValue(), getSpOutActualValue(), false);
	}

	@Override
	public void closeDialog() 
	{
		dispose();
		
	}

	@Override
	public Date getSpInActualValue() {
		
		return (Date)spDateArriv.getValue();
	}

	@Override
	public Date getSpOutActualValue() {
		
		return (Date)spDateDeparture.getValue();
	}
	private JPanel getPnAddons() {
		if (pnAddons == null) {
			pnAddons = new JPanel();
			pnAddons.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), mainWindow.localizar("14"), TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
			pnAddons.setLayout(new GridLayout(0, 1, 0, 0));
			pnAddons.add(getChckbxBreakfast());
		}
		return pnAddons;
	}
	private JCheckBox getChckbxBreakfast() {
		if (chckbxBreakfast == null) {
			chckbxBreakfast = new JCheckBox(mainWindow.localizar("15"));
			chckbxBreakfast.setMnemonic(mainWindow.localizar("196").charAt(0));
			chckbxBreakfast.setHorizontalAlignment(SwingConstants.CENTER);
			chckbxBreakfast.setFont(new Font("Tahoma", Font.PLAIN, 14));
		}
		return chckbxBreakfast;
	}
	private JLabel getLblAdults() {
		if (lblAdults == null) {
			lblAdults = new JLabel(mainWindow.localizar("12"));
			lblAdults.setDisplayedMnemonic(mainWindow.localizar("166").charAt(0));
			lblAdults.setLabelFor(getSpAdults());
			lblAdults.setFont(new Font("Tahoma", Font.PLAIN, 14));
		}
		return lblAdults;
	}
	private JLabel getLblKids() {
		if (lblKids == null) {
			lblKids = new JLabel(mainWindow.localizar("13"));
			lblKids.setDisplayedMnemonic(mainWindow.localizar("167").charAt(0));
			lblKids.setLabelFor(getSpChildren());
			lblKids.setFont(new Font("Tahoma", Font.PLAIN, 14));
		}
		return lblKids;
	}
	private JLabel getLblArrivalDate() {
		if (lblArrivalDate == null) {
			lblArrivalDate = new JLabel(mainWindow.localizar("37"));
			lblArrivalDate.setDisplayedMnemonic(mainWindow.localizar("176").charAt(0));
			lblArrivalDate.setLabelFor(getSpDateArriv());
			lblArrivalDate.setFont(new Font("Tahoma", Font.PLAIN, 14));
		}
		return lblArrivalDate;
	}
	private JPanel getPnArrivalDateSpiner() {
		if (pnArrivalDateSpiner == null) {
			pnArrivalDateSpiner = new JPanel();
			pnArrivalDateSpiner.add(getSpDateArriv());
			pnArrivalDateSpiner.add(getLblCalendar1());
		}
		return pnArrivalDateSpiner;
	}
	private JPanel getPnDateDepartureSpinner() {
		if (pnDateDepartureSpinner == null) {
			pnDateDepartureSpinner = new JPanel();
			pnDateDepartureSpinner.add(getSpDateDeparture());
			pnDateDepartureSpinner.add(getLblCalendar2());
		}
		return pnDateDepartureSpinner;
	}
	private JLabel getLblDepartureDate() {
		if (lblDepartureDate == null) {
			lblDepartureDate = new JLabel(mainWindow.localizar("46"));
			lblDepartureDate.setFont(new Font("Tahoma", Font.PLAIN, 14));
		}
		return lblDepartureDate;
	}
	private JPanel getPnTitleBorder() {
		if (pnTitleBorder == null) {
			pnTitleBorder = new JPanel();
			pnTitleBorder.setBorder(new CompoundBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null), new LineBorder(new Color(0, 0, 0), 4, true)));
			pnTitleBorder.add(getLblFormIcon());
			pnTitleBorder.add(getLblPackageInformationForm());
		}
		return pnTitleBorder;
	}
}
