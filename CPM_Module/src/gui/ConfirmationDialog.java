package gui;

import java.awt.EventQueue;

import javax.swing.JDialog;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.awt.FlowLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.border.LineBorder;
import java.awt.Color;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JCheckBox;
import javax.swing.JButton;
import java.awt.Dimension;
import javax.swing.border.EmptyBorder;
import javax.swing.border.CompoundBorder;
import javax.swing.border.BevelBorder;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JMenuBar;

public class ConfirmationDialog extends JDialog {

	
	private MainWindow mainWindow;
	private JPanel pnGeneral;
	private JPanel pnTitle;
	private JPanel pnUserInfo;
	private JPanel pnControlButtons;
	private JPanel pnBack;
	private JPanel pnProcess;
	private JPanel pnName;
	private JPanel pnSurname;
	private JPanel pnId;
	private JPanel pnCreditCard;
	private JPanel pnRobot;
	private JPanel PnBorderTitle;
	private JLabel lblIdentification;
	private JLabel lblName;
	private JPanel pnNameLabel;
	private JPanel pnNameTextField;
	private JTextField txtName;
	private JPanel pnSurnameLabel;
	private JPanel pnSurnameTextField;
	private JLabel lblSurname;
	private JTextField txtSurname;
	private JPanel pnIdLabel;
	private JPanel pnIdTextField;
	private JLabel lblId;
	private JTextField txtId;
	private JPanel pnCreditCardLabel;
	private JPanel pnCreditCardTextField;
	private JLabel lblCreditCard;
	private JTextField txtCreditCard;
	private JPanel pnBorderRobot;
	private JCheckBox chckbxImNotA;
	private JButton btnReturn;
	private JButton btnProcess;
	private JLabel lblIDIcon;
	private JMenuBar menuBar;

	/**
	 * Create the dialog.
	 */
	public ConfirmationDialog(MainWindow mainWindow) 
	{
		setTitle(mainWindow.localizar("69"));
		this.mainWindow = mainWindow;
		setBounds(100, 100, 739, 488);
		getContentPane().add(getPnGeneral(), BorderLayout.CENTER);

		mainWindow.enableHelp(this,"confirm");

	}

	private JPanel getPnGeneral() {
		if (pnGeneral == null) {
			pnGeneral = new JPanel();
			pnGeneral.setLayout(new BorderLayout(0, 0));
			pnGeneral.add(getPnTitle(), BorderLayout.NORTH);
			pnGeneral.add(getPnUserInfo(), BorderLayout.CENTER);
			pnGeneral.add(getPnControlButtons(), BorderLayout.SOUTH);
			pnGeneral.add(getMenuBar(), BorderLayout.WEST);
		}
		return pnGeneral;
	}
	private JPanel getPnTitle() {
		if (pnTitle == null) {
			pnTitle = new JPanel();
			pnTitle.add(getPnBorderTitle());
		}
		return pnTitle;
	}
	private JPanel getPnUserInfo() {
		if (pnUserInfo == null) {
			pnUserInfo = new JPanel();
			pnUserInfo.setBorder(new EmptyBorder(5, 0, 5, 0));
			pnUserInfo.setLayout(new GridLayout(5, 1, 7, 10));
			pnUserInfo.add(getPnName());
			pnUserInfo.add(getPnSurname());
			pnUserInfo.add(getPnId());
			pnUserInfo.add(getPnCreditCard());
			pnUserInfo.add(getPnRobot());
		}
		return pnUserInfo;
	}
	private JPanel getPnControlButtons() {
		if (pnControlButtons == null) {
			pnControlButtons = new JPanel();
			pnControlButtons.setLayout(new GridLayout(0, 2, 0, 0));
			pnControlButtons.add(getPnBack());
			pnControlButtons.add(getPnProcess());
		}
		return pnControlButtons;
	}
	private JPanel getPnBack() {
		if (pnBack == null) {
			pnBack = new JPanel();
			pnBack.setBorder(null);
			pnBack.add(getBtnReturn());
		}
		return pnBack;
	}
	private JPanel getPnProcess() {
		if (pnProcess == null) {
			pnProcess = new JPanel();
			pnProcess.setBorder(null);
			pnProcess.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
			pnProcess.add(getBtnProcess());
		}
		return pnProcess;
	}
	private JPanel getPnName() {
		if (pnName == null) {
			pnName = new JPanel();
			pnName.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
			pnName.setLayout(new GridLayout(0, 2, 10, 0));
			pnName.add(getPnNameLabel());
			pnName.add(getPnNameTextField());
		}
		return pnName;
	}
	private JPanel getPnSurname() {
		if (pnSurname == null) {
			pnSurname = new JPanel();
			pnSurname.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
			pnSurname.setLayout(new GridLayout(0, 2, 10, 0));
			pnSurname.add(getPnSurnameLabel());
			pnSurname.add(getPnSurnameTextField());
		}
		return pnSurname;
	}
	private JPanel getPnId() {
		if (pnId == null) {
			pnId = new JPanel();
			pnId.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
			pnId.setLayout(new GridLayout(0, 2, 10, 0));
			pnId.add(getPnIdLabel());
			pnId.add(getPnIdTextField());
		}
		return pnId;
	}
	private JPanel getPnCreditCard() {
		if (pnCreditCard == null) {
			pnCreditCard = new JPanel();
			pnCreditCard.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
			pnCreditCard.setLayout(new GridLayout(0, 2, 10, 0));
			pnCreditCard.add(getPnCreditCardLabel());
			pnCreditCard.add(getPnCreditCardTextField());
		}
		return pnCreditCard;
	}
	private JPanel getPnRobot() {
		if (pnRobot == null) {
			pnRobot = new JPanel();
			FlowLayout flowLayout = (FlowLayout) pnRobot.getLayout();
			pnRobot.add(getPnBorderRobot());
		}
		return pnRobot;
	}
	private JPanel getPnBorderTitle() {
		if (PnBorderTitle == null) {
			PnBorderTitle = new JPanel();
			PnBorderTitle.setBorder(new CompoundBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null), new LineBorder(new Color(0, 0, 0), 4)));
			PnBorderTitle.add(getLblIDIcon());
			PnBorderTitle.add(getLblIdentification());
		}
		return PnBorderTitle;
	}
	private JLabel getLblIdentification() {
		if (lblIdentification == null) {
			lblIdentification = new JLabel(mainWindow.localizar("57"));
			lblIdentification.setFont(new Font("Tahoma", Font.BOLD, 24));
		}
		return lblIdentification;
	}
	private JLabel getLblName() {
		if (lblName == null) {
			lblName = new JLabel(mainWindow.localizar("58"));
			lblName.setDisplayedMnemonic(mainWindow.localizar("186").charAt(0));
			lblName.setLabelFor(getTxtName());
			lblName.setFont(new Font("Tahoma", Font.PLAIN, 18));
			lblName.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return lblName;
	}
	private JPanel getPnNameLabel() {
		if (pnNameLabel == null) {
			pnNameLabel = new JPanel();
			pnNameLabel.setLayout(new GridLayout(0, 1, 0, 0));
			pnNameLabel.add(getLblName());
		}
		return pnNameLabel;
	}
	private JPanel getPnNameTextField() {
		if (pnNameTextField == null) {
			pnNameTextField = new JPanel();
			pnNameTextField.setLayout(new GridLayout(0, 1, 0, 0));
			pnNameTextField.add(getTxtName());
		}
		return pnNameTextField;
	}
	private JTextField getTxtName() {
		if (txtName == null) {
			txtName = new JTextField();
			txtName.setPreferredSize(new Dimension(100, 30));
			txtName.setColumns(10);
		}
		return txtName;
	}
	private JPanel getPnSurnameLabel() {
		if (pnSurnameLabel == null) {
			pnSurnameLabel = new JPanel();
			pnSurnameLabel.setLayout(new GridLayout(1, 0, 0, 0));
			pnSurnameLabel.add(getLblSurname());
		}
		return pnSurnameLabel;
	}
	private JPanel getPnSurnameTextField() {
		if (pnSurnameTextField == null) {
			pnSurnameTextField = new JPanel();
			pnSurnameTextField.setLayout(new GridLayout(0, 1, 0, 0));
			pnSurnameTextField.add(getTxtSurname());
		}
		return pnSurnameTextField;
	}
	private JLabel getLblSurname() {
		if (lblSurname == null) {
			lblSurname = new JLabel(mainWindow.localizar("59"));
			lblSurname.setDisplayedMnemonic(mainWindow.localizar("187").charAt(0));
			lblSurname.setFont(new Font("Tahoma", Font.PLAIN, 18));
			lblSurname.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return lblSurname;
	}
	private JTextField getTxtSurname() {
		if (txtSurname == null) {
			txtSurname = new JTextField();
			txtSurname.setColumns(10);
		}
		return txtSurname;
	}
	private JPanel getPnIdLabel() {
		if (pnIdLabel == null) {
			pnIdLabel = new JPanel();
			pnIdLabel.setLayout(new GridLayout(1, 0, 0, 0));
			pnIdLabel.add(getLblId());
		}
		return pnIdLabel;
	}
	private JPanel getPnIdTextField() {
		if (pnIdTextField == null) {
			pnIdTextField = new JPanel();
			pnIdTextField.setLayout(new GridLayout(0, 1, 0, 0));
			pnIdTextField.add(getTxtId());
		}
		return pnIdTextField;
	}
	private JLabel getLblId() {
		if (lblId == null) {
			lblId = new JLabel(mainWindow.localizar("60"));
			lblId.setDisplayedMnemonic(mainWindow.localizar("188").charAt(0));
			lblId.setFont(new Font("Tahoma", Font.PLAIN, 18));
			lblId.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return lblId;
	}
	private JTextField getTxtId() {
		if (txtId == null) {
			txtId = new JTextField();
			txtId.setColumns(10);
		}
		return txtId;
	}
	private JPanel getPnCreditCardLabel() {
		if (pnCreditCardLabel == null) {
			pnCreditCardLabel = new JPanel();
			pnCreditCardLabel.setLayout(new GridLayout(0, 1, 0, 0));
			pnCreditCardLabel.add(getLblCreditCard());
		}
		return pnCreditCardLabel;
	}
	private JPanel getPnCreditCardTextField() {
		if (pnCreditCardTextField == null) {
			pnCreditCardTextField = new JPanel();
			pnCreditCardTextField.setLayout(new GridLayout(0, 1, 0, 0));
			pnCreditCardTextField.add(getTxtCreditCard());
		}
		return pnCreditCardTextField;
	}
	private JLabel getLblCreditCard() {
		if (lblCreditCard == null) {
			lblCreditCard = new JLabel(mainWindow.localizar("61"));
			lblCreditCard.setDisplayedMnemonic(mainWindow.localizar("189").charAt(0));
			lblCreditCard.setHorizontalAlignment(SwingConstants.CENTER);
			lblCreditCard.setFont(new Font("Tahoma", Font.PLAIN, 18));
		}
		return lblCreditCard;
	}
	private JTextField getTxtCreditCard() {
		if (txtCreditCard == null) {
			txtCreditCard = new JTextField();
			txtCreditCard.setColumns(10);
		}
		return txtCreditCard;
	}
	private JPanel getPnBorderRobot() {
		if (pnBorderRobot == null) {
			pnBorderRobot = new JPanel();
			pnBorderRobot.setBorder(new LineBorder(new Color(0, 0, 0), 2, true));
			pnBorderRobot.add(getChckbxImNotA());
		}
		return pnBorderRobot;
	}
	private JCheckBox getChckbxImNotA() {
		if (chckbxImNotA == null) {
			chckbxImNotA = new JCheckBox(mainWindow.localizar("62"));
			chckbxImNotA.setMnemonic(mainWindow.localizar("191").charAt(0));
			chckbxImNotA.setFont(new Font("Tahoma", Font.PLAIN, 18));
		}
		return chckbxImNotA;
	}
	private JButton getBtnReturn() {
		if (btnReturn == null) {
			btnReturn = new JButton(mainWindow.localizar("63"));
			btnReturn.setMnemonic(mainWindow.localizar("190").charAt(0));
			btnReturn.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) 
				{
					dispose();
				}
			});
			btnReturn.setPreferredSize(new Dimension(100, 30));
		}
		return btnReturn;
	}
	private JButton getBtnProcess() {
		if (btnProcess == null) {
			btnProcess = new JButton(mainWindow.localizar("64"));
			btnProcess.setMnemonic(mainWindow.localizar("192").charAt(0));
			btnProcess.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) 
				{
					if(isAllFieldsCorrect())
					{
						ReceiptDialog rD = new ReceiptDialog(mainWindow,getConfirmationDialog());
						rD.setLocationRelativeTo(mainWindow);
						rD.setModal(true);
						rD.setVisible(true);
						
					}
				}
			});
			btnProcess.setPreferredSize(new Dimension(100, 30));
		}
		return btnProcess;
	}
	
	public ArrayList<String> getUserInfo()
	{
		ArrayList<String> info = new ArrayList<String>();
		info.add(txtName.getText());
		info.add(txtSurname.getText());
		info.add(txtId.getText());
		return info;
	}
	public ConfirmationDialog getConfirmationDialog()
	{
		return this;
	}
	
	private boolean checkNameSurname(JTextField field)
	{
		if(!field.getText().isEmpty())
		{
			
			Pattern myPattern = Pattern.compile("[0-9]+");
			Matcher theString = myPattern.matcher(field.getText());
			
			return !theString.find();
			
			
			
		}
		return false;
	}
	
	private boolean checkId()
	{
		if(!txtId.getText().isEmpty())
		{
			
			Pattern myPattern = Pattern.compile("[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][A-Z]");
			Matcher theString = myPattern.matcher(txtId.getText());
			
			return theString.matches();
			
		}
		
		return false;
	}
	
	private boolean checkCard()
	{
		if(!txtCreditCard.getText().isEmpty())
		{
			Pattern myPattern = Pattern.compile("[a-zA-Z]+");
			Matcher theString = myPattern.matcher(txtCreditCard.getText());
		
			return !theString.find();
		}
		return false;
	}
	
	
	private boolean isAllFieldsCorrect() 
	{
		
		if(checkNameSurname(txtName) && checkNameSurname(txtSurname) && checkId() 
				&& chckbxImNotA.isSelected() && checkCard())
		{
			return true;
		}
		else
		{
			JOptionPane.showMessageDialog(this,mainWindow.localizar("152") );
			return false;
		}
	}
	private JLabel getLblIDIcon() {
		if (lblIDIcon == null) {
			lblIDIcon = new JLabel("");
			lblIDIcon.setPreferredSize(new Dimension(30, 30));
			mainWindow.scaledImage(lblIDIcon, "/img/id.jpg");
		}
		return lblIDIcon;
	}
	private JMenuBar getMenuBar() {
		if (menuBar == null) {
			menuBar = new JMenuBar();
		}
		return menuBar;
	}
}
