package gui;

import java.awt.EventQueue;

import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Toolkit;
import java.util.ArrayList;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.border.CompoundBorder;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import logic.ReceiptHelper;

import java.awt.Color;
import java.awt.GridLayout;
import javax.swing.JButton;
import java.awt.FlowLayout;
import java.awt.Dimension;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import java.awt.event.ActionListener;
import java.io.File;
import java.awt.event.ActionEvent;

public class ReceiptDialog extends JDialog {
	private JPanel pnGeneral;
	private MainWindow mainWindow;
	private JPanel pnLeftAds;
	private JPanel pnRightAds;
	private JPanel pnTitle;
	private JPanel pnControlButton;
	private JPanel pnReceipt;
	private JPanel pnTittleBorder;
	private JLabel lblReceipt;
	private JPanel pnBack;
	private JPanel pnEndAndDownload;
	private JPanel pnEnd;
	private JButton btnReturn;
	private JPanel pnPdf;
	private JPanel pnTxt;
	private JButton btnTerminateAndDownload;
	private JButton btnTerminateAndDownload_1;
	private JButton btnTerminate;
	private JScrollPane scReceipt;
	private JTextArea textReceipt;
	private JLabel lblReceiptIcon;
	private ConfirmationDialog cD;
	private ReceiptHelper helper;
	private JFileChooser selector;

	public ReceiptDialog(MainWindow mainWindow, ConfirmationDialog cD) 
	{

		mainWindow.enableHelp(this,"receipt");
		setIconImage(Toolkit.getDefaultToolkit().getImage(ReceiptDialog.class.getResource("/img/roller-coaster.jpg")));
		setTitle(mainWindow.localizar("74") );
		
		this.cD = cD;
		this.mainWindow = mainWindow;
		helper = new ReceiptHelper(mainWindow,cD.getUserInfo());
		setBounds(100, 100, 736, 469);
		getContentPane().add(getPnGeneral(), BorderLayout.CENTER);

	}

	private JPanel getPnGeneral() {
		if (pnGeneral == null) {
			pnGeneral = new JPanel();
			pnGeneral.setLayout(new BorderLayout(0, 0));
			pnGeneral.add(getPnLeftAds(), BorderLayout.WEST);
			pnGeneral.add(getPnRightAds(), BorderLayout.EAST);
			pnGeneral.add(getPnTitle(), BorderLayout.NORTH);
			pnGeneral.add(getPnControlButton(), BorderLayout.SOUTH);
			pnGeneral.add(getPnReceipt(), BorderLayout.CENTER);
		}
		return pnGeneral;
	}
	private JPanel getPnLeftAds() {
		if (pnLeftAds == null) {
			pnLeftAds = new JPanel();
		}
		return pnLeftAds;
	}
	private JPanel getPnRightAds() {
		if (pnRightAds == null) {
			pnRightAds = new JPanel();
		}
		return pnRightAds;
	}
	private JPanel getPnTitle() {
		if (pnTitle == null) {
			pnTitle = new JPanel();
			pnTitle.add(getPnTittleBorder());
		}
		return pnTitle;
	}
	private JPanel getPnControlButton() {
		if (pnControlButton == null) {
			pnControlButton = new JPanel();
			pnControlButton.setLayout(new GridLayout(0, 3, 0, 0));
			pnControlButton.add(getPnBack());
			pnControlButton.add(getPnEndAndDownload());
			pnControlButton.add(getPnEnd());
		}
		return pnControlButton;
	}
	private JPanel getPnReceipt() {
		if (pnReceipt == null) {
			pnReceipt = new JPanel();
			pnReceipt.setLayout(new BorderLayout(0, 0));
			pnReceipt.add(getScReceipt());
		}
		return pnReceipt;
	}
	private JPanel getPnTittleBorder() {
		if (pnTittleBorder == null) {
			pnTittleBorder = new JPanel();
			pnTittleBorder.setBorder(new CompoundBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null), new LineBorder(new Color(0, 0, 0), 4, true)));
			pnTittleBorder.add(getLblReceiptIcon());
			pnTittleBorder.add(getLblReceipt());
		}
		return pnTittleBorder;
	}
	private JLabel getLblReceipt() {
		if (lblReceipt == null) {
			lblReceipt = new JLabel(mainWindow.localizar("70") );
			lblReceipt.setFont(new Font("Tahoma", Font.BOLD, 28));
		}
		return lblReceipt;
	}
	private JPanel getPnBack() {
		if (pnBack == null) {
			pnBack = new JPanel();
			FlowLayout flowLayout = (FlowLayout) pnBack.getLayout();
			flowLayout.setVgap(17);
			pnBack.add(getBtnReturn());
		}
		return pnBack;
	}
	private JPanel getPnEndAndDownload() {
		if (pnEndAndDownload == null) {
			pnEndAndDownload = new JPanel();
			pnEndAndDownload.setLayout(new GridLayout(2, 1, 0, 0));
			pnEndAndDownload.add(getPnPdf());
			pnEndAndDownload.add(getPnTxt());
		}
		return pnEndAndDownload;
	}
	private JPanel getPnEnd() {
		if (pnEnd == null) {
			pnEnd = new JPanel();
			FlowLayout flowLayout = (FlowLayout) pnEnd.getLayout();
			flowLayout.setVgap(17);
			pnEnd.add(getBtnTerminate());
		}
		return pnEnd;
	}
	private JButton getBtnReturn() {
		if (btnReturn == null) {
			btnReturn = new JButton(mainWindow.localizar("63") );
			btnReturn.setMnemonic(mainWindow.localizar("190").charAt(0));
			btnReturn.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) 
				{
					dispose();
				}
			});
			btnReturn.setPreferredSize(new Dimension(80, 31));
		}
		return btnReturn;
	}
	private JPanel getPnPdf() {
		if (pnPdf == null) {
			pnPdf = new JPanel();
			pnPdf.add(getBtnTerminateAndDownload());
		}
		return pnPdf;
	}
	private JPanel getPnTxt() {
		if (pnTxt == null) {
			pnTxt = new JPanel();
			pnTxt.add(getBtnTerminateAndDownload_1());
		}
		return pnTxt;
	}
	private JButton getBtnTerminateAndDownload() {
		if (btnTerminateAndDownload == null) {
			btnTerminateAndDownload = new JButton(mainWindow.localizar("72") );
			btnTerminateAndDownload.setMnemonic(mainWindow.localizar("193").charAt(0));
			btnTerminateAndDownload.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) 
				{
					int response = getRoot().showOpenDialog(null);
					if ( response == JFileChooser.APPROVE_OPTION) {
						helper.savePdf(getRoot().getSelectedFile().toString());
						terminate();
					}
					
				}
			});
		}
		return btnTerminateAndDownload;
	}
	private JButton getBtnTerminateAndDownload_1() {
		if (btnTerminateAndDownload_1 == null) {
			btnTerminateAndDownload_1 = new JButton(mainWindow.localizar("73") );
			btnTerminateAndDownload_1.setMnemonic(mainWindow.localizar("194").charAt(0));
			btnTerminateAndDownload_1.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) 
				{
				int response = getRoot().showOpenDialog(null);
				if ( response == JFileChooser.APPROVE_OPTION) {
					helper.saveTxt(getRoot().getSelectedFile().toString());
					terminate();
				}
			
					
				}
			});
		}
		return btnTerminateAndDownload_1;
	}
	private JButton getBtnTerminate() {
		if (btnTerminate == null) {
			btnTerminate = new JButton(mainWindow.localizar("71") );
			btnTerminate.setMnemonic(mainWindow.localizar("195").charAt(0));
			btnTerminate.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) 
				{
					terminate();
				}

				
			});
			btnTerminate.setPreferredSize(new Dimension(81, 31));
		}
		return btnTerminate;
	}
	private JScrollPane getScReceipt() {
		if (scReceipt == null) {
			scReceipt = new JScrollPane();
			scReceipt.setViewportView(getTextReceipt());
		}
		return scReceipt;
	}
	private void terminate() {
		cD.dispose();
		dispose();
		mainWindow.changeView("home");
		mainWindow.reset();
	}
	private JTextArea getTextReceipt() {
		if (textReceipt == null) {
			textReceipt = new JTextArea();
			textReceipt.setLineWrap(true);
			textReceipt.setWrapStyleWord(true);
			textReceipt.setText(helper.showInfo());
		}
		return textReceipt;
	}
	private JLabel getLblReceiptIcon() {
		if (lblReceiptIcon == null) {
			lblReceiptIcon = new JLabel("");
			lblReceiptIcon.setPreferredSize(new Dimension(30, 30));
			mainWindow.scaledImage(lblReceiptIcon, "/img/receipt.jpg");
		}
		return lblReceiptIcon;
	}
	private JFileChooser getRoot() {
		if (selector == null) {
			selector = new JFileChooser();
			selector.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY); 
			selector.setCurrentDirectory(new File(System.getProperty("user.home")+"/Desktop"));
		}
		
		return selector;
	}
}
