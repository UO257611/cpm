package gui;

import java.util.Date;

import logic.CartObject;
import logic.TypesOfBuy;

public interface MyCreatedDialogs 
{
	
	public String getAdults();
	public String getChilds();
	public String getCheckIn();
	public String getCheckOut();
	public boolean isBreakFast();
	public String getTypeOfBuy();
	public String getPrice();
	public TypesOfBuy getTheCurrentObject();
	public String getDescription();
	public CartObject createCartObject();
	public void closeDialog();
	public Date getSpInActualValue();
	public Date getSpOutActualValue();
	
	

}
