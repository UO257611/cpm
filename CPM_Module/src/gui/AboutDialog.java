package gui;

import java.awt.EventQueue;

import javax.swing.JDialog;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Dimension;
import java.awt.GridLayout;
import javax.swing.SwingConstants;
import java.awt.FlowLayout;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.JButton;
import javax.swing.border.EmptyBorder;
import javax.swing.border.CompoundBorder;
import javax.swing.border.TitledBorder;
import javax.swing.UIManager;
import java.awt.Color;
import java.awt.Toolkit;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.border.LineBorder;

public class AboutDialog extends JDialog {
	private JPanel pnGeneral;
	private JPanel pnTittleAuthor;
	private JLabel lblTittle;
	private JLabel lblIcon;
	private MainWindow mainWindow;
	private JPanel pnInfo;
	private JPanel pnAuthor;
	private JPanel pnApp;
	private JPanel pnAuthorInfo;
	private JPanel pnAuthorInfo2;
	private JLabel lblCreator;
	private JLabel lblDeveloper;
	private JScrollPane scAppInfo;
	private JPanel pnControlButton;
	private JButton btnOk;
	private JTextPane txtpnThisAppWas;
	private JPanel pnTittleBorder;

	/**
	 * Create the dialog.
	 */
	public AboutDialog(MainWindow mainWindow) {
		this.mainWindow = mainWindow;
		setIconImage(Toolkit.getDefaultToolkit().getImage(AboutDialog.class.getResource("/img/roller-coaster.jpg")));
		setTitle(this.mainWindow.localizar("197"));
		setBounds(100, 100, 519, 417);
		getContentPane().add(getPnGeneral(), BorderLayout.CENTER);
		

	}

	private JPanel getPnGeneral() {
		if (pnGeneral == null) {
			pnGeneral = new JPanel();
			pnGeneral.setLayout(new BorderLayout(0, 0));
			pnGeneral.add(getPnTittleAuthor(), BorderLayout.NORTH);
			pnGeneral.add(getPnInfo(), BorderLayout.CENTER);
			pnGeneral.add(getPnControlButton(), BorderLayout.SOUTH);
		}
		return pnGeneral;
	}
	private JPanel getPnTittleAuthor() {
		if (pnTittleAuthor == null) {
			pnTittleAuthor = new JPanel();
			pnTittleAuthor.add(getPnTittleBorder());
		}
		return pnTittleAuthor;
	}
	private JLabel getLblTittle() {
		if (lblTittle == null) {
			lblTittle = new JLabel("Viajaja app");
			lblTittle.setFont(new Font("Tahoma", Font.BOLD, 18));
		}
		return lblTittle;
	}
	private JLabel getLblIcon() {
		if (lblIcon == null) {
			lblIcon = new JLabel("");
			lblIcon.setPreferredSize(new Dimension(40, 40));
			mainWindow.scaledImage(lblIcon, "/img/roller-coaster.jpg");
		}
		return lblIcon;
	}
	private JPanel getPnInfo() {
		if (pnInfo == null) {
			pnInfo = new JPanel();
			pnInfo.setLayout(new GridLayout(2, 1, 0, 0));
			pnInfo.add(getPnAuthor());
			pnInfo.add(getPnApp());
		}
		return pnInfo;
	}
	private JPanel getPnAuthor() {
		if (pnAuthor == null) {
			pnAuthor = new JPanel();
			pnAuthor.setLayout(new GridLayout(1, 2, 0, 0));
			pnAuthor.add(getPnAuthorInfo());
			pnAuthor.add(getPnAuthorInfo2());
		}
		return pnAuthor;
	}
	private JPanel getPnApp() {
		if (pnApp == null) {
			pnApp = new JPanel();
			pnApp.setBorder(new CompoundBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), mainWindow.localizar("198"), TitledBorder.CENTER, TitledBorder.TOP, null, new Color(0, 0, 0)), new EmptyBorder(10, 30, 10, 30)));
			pnApp.setLayout(new BorderLayout(0, 0));
			pnApp.add(getScAppInfo());
		}
		return pnApp;
	}
	private JPanel getPnAuthorInfo() {
		if (pnAuthorInfo == null) {
			pnAuthorInfo = new JPanel();
			pnAuthorInfo.setLayout(new GridLayout(0, 1, 0, 0));
			pnAuthorInfo.add(getLblCreator());
		}
		return pnAuthorInfo;
	}
	private JPanel getPnAuthorInfo2() {
		if (pnAuthorInfo2 == null) {
			pnAuthorInfo2 = new JPanel();
			pnAuthorInfo2.setLayout(new GridLayout(1, 0, 0, 0));
			pnAuthorInfo2.add(getLblDeveloper());
		}
		return pnAuthorInfo2;
	}
	private JLabel getLblCreator() {
		if (lblCreator == null) {
			lblCreator = new JLabel(mainWindow.localizar("199"));
			lblCreator.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return lblCreator;
	}
	private JLabel getLblDeveloper() {
		if (lblDeveloper == null) {
			lblDeveloper = new JLabel("Eduardo Lamas Su\u00E1rez");
			lblDeveloper.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return lblDeveloper;
	}
	private JScrollPane getScAppInfo() {
		if (scAppInfo == null) {
			scAppInfo = new JScrollPane();
			scAppInfo.setViewportView(getTxtpnThisAppWas());
			
		}
		return scAppInfo;
	}
	private JPanel getPnControlButton() {
		if (pnControlButton == null) {
			pnControlButton = new JPanel();
			pnControlButton.add(getBtnOk());
		}
		return pnControlButton;
	}
	private JButton getBtnOk() {
		if (btnOk == null) {
			btnOk = new JButton(mainWindow.localizar("201"));
			btnOk.setMnemonic(mainWindow.localizar("202").charAt(0));
			btnOk.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) 
				{
					dispose();
				}
			});
			btnOk.setPreferredSize(new Dimension(80, 30));
		}
		return btnOk;
	}
	private JTextPane getTxtpnThisAppWas() {
		if (txtpnThisAppWas == null) {
			txtpnThisAppWas = new JTextPane();
			txtpnThisAppWas.setEditable(false);
			txtpnThisAppWas.setText(mainWindow.localizar("200"));

			txtpnThisAppWas.setCaretPosition(0);
		}
		return txtpnThisAppWas;
	}
	private JPanel getPnTittleBorder() {
		if (pnTittleBorder == null) {
			pnTittleBorder = new JPanel();
			pnTittleBorder.setBorder(new LineBorder(new Color(0, 0, 0), 4));
			pnTittleBorder.add(getLblIcon());
			pnTittleBorder.add(getLblTittle());
		}
		return pnTittleBorder;
	}
}
