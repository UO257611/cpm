package gui;

import java.awt.EventQueue;

import javax.swing.JDialog;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.util.Calendar;
import java.util.Date;

import javax.swing.border.EmptyBorder;
import javax.swing.BoxLayout;
import javax.swing.JSpinner;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.SpinnerDateModel;

import java.awt.Dimension;
import javax.swing.border.TitledBorder;

import logic.CartObject;
import logic.Park;
import logic.TypesOfBuy;

import javax.swing.UIManager;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SpinnerNumberModel;
import java.awt.Font;
import javax.swing.border.BevelBorder;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Toolkit;
import javax.swing.border.CompoundBorder;
import javax.swing.border.LineBorder;

public class ParkDialog extends JDialog  implements MyCreatedDialogs{
	private JPanel pnGeneral;
	private JPanel pnUserInfo;
	private JPanel pnParkInfo;
	private JPanel pnTitle;
	private JPanel pnInfo;
	private JPanel pnPhoto;
	private JPanel pnDescription;
	private JPanel pnPeople;
	private JPanel pnDate;
	private JPanel pnData;
	private JPanel pnButtonTextField;
	private JPanel pnCalculate;
	private JPanel pnPrice;
	private JPanel pnAdults;
	private JPanel pnChildren;
	private JSpinner spAdults;
	private JSpinner spChildren;
	private JSpinner spDateIn;
	private JButton btnCalculate;
	private JTextField txtFinalPrice;
	private JLabel lblCalendar;
	private MainWindow mainWindow;
	private Park actualPark;
	private JLabel lblViajajaParkForm;
	private JLabel lblPhoto;
	private JScrollPane scDescription;
	private JTextArea textDescription;
	private JPanel pnControlButton;
	private JButton btnGoBack;
	private JButton btnConfirmAndContinue;
	private JButton btnConfirmAndGo;
	private JDialog myDialog = this;
	private JLabel lblFormIcon;
	private JLabel lblFinalPrice;
	private JPanel pnBack;
	private JPanel pnContinue;
	private JPanel pnDateOut;
	private JPanel pnDateIn;
	private JSpinner spDateOut;
	private JLabel lblCalendar2;
	private JPanel pnDateInSpinner;
	private JLabel lblDateIn;
	private JPanel pnDateOutSpinner;
	private JLabel lblLastDay;
	private JLabel lblAdults;
	private JLabel lblKids;
	private JPanel pnTitleBorder;

	
	
	/**
	 * @wbp.parser.constructor
	 */
	public ParkDialog(MainWindow mainWindow, Park theClickedObject) 
	{
		setIconImage(Toolkit.getDefaultToolkit().getImage(ParkDialog.class.getResource("/img/roller-coaster.jpg")));
		setTitle(mainWindow.localizar("68"));
		setBounds(100, 100, 779, 560);
		this.mainWindow = mainWindow;
		this.actualPark = theClickedObject;
		getContentPane().add(getPnGeneral(), BorderLayout.CENTER);
		prepareDialog(false, "0","0",null,null);
	
	}
	
	
	public ParkDialog(MainWindow mainWindow, CartObject theEditingObject)
	{
		setIconImage(Toolkit.getDefaultToolkit().getImage(ParkDialog.class.getResource("/img/roller-coaster.jpg")));
		setTitle(mainWindow.localizar("68"));
		setBounds(100, 100, 779, 560);
		
		this.mainWindow = mainWindow;
		this.actualPark = (Park) theEditingObject.getShoppedObject();
		
		getContentPane().add(getPnGeneral(), BorderLayout.CENTER);
		prepareDialog(true, theEditingObject.getChilds(),theEditingObject.getAdults(),theEditingObject.getSpInValue(),theEditingObject.getSpOutValue());
		
		
	}

	private void prepareDialog(boolean editing, String children, String adults, Date dateIn, Date dateOut) 
	{
		if(!editing)
		{
			spChildren.setValue(mainWindow.getChilds());
			spAdults.setValue(mainWindow.getAdults());
			spDateIn.setValue(mainWindow.getCheckIn());
			spDateOut.setValue((Date) spDateIn.getValue());
			calculateMoney(mainWindow.getAdults(),mainWindow.getChilds());
		}
		else
		{

			spChildren.setValue(Integer.valueOf(children));
			spAdults.setValue(Integer.valueOf(adults));
			spDateIn.setValue(dateIn);
			spDateOut.setValue(dateOut);
			calculateMoney(Integer.valueOf(adults), Integer.valueOf(children));
		}
		
		
	}

	private JPanel getPnGeneral() {
		if (pnGeneral == null) {
			pnGeneral = new JPanel();
			pnGeneral.setLayout(new BorderLayout(0, 0));
			pnGeneral.add(getPnUserInfo(), BorderLayout.WEST);
			pnGeneral.add(getPnParkInfo(), BorderLayout.CENTER);
			pnGeneral.add(getPnControlButton(), BorderLayout.SOUTH);
		}
		return pnGeneral;
	}
	private JPanel getPnUserInfo() {
		if (pnUserInfo == null) {
			pnUserInfo = new JPanel();
			pnUserInfo.setLayout(new GridLayout(0, 1, 0, 0));
			pnUserInfo.add(getPnData());
			pnUserInfo.add(getPnButtonTextField());
		}
		return pnUserInfo;
	}
	private JPanel getPnParkInfo() {
		if (pnParkInfo == null) {
			pnParkInfo = new JPanel();
			pnParkInfo.setLayout(new BorderLayout(0, 0));
			pnParkInfo.add(getPnTitle(), BorderLayout.NORTH);
			pnParkInfo.add(getPnInfo(), BorderLayout.CENTER);
		}
		return pnParkInfo;
	}
	private JPanel getPnTitle() {
		if (pnTitle == null) {
			pnTitle = new JPanel();
			pnTitle.add(getPnTitleBorder());
		}
		return pnTitle;
	}
	private JPanel getPnInfo() {
		if (pnInfo == null) {
			pnInfo = new JPanel();
			pnInfo.setBorder(new EmptyBorder(82, 20, 82, 20));
			pnInfo.setLayout(new GridLayout(0, 2, 50, 0));
			pnInfo.add(getPnPhoto());
			pnInfo.add(getPnDescription());
		}
		return pnInfo;
	}
	private JPanel getPnPhoto() {
		if (pnPhoto == null) {
			pnPhoto = new JPanel();
			pnPhoto.add(getLblPhoto());
		}
		return pnPhoto;
	}
	private JPanel getPnDescription() {
		if (pnDescription == null) {
			pnDescription = new JPanel();
			pnDescription.setLayout(new BorderLayout(0, 0));
			pnDescription.add(getScDescription());
		}
		return pnDescription;
	}
	private JPanel getPnPeople() {
		if (pnPeople == null) {
			pnPeople = new JPanel();
			pnPeople.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			pnPeople.setLayout(new GridLayout(0, 2, 0, 0));
			pnPeople.add(getPnAdults());
			pnPeople.add(getPnChildren());
		}
		return pnPeople;
	}
	private JPanel getPnDate() {
		if (pnDate == null) {
			pnDate = new JPanel();
			pnDate.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			pnDate.setLayout(new GridLayout(0, 2, 0, 0));
			pnDate.add(getPnDateIn());
			pnDate.add(getPnDateOut());
		}
		return pnDate;
	}
	private JPanel getPnData() {
		if (pnData == null) {
			pnData = new JPanel();
			pnData.setBorder(new EmptyBorder(30, 0, 0, 0));
			pnData.setLayout(new GridLayout(0, 1, 0, 0));
			pnData.add(getPnPeople());
			pnData.add(getPnDate());
		}
		return pnData;
	}
	private JPanel getPnButtonTextField() {
		if (pnButtonTextField == null) {
			pnButtonTextField = new JPanel();
			pnButtonTextField.setLayout(new GridLayout(0, 1, 0, 0));
			pnButtonTextField.add(getPnCalculate());
			pnButtonTextField.add(getPnPrice());
		}
		return pnButtonTextField;
	}
	private JPanel getPnCalculate() {
		if (pnCalculate == null) {
			pnCalculate = new JPanel();
			pnCalculate.setBorder(new EmptyBorder(30, 0, 0, 0));
			pnCalculate.setPreferredSize(new Dimension(200, 100));
			pnCalculate.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
			pnCalculate.add(getBtnCalculate());
		}
		return pnCalculate;
	}
	private JPanel getPnPrice() {
		if (pnPrice == null) {
			pnPrice = new JPanel();
			pnPrice.add(getLblFinalPrice());
			pnPrice.add(getTxtFinalPrice());
		}
		return pnPrice;
	}
	private JPanel getPnAdults() {
		if (pnAdults == null) {
			pnAdults = new JPanel();
			pnAdults.setBorder(new CompoundBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)), new EmptyBorder(25, 0, 0, 0)));
			pnAdults.add(getLblAdults());
			pnAdults.add(getSpAdults());
		}
		return pnAdults;
	}
	private JPanel getPnChildren() {
		if (pnChildren == null) {
			pnChildren = new JPanel();
			pnChildren.setBorder(new CompoundBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)), new EmptyBorder(25, 0, 0, 0)));
			pnChildren.add(getLblKids());
			pnChildren.add(getSpChildren());
		}
		return pnChildren;
	}
	private JSpinner getSpAdults() {
		if (spAdults == null) {
			spAdults = new JSpinner();
			spAdults.setModel(new SpinnerNumberModel(new Integer(1), new Integer(0), null, new Integer(1)));
			spAdults.setPreferredSize(new Dimension(50, 20));
		}
		return spAdults;
	}
	private JSpinner getSpChildren() {
		if (spChildren == null) {
			spChildren = new JSpinner();
			spChildren.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));
			spChildren.setPreferredSize(new Dimension(50, 20));
		}
		return spChildren;
	}
	private JSpinner getSpDateIn() {
		if (spDateIn == null) {
			spDateIn = new JSpinner();
			spDateIn.setPreferredSize(new Dimension(85, 20));
			Date currentDate = mainWindow.getCurrentDay();
			
			Calendar c = Calendar.getInstance();
			c.setTime(currentDate);
			c.add(Calendar.DATE, -1);
			
			Date min = c.getTime();
			
			spDateIn.setModel(new SpinnerDateModel(currentDate, min, null, Calendar.DAY_OF_YEAR));
			spDateIn.setEditor(new JSpinner.DateEditor(spDateIn, "dd/MM/yyyy"));

		}
		return spDateIn;
	}
	private JButton getBtnCalculate() {
		if (btnCalculate == null) {
			btnCalculate = new JButton(mainWindow.localizar("41"));
			btnCalculate.setMnemonic(mainWindow.localizar("173").charAt(0));
			btnCalculate.setToolTipText(mainWindow.localizar("211"));
			btnCalculate.setFont(new Font("Tahoma", Font.PLAIN, 14));
			btnCalculate.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0)
				{
					checkButton();
				}


			});
			btnCalculate.setPreferredSize(new Dimension(100, 35));
		}
		return btnCalculate;
	}
	
	/**
	 * This method that the number of adults and child's is more than 0
	 */
	private void checkButton() {
		int adults = Integer.valueOf(spAdults.getValue().toString());
		int childs = Integer.valueOf(spChildren.getValue().toString());
		
		if(adults == 0 && childs == 0)
		{
			btnConfirmAndContinue.setEnabled(false);
			btnConfirmAndGo.setEnabled(false);
			JOptionPane.showMessageDialog( myDialog , mainWindow.localizar("143"));
		}
		else
		{
			btnConfirmAndContinue.setEnabled(true);
			btnConfirmAndGo.setEnabled(true);
			calculateMoney(adults, childs);
		}
	}
	
	/**
	 * This method calculate the current money to pay
	 * @param adults
	 * @param childs
	 */
	private void calculateMoney(int adults, int childs)
	{
		try 
		{
			
		
			double finalPrice = 0.0;
		
			long difference = ((Date) spDateOut.getValue()).getTime() - ((Date) spDateIn.getValue()).getTime();
			int days = (int) (difference / (24 * 60 * 60 * 1000));
			days++;
			
			if(days>=1)
			{
				btnConfirmAndContinue.setEnabled(true);
				btnConfirmAndGo.setEnabled(true);
				
				finalPrice += mainWindow.getTicketsReader().searchByPark(actualPark.getCode()).getFinalPriceDialog(adults, childs);
				finalPrice = finalPrice*days;
				txtFinalPrice.setText(String.valueOf(finalPrice)+mainWindow.localizar("203"));
			}
			else
			{
				JOptionPane.showMessageDialog(this, mainWindow.localizar("214"));
				
				btnConfirmAndContinue.setEnabled(false);
				btnConfirmAndGo.setEnabled(false);
			}
		}
		catch (Exception e) 
		{
			System.out.println(mainWindow.localizar("145"));
		}
		;
		
	}
	
	
	private JTextField getTxtFinalPrice() {
		if (txtFinalPrice == null) {
			txtFinalPrice = new JTextField();
			txtFinalPrice.setEditable(false);
			txtFinalPrice.setPreferredSize(new Dimension(400, 30));
			txtFinalPrice.setColumns(10);
		}
		return txtFinalPrice;
	}
	private JLabel getLblCalendar() {
		if (lblCalendar == null) {
			lblCalendar = new JLabel("");
			lblCalendar.setPreferredSize(new Dimension(30, 30));
			mainWindow.scaledImage(lblCalendar,"/img/calendar.jpg" );
		}
		return lblCalendar;
	}
	private JLabel getLblViajajaParkForm() {
		if (lblViajajaParkForm == null) {
			lblViajajaParkForm = new JLabel("Viajaja Park Form");
			lblViajajaParkForm.setFont(new Font("Tahoma", Font.BOLD, 24));
		}
		return lblViajajaParkForm;
	}
	private JLabel getLblPhoto() {
		if (lblPhoto == null) {
			lblPhoto = new JLabel("");
			lblPhoto.setPreferredSize(new Dimension(250, 305));
			mainWindow.scaledImage(lblPhoto, actualPark.getPhotoPath());
		}
		return lblPhoto;
	}
	private JScrollPane getScDescription() {
		if (scDescription == null) {
			scDescription = new JScrollPane();
			scDescription.setViewportView(getTextDescription());
		}
		return scDescription;
	}
	private JTextArea getTextDescription() {
		if (textDescription == null) {
			textDescription = new JTextArea();
			textDescription.setLineWrap(true);
			textDescription.setWrapStyleWord(true);
			textDescription.setText(actualPark.getDialogInfo(mainWindow.getTicketsReader(),mainWindow.getMyLocale()));
		}
		return textDescription;
	}
	private JPanel getPnControlButton() {
		if (pnControlButton == null) {
			pnControlButton = new JPanel();
			pnControlButton.setLayout(new GridLayout(0, 2, 0, 0));
			pnControlButton.add(getPnBack());
			pnControlButton.add(getPnContinue());
		}
		return pnControlButton;
	}
	private JButton getBtnGoBack() {
		if (btnGoBack == null) {
			btnGoBack = new JButton(mainWindow.localizar("141"));
			btnGoBack.setMnemonic(mainWindow.localizar("180").charAt(0));
			btnGoBack.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) 
				{
					dispose();
				}
			});
		}
		return btnGoBack;
	}
	private JButton getBtnConfirmAndContinue() {
		if (btnConfirmAndContinue == null) {
			btnConfirmAndContinue = new JButton(mainWindow.localizar("44"));
			btnConfirmAndContinue.setMnemonic(mainWindow.localizar("181").charAt(0));
			try {
				btnConfirmAndContinue.addActionListener(mainWindow.getMyCartButtonListner("continue", this));
			} catch (Exception e) {
				System.out.println(mainWindow.localizar("142"));
			}
		}
		return btnConfirmAndContinue;
	}
	private JButton getBtnConfirmAndGo() {
		if (btnConfirmAndGo == null) {
			btnConfirmAndGo = new JButton(mainWindow.localizar("45"));
			btnConfirmAndGo.setMnemonic(mainWindow.localizar("182").charAt(0));
			try {
				btnConfirmAndGo.addActionListener(mainWindow.getMyCartButtonListner("cart", this));
			} catch (Exception e) {
				System.out.println(mainWindow.localizar("142"));
			}
		}
		return btnConfirmAndGo;
	}
	private JLabel getLblFormIcon() {
		if (lblFormIcon == null) {
			lblFormIcon = new JLabel("");
			lblFormIcon.setPreferredSize(new Dimension(30,30));
			mainWindow.scaledImage(lblFormIcon, "/img/form.jpg");
		}
		return lblFormIcon;
	}
	private JLabel getLblFinalPrice() {
		if (lblFinalPrice == null) {
			lblFinalPrice = new JLabel(mainWindow.localizar("42"));
			lblFinalPrice.setFont(new Font("Tahoma", Font.PLAIN, 17));
		}
		return lblFinalPrice;
	}
	private JPanel getPnBack() {
		if (pnBack == null) {
			pnBack = new JPanel();
			FlowLayout flowLayout = (FlowLayout) pnBack.getLayout();
			flowLayout.setHgap(35);
			flowLayout.setAlignment(FlowLayout.LEFT);
			pnBack.add(getBtnGoBack());
		}
		return pnBack;
	}
	private JPanel getPnContinue() {
		if (pnContinue == null) {
			pnContinue = new JPanel();
			pnContinue.add(getBtnConfirmAndContinue());
			pnContinue.add(getBtnConfirmAndGo());
		}
		return pnContinue;
	}

	@Override
	public String getAdults() {
		
		return spAdults.getValue().toString();
	}

	@Override
	public String getChilds() {
		
		return spChildren.getValue().toString();
	}

	@Override
	public String getCheckIn() {
	
		Date in =(Date)spDateIn.getValue();
		 
		Calendar c = Calendar.getInstance();
		c.setTime(in);
		String x = ""+c.get(Calendar.DATE)+"/"+(c.get(Calendar.MONTH)+1)+"/"+c.get(Calendar.YEAR);
		return x;
		
	}

	@Override
	public String getCheckOut() 
	{

		Date in =(Date)spDateIn.getValue();
		 
		Calendar c = Calendar.getInstance();
		c.setTime(in);
		String x = ""+c.get(Calendar.DATE)+"/"+(c.get(Calendar.MONTH)+1)+"/"+c.get(Calendar.YEAR);
		return x;
		
	}

	@Override
	public boolean isBreakFast() {
		
		return false;
	}

	@Override
	public String getTypeOfBuy() {
		
		return "Park";
	}

	@Override
	public String getPrice() {
		
		return txtFinalPrice.getText();
	}

	@Override
	public TypesOfBuy getTheCurrentObject() {
		
		return actualPark;
	}

	@Override
	/**
	 * This method return a description of the park
	 */
	public String getDescription() 
	{
		StringBuilder info = new StringBuilder();
		info.append(mainWindow.localizar("135")+" " + actualPark.getNamePark()+".\n");
		info.append(mainWindow.localizar("151")+" " + actualPark.getCity()+".\n");
		return  info.toString();
	}
	
	@Override
	public CartObject createCartObject() {
		return new CartObject(getAdults(),getChilds(), getTheCurrentObject(), getCheckIn(),
				getCheckOut(), getPrice(), getDescription(), getTypeOfBuy(),getSpInActualValue(),getSpOutActualValue(), false );
	}
	
	@Override
	/**
	 * This method close the dialog
	 */
	public void closeDialog() 
	{
		dispose();
		
	}


	@Override
	public Date getSpInActualValue() {
		
		return (Date) spDateIn.getValue();
	}


	@Override
	public Date getSpOutActualValue() {
		
		return (Date)spDateOut.getValue();
	}
	private JPanel getPnDateOut() {
		if (pnDateOut == null) {
			pnDateOut = new JPanel();
			pnDateOut.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			pnDateOut.setLayout(new GridLayout(0, 1, 0, 0));
			pnDateOut.add(getLblLastDay());
			pnDateOut.add(getPnDateOutSpinner());
		}
		return pnDateOut;
	}
	private JPanel getPnDateIn() {
		if (pnDateIn == null) {
			pnDateIn = new JPanel();
			pnDateIn.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			pnDateIn.setLayout(new GridLayout(0, 1, 0, 0));
			pnDateIn.add(getLblDateIn());
			pnDateIn.add(getPnDateInSpinner());
		}
		return pnDateIn;
	}
	private JSpinner getSpDateOut() {
		if (spDateOut == null) {
			spDateOut = new JSpinner();
			spDateOut.setPreferredSize(new Dimension(85, 20));
			Date currentDate = mainWindow.getCurrentDay();
			
			Calendar c = Calendar.getInstance();
			c.setTime(currentDate);
			c.add(Calendar.DATE, -1);
			
			Date min = c.getTime();
			
			spDateOut.setModel(new SpinnerDateModel(currentDate, min, null, Calendar.DAY_OF_YEAR));
			spDateOut.setEditor(new JSpinner.DateEditor(spDateOut, "dd/MM/yyyy"));
		}
		return spDateOut;
	}
	private JLabel getLblCalendar2() {
		if (lblCalendar2 == null) {
			lblCalendar2 = new JLabel("");
			lblCalendar2.setPreferredSize(new Dimension(30, 30));
			mainWindow.scaledImage(lblCalendar2,"/img/calendar.jpg" );
		}
		return lblCalendar2;
	}
	private JPanel getPnDateInSpinner() {
		if (pnDateInSpinner == null) {
			pnDateInSpinner = new JPanel();
			pnDateInSpinner.add(getLblCalendar());
			pnDateInSpinner.add(getSpDateIn());
		}
		return pnDateInSpinner;
	}
	private JLabel getLblDateIn() {
		if (lblDateIn == null) {
			lblDateIn = new JLabel(mainWindow.localizar("49"));
			lblDateIn.setDisplayedMnemonic(mainWindow.localizar("184").charAt(0));
			lblDateIn.setLabelFor(getSpDateIn());
			lblDateIn.setFont(new Font("Tahoma", Font.PLAIN, 14));
		}
		return lblDateIn;
	}
	private JPanel getPnDateOutSpinner() {
		if (pnDateOutSpinner == null) {
			pnDateOutSpinner = new JPanel();
			pnDateOutSpinner.add(getLblCalendar2());
			pnDateOutSpinner.add(getSpDateOut());
		}
		return pnDateOutSpinner;
	}
	private JLabel getLblLastDay() {
		if (lblLastDay == null) {
			lblLastDay = new JLabel(mainWindow.localizar("50"));
			lblLastDay.setDisplayedMnemonic(mainWindow.localizar("185").charAt(0));
			lblLastDay.setLabelFor(getSpDateOut());
			lblLastDay.setFont(new Font("Tahoma", Font.PLAIN, 14));
		}
		return lblLastDay;
	}
	private JLabel getLblAdults() {
		if (lblAdults == null) {
			lblAdults = new JLabel(mainWindow.localizar("12"));
			lblAdults.setDisplayedMnemonic(mainWindow.localizar("166").charAt(0));
			lblAdults.setLabelFor(getSpAdults());
			lblAdults.setFont(new Font("Tahoma", Font.PLAIN, 14));
		}
		return lblAdults;
	}
	private JLabel getLblKids() {
		if (lblKids == null) {
			lblKids = new JLabel(mainWindow.localizar("13"));
			lblKids.setDisplayedMnemonic(mainWindow.localizar("167").charAt(0));
			lblKids.setLabelFor(getSpChildren());
			lblKids.setFont(new Font("Tahoma", Font.PLAIN, 14));
		}
		return lblKids;
	}
	private JPanel getPnTitleBorder() {
		if (pnTitleBorder == null) {
			pnTitleBorder = new JPanel();
			pnTitleBorder.setBorder(new CompoundBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null), new LineBorder(new Color(0, 0, 0), 4, true)));
			pnTitleBorder.add(getLblFormIcon());
			pnTitleBorder.add(getLblViajajaParkForm());
		}
		return pnTitleBorder;
	}
}

