package gui;

import java.awt.EventQueue;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.Image;

import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JSpinner;
import javax.swing.border.TitledBorder;

import logic.Accommodation;
import logic.CartObject;
import logic.Park;
import logic.TypesOfBuy;

import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.UIManager;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import java.awt.Toolkit;
import javax.swing.SpinnerNumberModel;
import javax.swing.SpinnerDateModel;
import java.util.Date;
import java.util.ArrayList;
import java.util.Calendar;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.FlowLayout;
import javax.swing.SwingConstants;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import java.awt.Font;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.border.CompoundBorder;

public class AccommodationDialog extends JDialog implements MyCreatedDialogs{
	private JPanel generalPane;
	private JPanel pnUserInfotmation;
	private JPanel pnAccomodationInfo;
	private JPanel pnControlButtons;
	private JPanel pnPeople;
	private JPanel pnNights;
	private JPanel pnDate;
	private JPanel pnPark;
	private JPanel pnAddons;
	private JPanel pnPhoto;
	private JPanel pnDescription;
	private JScrollPane scDescription;
	private JSpinner spNights;
	private JSpinner spAdults;
	private JSpinner spDate;
	private JComboBox cbParks;
	private JButton btnGoBack;
	private JButton btnConfirmShopping;
	private JButton btnGoCart;
	private JLabel lblPhoto;
	private JTextArea textDescription;
	private MainWindow mainWindow;
	private Accommodation actualAccomodation;
	private JPanel pnCalculate;
	private JButton btnCalculate;
	private JPanel pnBack;
	private JPanel pnContinue;
	private JCheckBox chckbxSuit;
	private JCheckBox chckbxDuplex;
	private JCheckBox chckbxBreakfast;
	private JPanel pnFinalPrice;
	private JTextField txtFinalPrice;
	private JPanel pnInfo;
	private JLabel lblViajajaAccomodationForm;
	private JPanel pnTitle;
	private JLabel lblCalendar;
	private JLabel lblFormIcon;
	private JLabel lblFinalPriceTo;
	private JPanel pnAdults;
	private JPanel pnChildren;
	private JSpinner spChildren;
	private JDialog myDialog = this;
	private JLabel lblNights;
	private JLabel lblAdults;
	private JLabel lblKids;
	private JLabel lblArrivalDate;
	private JLabel lblParks;
	private JPanel pnTitleBorder;

	
	/**
	 * Create the dialog.
	 * @wbp.parser.constructor
	 */
	public AccommodationDialog(MainWindow mainWindow, Accommodation clickedObject) {
		this.mainWindow= mainWindow;
		this.actualAccomodation = clickedObject;
		
		setTitle("Viajaja app: Accomodation form");
		setIconImage(Toolkit.getDefaultToolkit().getImage(AccommodationDialog.class.getResource("/img/roller-coaster.jpg")));
		setBounds(100, 100, 830, 626);
		getContentPane().add(getGeneralPane(), BorderLayout.CENTER);
		prepareDialog(false, "", "", null, null, mainWindow.getChBreakfast().isSelected());

	}
	
	
	
	public AccommodationDialog(MainWindow mainWindow, CartObject editingObject)
	{
		setTitle(mainWindow.localizar("47"));
		setIconImage(Toolkit.getDefaultToolkit().getImage(AccommodationDialog.class.getResource("/img/roller-coaster.jpg")));
		setBounds(100, 100, 830, 626);
		this.mainWindow = mainWindow;
		this.actualAccomodation = (Accommodation) editingObject.getShoppedObject();
		getContentPane().add(getGeneralPane(), BorderLayout.CENTER);
		prepareDialog(true, editingObject.getAdults(), editingObject.getChilds(), editingObject.getSpInValue(), editingObject.getSpOutValue(), editingObject.isBreakfast() );
		
	}
	
	private void prepareDialog(boolean editing, String adults, String children, Date dateIn, Date dateOut, boolean breakfast) 
	{

		mainWindow.enableHelp(this, "accomForm");
		if(!editing)
		{
			int people = mainWindow.getAdults()+mainWindow.getChilds();
			spAdults.setValue(people);
			spDate.setValue(mainWindow.getCheckIn());
			spNights.setValue(mainWindow.getNights());
			chckbxBreakfast.setSelected(breakfast);
			enableComponents();
		}
		else
		{
			
			int people = Integer.valueOf(adults)+ Integer.valueOf(children);
			spAdults.setValue(people);
			spDate.setValue(dateIn);
			long difference =  dateOut.getTime() - dateIn.getTime();
			int nights = (int) (difference / (24 * 60 * 60 * 1000));
			spNights.setValue(nights);
			chckbxBreakfast.setSelected(breakfast);
			enableComponents();
			
		}
		
		calculateMoney();
		
		
	}



	private void enableComponents() {
		if(actualAccomodation.getType().equals("HO"))
		{
			spAdults.setEnabled(true);
			spChildren.setEnabled(true);
			chckbxBreakfast.setSelected(mainWindow.getChBreakfast().isSelected());
			chckbxBreakfast.setEnabled(true);
		}
		else
		{
			spAdults.setEnabled(false);
			spChildren.setEnabled(false);
			spChildren.setEnabled(false);
			chckbxBreakfast.setEnabled(false);
		}
	}
	private JPanel getGeneralPane() {
		if (generalPane == null) {
			generalPane = new JPanel();
			generalPane.setLayout(new BorderLayout(0, 0));
			generalPane.add(getPnUserInfotmation(), BorderLayout.WEST);
			generalPane.add(getPnAccomodationInfo(), BorderLayout.CENTER);
			generalPane.add(getPnControlButtons(), BorderLayout.SOUTH);
		}
		return generalPane;
	}
	private JPanel getPnUserInfotmation() {
		if (pnUserInfotmation == null) {
			pnUserInfotmation = new JPanel();
			pnUserInfotmation.setBorder(new EmptyBorder(16, 0, 19, 0));
			pnUserInfotmation.setLayout(new GridLayout(0, 1, 0, 0));
			pnUserInfotmation.add(getPnNights());
			pnUserInfotmation.add(getPnPeople());
			pnUserInfotmation.add(getPnDate());
			pnUserInfotmation.add(getPnPark());
			pnUserInfotmation.add(getPnAddons());
			pnUserInfotmation.add(getPnCalculate());
		}
		return pnUserInfotmation;
	}
	private JPanel getPnAccomodationInfo() {
		if (pnAccomodationInfo == null) {
			pnAccomodationInfo = new JPanel();
			pnAccomodationInfo.setLayout(new BorderLayout(0, 0));
			pnAccomodationInfo.add(getPnInfo(), BorderLayout.CENTER);
			pnAccomodationInfo.add(getPnFinalPrice(), BorderLayout.SOUTH);
			pnAccomodationInfo.add(getPnTitle(), BorderLayout.NORTH);
			
		}
		return pnAccomodationInfo;
	}
	private JPanel getPnControlButtons() {
		if (pnControlButtons == null) {
			pnControlButtons = new JPanel();
			pnControlButtons.setLayout(new GridLayout(0, 2, 0, 0));
			pnControlButtons.add(getPnBack());
			pnControlButtons.add(getPnContinue());
		}
		return pnControlButtons;
	}
	private JPanel getPnPeople() {
		if (pnPeople == null) {
			pnPeople = new JPanel();
			pnPeople.setBorder(null);
			pnPeople.setLayout(new GridLayout(0, 2, 0, 0));
			pnPeople.add(getPnAdults());
			pnPeople.add(getPnChildren());
		}
		return pnPeople;
	}
	private JPanel getPnNights() {
		if (pnNights == null) {
			pnNights = new JPanel();
			pnNights.setBorder(new CompoundBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)), new EmptyBorder(19, 0, 0, 0)));
			pnNights.add(getLblNights());
			pnNights.add(getSpNights());
		}
		return pnNights;
	}
	private JPanel getPnDate() {
		if (pnDate == null) {
			pnDate = new JPanel();
			pnDate.setBorder(new CompoundBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)), new EmptyBorder(19, 0, 0, 0)));
			pnDate.add(getLblArrivalDate());
			pnDate.add(getSpDate());
			pnDate.add(getLblCalendar());
		}
		return pnDate;
	}
	private JPanel getPnPark() {
		if (pnPark == null) {
			pnPark = new JPanel();
			pnPark.setBorder(new CompoundBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)), new EmptyBorder(19, 0, 0, 0)));
			pnPark.add(getLblParks());
			pnPark.add(getCbParks());
		}
		return pnPark;
	}
	private JPanel getPnAddons() {
		if (pnAddons == null) {
			pnAddons = new JPanel();
			pnAddons.setBorder(new TitledBorder(null, mainWindow.localizar("14"), TitledBorder.LEADING, TitledBorder.TOP, null, null));
			pnAddons.add(getChckbxSuit());
			pnAddons.add(getChckbxDuplex());
			pnAddons.add(getChckbxBreakfast());
		}
		return pnAddons;
	}
	private JPanel getPnPhoto() {
		if (pnPhoto == null) {
			pnPhoto = new JPanel();
			pnPhoto.setLayout(new BorderLayout(0, 0));
			pnPhoto.add(getLblPhoto());
		}
		return pnPhoto;
	}
	private JPanel getPnDescription() {
		if (pnDescription == null) {
			pnDescription = new JPanel();
			pnDescription.setLayout(new BorderLayout(0, 10));
			pnDescription.add(getScDescription(), BorderLayout.CENTER);
		}
		return pnDescription;
	}
	private JScrollPane getScDescription() {
		if (scDescription == null) {
			scDescription = new JScrollPane();
			scDescription.setViewportView(getTextDescription());
		}
		return scDescription;
	}
	private JSpinner getSpNights() {
		if (spNights == null) {
			spNights = new JSpinner();
			spNights.setModel(new SpinnerNumberModel(new Integer(1), new Integer(1), null, new Integer(1)));
			spNights.setPreferredSize(new Dimension(50,25));
		}
		return spNights;
	}
	private JSpinner getSpAdults() {
		if (spAdults == null) {
			spAdults = new JSpinner();
			spAdults.setModel(new SpinnerNumberModel(new Integer(1), new Integer(0), null, new Integer(1)));
			spAdults.setPreferredSize(new Dimension(50,25));
		}
		return spAdults;
	}
	private JSpinner getSpDate() {
		if (spDate == null) {
			spDate = new JSpinner();
			Date currentDate = mainWindow.getCurrentDay();
			
			Calendar c = Calendar.getInstance();
			c.setTime(currentDate);
			c.add(Calendar.DATE, -1);
			Date min = c.getTime();
			
			spDate.setModel(new SpinnerDateModel(currentDate, min, null, Calendar.DAY_OF_YEAR));
			spDate.setEditor(new JSpinner.DateEditor(spDate, "dd/MM/yyyy"));
			spDate.setPreferredSize(new Dimension(110, 25));
		}
		return spDate;
	}
	private JComboBox getCbParks() {
		if (cbParks == null) {
			cbParks = new JComboBox();
			cbParks.addItem(mainWindow.localizar("43"));
			Park park = mainWindow.getParkReader().searchPark(actualAccomodation.getThemeParkCode());
			cbParks.addItem(park.getNamePark());
		}
		return cbParks;
	}
	private JButton getBtnGoBack() {
		if (btnGoBack == null) {
			btnGoBack = new JButton(mainWindow.localizar("141"));
			btnGoBack.setMnemonic(mainWindow.localizar("180").charAt(0));
			btnGoBack.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) 
				{
					dispose();
				}
			});
			btnGoBack.setHorizontalAlignment(SwingConstants.LEFT);
		}
		return btnGoBack;
	}
	private JButton getBtnConfirmShopping() {
		if (btnConfirmShopping == null) {
			btnConfirmShopping = new JButton(mainWindow.localizar("44"));
			btnConfirmShopping.setMnemonic(mainWindow.localizar("181").charAt(0));
			btnConfirmShopping.setHorizontalAlignment(SwingConstants.RIGHT);
			try {
				btnConfirmShopping.addActionListener(mainWindow.getMyCartButtonListner("continue", this));
			} catch (Exception e) {
				System.out.println(mainWindow.localizar("142"));
			}
		}
		return btnConfirmShopping;
	}
	private JButton getBtnGoCart() {
		if (btnGoCart == null) {
			btnGoCart = new JButton(mainWindow.localizar("45"));
			btnGoCart.setMnemonic(mainWindow.localizar("182").charAt(0));
			btnGoCart.setHorizontalAlignment(SwingConstants.RIGHT);
			try {
				btnGoCart.addActionListener(mainWindow.getMyCartButtonListner("cart", this));
			} catch (Exception e) {
				System.out.println(mainWindow.localizar("142"));
			}
		}
		return btnGoCart;
	}
	private JLabel getLblPhoto() {
		if (lblPhoto == null) {
			lblPhoto = new JLabel("");
			lblPhoto.setPreferredSize(new Dimension(300,305));
			mainWindow.scaledImage(lblPhoto, actualAccomodation.getPhotoPath());
			
		}
		return lblPhoto;
	}
	private JTextArea getTextDescription() {
		if (textDescription == null) {
			textDescription = new JTextArea();
			textDescription.setLineWrap(true);
			textDescription.setWrapStyleWord(true);
			textDescription.setText(actualAccomodation.getDataDialogPrepared(mainWindow.getTicketsReader(), mainWindow.getParkReader(),mainWindow.getMyLocale()));
		}
		return textDescription;
	}
	private JPanel getPnCalculate() {
		if (pnCalculate == null) {
			pnCalculate = new JPanel();
			pnCalculate.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			FlowLayout flowLayout = (FlowLayout) pnCalculate.getLayout();
			flowLayout.setVgap(25);
			pnCalculate.add(getBtnCalculate());
		}
		return pnCalculate;
	}
	private JButton getBtnCalculate() {
		if (btnCalculate == null) {
			btnCalculate = new JButton(mainWindow.localizar("41"));
			btnCalculate.setToolTipText(mainWindow.localizar("209"));
			btnCalculate.setMnemonic(mainWindow.localizar("179").charAt(0));
			btnCalculate.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0)
				{
					checkButton();
					
					
					
					
				}


			});
			btnCalculate.setPreferredSize(new Dimension(100, 35));
		}
		return btnCalculate;
	}
	
	private void checkButton() {
		if( Integer.valueOf(spAdults.getValue().toString())==0 && Integer.valueOf(spChildren.getValue().toString())==0)
		{
			btnConfirmShopping.setEnabled(false);
			btnGoCart.setEnabled(false);
			JOptionPane.showMessageDialog( myDialog , mainWindow.localizar("143"));
			
		}
		else
		{
			btnConfirmShopping.setEnabled(true);
			btnGoCart.setEnabled(true);
			calculateMoney();
		}
	}
	
	private void calculateMoney()
	{
		double finalPrice = 0.0;
		int adult = Integer.valueOf((spAdults.getValue().toString()));
		int children = Integer.valueOf((spChildren.getValue().toString()));
		if(adult+ children > actualAccomodation.getCapacity())
		{
			JOptionPane.showMessageDialog(this,  mainWindow.localizar("144") + actualAccomodation.getCapacity());
			btnConfirmShopping.setEnabled(false);
			btnGoCart.setEnabled(false);
		}
		else
		{
			if(actualAccomodation.getType().equals("HO"))
			{
				finalPrice = actualAccomodation.getFinalDialogPrice(children+adult);
			}
			else
			{
				finalPrice = actualAccomodation.getFinalDialogPrice(children+adult);
			}
			
			if(!cbParks.getSelectedItem().equals(mainWindow.localizar("43")))
			{
				
				try {
					finalPrice += mainWindow.getTicketsReader().searchByPark(actualAccomodation.getThemeParkCode()).getFinalPriceDialog(adult, children);
				} catch (NumberFormatException e) {
					
					e.printStackTrace();
				} catch (Exception e) {
					System.out.println(mainWindow.localizar("145"));
				}
				
			}
			
			if(chckbxBreakfast.isSelected())
			{
				finalPrice += (10*finalPrice)/100;
			}
			
			finalPrice = finalPrice * Integer.valueOf(spNights.getValue().toString());
			
			
				btnConfirmShopping.setEnabled(true);
				btnGoCart.setEnabled(true);
				txtFinalPrice.setText(String.valueOf(finalPrice) + mainWindow.localizar("203") );
			}
			
	}
	private JPanel getPnBack() {
		if (pnBack == null) {
			pnBack = new JPanel();
			FlowLayout flowLayout = (FlowLayout) pnBack.getLayout();
			flowLayout.setHgap(35);
			flowLayout.setAlignment(FlowLayout.LEFT);
			pnBack.add(getBtnGoBack());
		}
		return pnBack;
	}
	private JPanel getPnContinue() {
		if (pnContinue == null) {
			pnContinue = new JPanel();
			FlowLayout flowLayout = (FlowLayout) pnContinue.getLayout();
			flowLayout.setHgap(0);
			pnContinue.setBorder(null);
			pnContinue.add(getBtnConfirmShopping());
			pnContinue.add(getBtnGoCart());
		}
		return pnContinue;
	}
	private JCheckBox getChckbxSuit() {
		if (chckbxSuit == null) {
			chckbxSuit = new JCheckBox(mainWindow.localizar("39"));
			chckbxSuit.setMnemonic(mainWindow.localizar("177").charAt(0));
			chckbxSuit.setFont(new Font("Tahoma", Font.PLAIN, 14));
		}
		return chckbxSuit;
	}
	private JCheckBox getChckbxDuplex() {
		if (chckbxDuplex == null) {
			chckbxDuplex = new JCheckBox(mainWindow.localizar("40"));
			chckbxDuplex.setMnemonic(mainWindow.localizar("178").charAt(0));
			chckbxDuplex.setFont(new Font("Tahoma", Font.PLAIN, 14));
		}
		return chckbxDuplex;
	}
	private JCheckBox getChckbxBreakfast() {
		if (chckbxBreakfast == null) {
			chckbxBreakfast = new JCheckBox(mainWindow.localizar("15"));
			chckbxBreakfast.setMnemonic(mainWindow.localizar("168").charAt(0));
			chckbxBreakfast.setFont(new Font("Tahoma", Font.PLAIN, 14));
		}
		return chckbxBreakfast;
	}
	private JPanel getPnFinalPrice() {
		if (pnFinalPrice == null) {
			pnFinalPrice = new JPanel();
			pnFinalPrice.setBorder(new EmptyBorder(0, 0, 37, 0));
			pnFinalPrice.add(getLblFinalPriceTo());
			pnFinalPrice.add(getTxtFinalPrice());
		}
		return pnFinalPrice;
	}
	private JTextField getTxtFinalPrice() {
		if (txtFinalPrice == null) {
			txtFinalPrice = new JTextField();
			txtFinalPrice.setPreferredSize(new Dimension(400, 45));
			txtFinalPrice.setEditable(false);
			txtFinalPrice.setColumns(10);
		}
		return txtFinalPrice;
	}
	private JPanel getPnInfo() {
		if (pnInfo == null) {
			pnInfo = new JPanel();
			pnInfo.setBorder(new EmptyBorder(82, 20, 82, 20));
			pnInfo.setLayout(new GridLayout(0, 2, 50, 0));
			pnInfo.add(getPnPhoto());
			pnInfo.add(getPnDescription());
		}
		return pnInfo;
	}
	private JLabel getLblViajajaAccomodationForm() {
		if (lblViajajaAccomodationForm == null) {
			lblViajajaAccomodationForm = new JLabel(mainWindow.localizar("47"));
			lblViajajaAccomodationForm.setFont(new Font("Tahoma", Font.BOLD, 24));
			lblViajajaAccomodationForm.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return lblViajajaAccomodationForm;
	}
	private JPanel getPnTitle() {
		if (pnTitle == null) {
			pnTitle = new JPanel();
			pnTitle.add(getPnTitleBorder());
		}
		return pnTitle;
	}
	private JLabel getLblCalendar() {
		if (lblCalendar == null) {
			lblCalendar = new JLabel("");
			lblCalendar.setPreferredSize(new Dimension(30,30));
			
			mainWindow.scaledImage(lblCalendar, "/img/calendar.jpg");
			
			
		}
		return lblCalendar;
	}

	
	private JLabel getLblFormIcon() {
		if (lblFormIcon == null) {
			lblFormIcon = new JLabel("");
			lblFormIcon.setPreferredSize(new Dimension(25,25));
			mainWindow.scaledImage(lblFormIcon, "/img/form.jpg");
		}
		return lblFormIcon;
	}
	private JLabel getLblFinalPriceTo() {
		if (lblFinalPriceTo == null) {
			lblFinalPriceTo = new JLabel(mainWindow.localizar("42"));
			lblFinalPriceTo.setFont(new Font("Tahoma", Font.PLAIN, 18));
		}
		return lblFinalPriceTo;
	}
	private JPanel getPnAdults() {
		if (pnAdults == null) {
			pnAdults = new JPanel();
			pnAdults.setBorder(new CompoundBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)), new EmptyBorder(19, 0, 0, 0)));
			pnAdults.add(getLblAdults());
			pnAdults.add(getSpAdults());
		}
		return pnAdults;
	}
	private JPanel getPnChildren() {
		if (pnChildren == null) {
			pnChildren = new JPanel();
			pnChildren.setBorder(new CompoundBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)), new EmptyBorder(19, 0, 0, 0)));
			pnChildren.add(getLblKids());
			pnChildren.add(getSpChildren());
		}
		return pnChildren;
	}
	private JSpinner getSpChildren() {
		if (spChildren == null) {
			spChildren = new JSpinner();
			spChildren.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));
			spChildren.setPreferredSize(new Dimension(50,25));
		}
		return spChildren;
	}
	
	
	@Override
	public String getAdults() {
		
		return spAdults.getValue().toString();
	}

	@Override
	public String getChilds() {
		
		return spChildren.getValue().toString();
	}

	@Override
	public String getCheckIn() {
	
		Date in =(Date)spDate.getValue();
		 
		Calendar c = Calendar.getInstance();
		c.setTime(in);
		String x = ""+c.get(Calendar.DATE)+"/"+(c.get(Calendar.MONTH)+1)+"/"+c.get(Calendar.YEAR);
		return x;
		
	}

	@Override
	public String getCheckOut() {
		
		Date out =(Date)spDate.getValue();
		 
		Calendar c = Calendar.getInstance();
		c.setTime(out);
		c.add(Calendar.DATE, Integer.valueOf(spNights.getValue().toString()));
		String x = ""+c.get(Calendar.DATE)+"/"+(c.get(Calendar.MONTH)+1)+"/"+c.get(Calendar.YEAR);
		return x;
	}

	@Override
	public boolean isBreakFast() {
		
		return false;
	}

	@Override
	public String getTypeOfBuy() {
		
		return "Park";
	}

	@Override
	public String getPrice() {
		
		return txtFinalPrice.getText();
	}

	@Override
	public TypesOfBuy getTheCurrentObject() {
		
		return actualAccomodation;
	}

	@Override
	public String getDescription() 
	{
		StringBuilder info = new StringBuilder();
		info.append(mainWindow.localizar("146")+" "+ actualAccomodation.getFacilityName());
		return info.toString();
	}
	
	@Override
	public CartObject createCartObject() {
		return new CartObject(getAdults(),getChilds(), getTheCurrentObject(), getCheckIn(),
				getCheckOut(), getPrice(), getDescription(), getTypeOfBuy(), getSpInActualValue(), getSpOutActualValue(), chckbxBreakfast.isSelected());
	}
	
	@Override
	public void closeDialog() 
	{
		dispose();
		
	}

	@Override
	public Date getSpInActualValue() {
		
		return (Date)spDate.getValue();
	}

	@Override
	public Date getSpOutActualValue() {
		Calendar c = Calendar.getInstance();
		c.setTime(getSpInActualValue());
		c.add(Calendar.DATE, Integer.valueOf(spNights.getValue().toString()));
		return c.getTime();
	}
	private JLabel getLblNights() {
		if (lblNights == null) {
			lblNights = new JLabel(mainWindow.localizar("36"));
			lblNights.setDisplayedMnemonic(mainWindow.localizar("175").charAt(0));
			lblNights.setLabelFor(getSpNights());
			lblNights.setFont(new Font("Tahoma", Font.PLAIN, 14));
		}
		return lblNights;
	}
	private JLabel getLblAdults() {
		if (lblAdults == null) {
			lblAdults = new JLabel(mainWindow.localizar("12"));
			lblAdults.setDisplayedMnemonic(mainWindow.localizar("166").charAt(0));
			lblAdults.setLabelFor(getSpAdults());
			lblAdults.setFont(new Font("Tahoma", Font.PLAIN, 14));
		}
		return lblAdults;
	}
	private JLabel getLblKids() {
		if (lblKids == null) {
			lblKids = new JLabel(mainWindow.localizar("13"));
			lblKids.setDisplayedMnemonic(mainWindow.localizar("167").charAt(0));
			lblKids.setLabelFor(getSpChildren());
			lblKids.setFont(new Font("Tahoma", Font.PLAIN, 14));
		}
		return lblKids;
	}
	private JLabel getLblArrivalDate() {
		if (lblArrivalDate == null) {
			lblArrivalDate = new JLabel(mainWindow.localizar("37"));
			lblArrivalDate.setDisplayedMnemonic(mainWindow.localizar("176").charAt(0));
			lblArrivalDate.setLabelFor(getSpDate());
			lblArrivalDate.setFont(new Font("Tahoma", Font.PLAIN, 14));
		}
		return lblArrivalDate;
	}
	private JLabel getLblParks() {
		if (lblParks == null) {
			lblParks = new JLabel(mainWindow.localizar("38"));
			lblParks.setDisplayedMnemonic(mainWindow.localizar("172").charAt(0));
			lblParks.setLabelFor(getCbParks());
			lblParks.setFont(new Font("Tahoma", Font.PLAIN, 14));
		}
		return lblParks;
	}
	private JPanel getPnTitleBorder() {
		if (pnTitleBorder == null) {
			pnTitleBorder = new JPanel();
			pnTitleBorder.setBorder(new CompoundBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null), new LineBorder(new Color(0, 0, 0), 4, true)));
			pnTitleBorder.add(getLblFormIcon());
			pnTitleBorder.add(getLblViajajaAccomodationForm());
		}
		return pnTitleBorder;
	}
}
