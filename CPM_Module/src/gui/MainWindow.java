package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import javax.help.*;
import java.net.*;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.FlowLayout;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Component;

import java.awt.GridLayout;
import java.awt.Image;

import javax.swing.JButton;
import java.awt.Toolkit;
import java.awt.CardLayout;
import javax.swing.JTabbedPane;
import javax.swing.JScrollPane;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.AbstractDocument.AbstractElement;

import org.jvnet.substance.SubstanceLookAndFeel;

import gui_models.MyMultipleLineCellRender;
import gui_models.MyTableButton;
import gui_models.NonEditableAndPhoto;
import logic.Accommodation;
import logic.AccommodationReader;
import logic.CartObject;
import logic.PackagesReader;
import logic.Park;
import logic.ParksReader;
import logic.TicketsReader;
import logic.TypesOfBuy;
import myComponentsModified.MyClickedButton;
import logic.Package;

import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.UIManager;
import java.awt.Color;
import javax.swing.border.LineBorder;
import javax.swing.JSpinner;
import javax.swing.JCheckBox;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.awt.event.ActionEvent;
import javax.swing.SpinnerNumberModel;
import javax.swing.SpinnerDateModel;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.ArrayList;
import java.util.Calendar;
import javax.swing.JTable;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.border.CompoundBorder;
import javax.swing.border.BevelBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JSeparator;
import javax.swing.KeyStroke;
import java.awt.event.KeyEvent;
import java.awt.event.InputEvent;

public class MainWindow extends JFrame {

	private static final  int INITIAL_ADULT_VALUE = 1;
	private static final  int INITIAL_CHILD_VALUE = 0;
	private static final int INITIAL_NIGHT_VALUE = 1;
	
	private JPanel contentPane;
	private JPanel pnCenterGeneral;
	private URL hsURL;
	private HelpSet hs;
	private JPanel pnSouth;
	private JLabel lblWelcomeToViajaja;
	private JLabel lblYourBestJourney;
	private JButton btnBookAPackage;
	private JButton btnBookAnAccomodation;
	private JPanel pnHome;
	private JPanel pnSubTitle;
	private JPanel pnTitle;
	private JPanel pnAccomPackMenu;
	private JButton btnHome;
	private JPanel pnFilter;
	private JPanel pnAccommodation;
	private JScrollPane scAccomodationTable;
	private JPanel pnDestination;
	private JPanel pnDates;
	private JPanel pnAdultsChild;
	private JPanel pnAddons;
	private JPanel pnStars;
	private JPanel pnFilterButton;
	private JComboBox cbDestinations;
	private JPanel pnCheckIn;
	private JPanel pnCheckOut;
	private JLabel lblAdults;
	private JSpinner spAdults;
	private JLabel lblChildren;
	private JSpinner spChilds;
	private JCheckBox chBreakfast;
	private JCheckBox chLiftAvailable;
	private JCheckBox chPool;
	private JButton btnLetsGetStarted;
	private JPanel pnControlAcccomPackButtons;
	private JButton btnShoppingCart;
	private JSpinner spCheckIn;
	private JSpinner spCheckOut;
	private JTable tableAccomodations;
	private AccommodationReader accomodationReader;
	private JPanel pnGoBack;
	private JPanel pnGoToCard;
	private JTabbedPane tbTables;
	private JPanel pnPackages;
	private JScrollPane scPackages;
	private JTable tablePackages;
	private DefaultTableModel tableAccomodationModel;
	private MyMultipleLineCellRender multipleLineRender = new MyMultipleLineCellRender();
	private ParksReader parkReader;
	private MyTableButton buttonRender;
	private DefaultTableModel tablePackagesModel;
	private PackagesReader packagesReader;
	private JButton btnBuyThePark;
	private JPanel pnParks;
	private JScrollPane scParks;
	private JTable tableParks;
	private DefaultTableModel tableParkModel;
	private TicketsReader ticketsReader;
	private int childs;
	private int adults;
	private int nights;
	private JLabel lblCalendar1;
	private JLabel lblCalendar2;
	private String checkInDate;
	private JPanel pnGeneralCart;
	private JPanel pnControlCartButtons;
	private JButton btnContinue;
	private JButton btnProceed;
	private JPanel pnCurrentCart;
	private JPanel pnHomeAccPack;
	private JPanel pnCartTable;
	private JScrollPane scPane;
	private JTable tableCart;
	private JPanel pnTittle;
	private JLabel lblTittle;
	private DefaultTableModel tableCartModel;
	private ArrayList<CartObject> cart;
	private MyAccomodationReserveListener myButtonListener  = new MyAccomodationReserveListener();
	private MyTableListener myTableListener = new MyTableListener();
	private JLabel lblCartIcon;
	private JPanel pnBorderTitle;
	private JMenuBar menuBar;
	private JMenu mnSystem;
	private JMenu mnOptions;
	private HelpBroker hb;
	private JMenu mnHelp;
	private JMenuItem mntmExit;
	private JMenuItem mntmAbout;
	private JRadioButton rdOne;
	private JRadioButton rdTwo;
	private JRadioButton rdThree;
	private JRadioButton rdFour;
	private JRadioButton rdFive;
	private JRadioButton rdbtnAll;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JLabel lblStar;
	private JLabel lblCheckin;
	private JLabel lblCheckout;
	private JPanel pnCheckInSpiner;
	private JPanel pnCheckOutSpinner;
	private JLabel lblDestinations;
	private JPanel pnBlockTitle;
	private JPanel pnPhotoNorth;
	private JPanel pnPhotoSouth;
	private JLabel lblPhotoMenuNorth;
	private JLabel lblPhotoMenuSouth;
	private JPanel pnCenterTitle;
	private JMenuItem mntmContents;
	private JSeparator separator;
	private ResourceBundle textos;
	private Locale local;
	private JMenuItem mntmSpanish;
	private JMenuItem mntmEnglish;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {

					JFrame.setDefaultLookAndFeelDecorated(true);
					JDialog.setDefaultLookAndFeelDecorated(true);
					
					SubstanceLookAndFeel.setSkin("org.jvnet.substance.skin.MistSilverSkin");
					MainWindow frame = new MainWindow(new Locale("en"));
					frame.setLocationRelativeTo(null);
					frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
	}

	/**
	 * Create the frame.
	 */
	public MainWindow(Locale x) {
		updateText(x);
		ticketsReader = new TicketsReader(x);
		parkReader= new ParksReader(x);
		packagesReader = new PackagesReader(x);
		accomodationReader= new AccommodationReader(x);
		updateDiscount();
		cart = new ArrayList<CartObject>();
		adults=INITIAL_ADULT_VALUE;
		childs = INITIAL_CHILD_VALUE;
		nights = INITIAL_NIGHT_VALUE;
		buttonRender = new MyTableButton();
		setTitle(localizar("65"));
		setIconImage(Toolkit.getDefaultToolkit().getImage(MainWindow.class.getResource("/img/roller-coaster.jpg")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 750, 515);
		setJMenuBar(getMenuBar_1());
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new CardLayout(0, 0));
		contentPane.add(getPnHome(), "home");
		contentPane.add(getAccomodationMenu(), "menu");
		contentPane.add(getPnGeneralCart(), "cart");
		checkInDate = spCheckIn.getValue().toString();
		loadHelp();
		
		
	
	}

	public Object getCheckIn() {
		return spCheckIn.getValue();
	}

	private JPanel getPnCenterGeneral() {
		if (pnCenterGeneral == null) {
			pnCenterGeneral = new JPanel();
			pnCenterGeneral.setAlignmentX(Component.RIGHT_ALIGNMENT);
			pnCenterGeneral.setLayout(new BorderLayout(0, 0));
			pnCenterGeneral.add(getPnPhotoNorth(), BorderLayout.NORTH);
			pnCenterGeneral.add(getPnPhotoSouth(), BorderLayout.SOUTH);
			pnCenterGeneral.add(getPnCenterTitle(), BorderLayout.CENTER);
		}
		
		return pnCenterGeneral;
	}
	

	
	
	private JPanel getPnSouth() {
		if (pnSouth == null) {
			pnSouth = new JPanel();
			pnSouth.add(getBtnBookAnAccomodation());
			pnSouth.add(getBtnBookAPackage());
			pnSouth.add(getBtnBuyThePark());
		}
		return pnSouth;
	}

	public String getCheckInDate() {
		return checkInDate;
	}

	private JLabel getLblWelcomeToViajaja() {
		if (lblWelcomeToViajaja == null) {
			lblWelcomeToViajaja = new JLabel(localizar("1"));
			lblWelcomeToViajaja.setFont(new Font("Tahoma", Font.PLAIN, 30));
		}
		return lblWelcomeToViajaja;
	}
	private JLabel getLblYourBestJourney() {
		if (lblYourBestJourney == null) {
			lblYourBestJourney = new JLabel(localizar("2"));
			lblYourBestJourney.setFont(new Font("Tahoma", Font.PLAIN, 18));
		}
		return lblYourBestJourney;
	}
	
	/**
	 * this method create or return a button that loads the package table first
	 * @return btnBookAPackage
	 */
	private JButton getBtnBookAPackage() {
		if (btnBookAPackage == null) {
			btnBookAPackage = new JButton(localizar("4"));
			btnBookAPackage.setMnemonic(localizar("161").charAt(0));
			btnBookAPackage.setFont(new Font("Tahoma", Font.PLAIN, 15));
			btnBookAPackage.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) 
				{
					changeView("menu");
					enableComponentsPackages();
					tbTables.setSelectedIndex(1);
				}
			});
		}
		return btnBookAPackage;
	}

	/**
	 * this method create or return a button that loads the Accommodation table first
	 * @return btnBookAnAccomodation
	 */
	private JButton getBtnBookAnAccomodation() {
		if (btnBookAnAccomodation == null) {
			btnBookAnAccomodation = new JButton(localizar("3"));
			btnBookAnAccomodation.setMnemonic(localizar("160").charAt(0));
			btnBookAnAccomodation.setFont(new Font("Tahoma", Font.PLAIN, 15));
			btnBookAnAccomodation.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) 
				{
					changeView("menu");
					enableComponentsAccomodation();
					tbTables.setSelectedIndex(0);
					
					
				}
			});
		}
		return btnBookAnAccomodation;
	}
	
	private JPanel getPnHome() {
		if (pnHome == null) {
			pnHome = new JPanel();
			pnHome.setLayout(new BorderLayout(0, 0));
			pnHome.add(getPnCenterGeneral(), BorderLayout.CENTER);
			pnHome.add(getPnSouth(), BorderLayout.SOUTH);
		}
		return pnHome;
	}
	private JPanel getPnSubTitle() {
		if (pnSubTitle == null) {
			pnSubTitle = new JPanel();
			pnSubTitle.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
			pnSubTitle.add(getLblYourBestJourney());
		}
		return pnSubTitle;
	}
	private JPanel getPnTitle() {
		if (pnTitle == null) {
			pnTitle = new JPanel();
			FlowLayout fl_pnTitle = (FlowLayout) pnTitle.getLayout();
			fl_pnTitle.setVgap(30);
			fl_pnTitle.setHgap(2);
			pnTitle.add(getLblWelcomeToViajaja());
		}
		return pnTitle;
	}
	private JPanel getAccomodationMenu() {
		if (pnAccomPackMenu == null) {
			pnAccomPackMenu = new JPanel();
			pnAccomPackMenu.setLayout(new BorderLayout(0, 0));
			pnAccomPackMenu.add(getPnFilter(), BorderLayout.WEST);
			pnAccomPackMenu.add(getPnControlAcccomPackButtons(), BorderLayout.SOUTH);
			pnAccomPackMenu.add(getTbTables(), BorderLayout.CENTER);
		}
		return pnAccomPackMenu;
	}

	/**
	 * this method create or return a button that return to the home menu
	 * @return btnHome
	 */
	private JButton getBtnHome() {
		if (btnHome == null) {
			btnHome = new JButton(localizar("31"));getContentPane();
			btnHome.setMnemonic(localizar("170").charAt(0));
			btnHome.setFont(new Font("Tahoma", Font.PLAIN, 13));
			btnHome.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) 
				{
					changeView("home");
				}
			});
		}
		return btnHome;
	}
	
	/**
	 * Abstract method to change between the view of the cardlayout
	 * @param view, view of the cardlayout
	 */
	public void changeView(String view)
	{
		switch(view)
		{
		case("home"):
			((CardLayout)contentPane.getLayout()).show(contentPane,"home");
			break;
		case("menu"): 
			((CardLayout)contentPane.getLayout()).show(contentPane,"menu");
			break;
		case("cart"):
			((CardLayout)contentPane.getLayout()).show(contentPane,"cart");
			break;
		}
		
	}
	
	private JPanel getPnFilter() {
		if (pnFilter == null) {
			pnFilter = new JPanel();
			pnFilter.setLayout(new GridLayout(0, 1, 0, 0));
			pnFilter.add(getPnDestination());
			pnFilter.add(getPnDates());
			pnFilter.add(getPnAdultsChild());
			pnFilter.add(getPnAddons());
			pnFilter.add(getPnStars());
			pnFilter.add(getPnFilterButton());
		}
		return pnFilter;
	}
	
	private JPanel getPnAccommodation() {
		if (pnAccommodation == null) {
			pnAccommodation = new JPanel();
			pnAccommodation.setLayout(new BorderLayout(0, 0));
			pnAccommodation.add(getScAccomodationTable(), BorderLayout.CENTER);
		}
		return pnAccommodation;
	}
	
	private JScrollPane getScAccomodationTable() {
		if (scAccomodationTable == null) {
			scAccomodationTable = new JScrollPane();
			scAccomodationTable.setViewportView(getTableAccomodations());
		}
		return scAccomodationTable;
	}
	
	private JPanel getPnDestination() {
		if (pnDestination == null) {
			pnDestination = new JPanel();
			pnDestination.setBorder(new CompoundBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)), new EmptyBorder(15, 0, 0, 0)));
			pnDestination.add(getLblDestinations());
			pnDestination.add(getCbDestinations());
		}
		return pnDestination;
	}
	
	private JPanel getPnDates() {
		if (pnDates == null) {
			pnDates = new JPanel();
			pnDates.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			pnDates.setLayout(new GridLayout(0, 2, 0, 0));
			pnDates.add(getPnCheckIn());
			pnDates.add(getPnCheckOut());
		}
		return pnDates;
	}
	
	private JPanel getPnAdultsChild() {
		if (pnAdultsChild == null) {
			pnAdultsChild = new JPanel();
			pnAdultsChild.setBorder(new CompoundBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)), new EmptyBorder(10, 0, 0, 0)));
			pnAdultsChild.add(getLblAdults());
			pnAdultsChild.add(getSpAdults());
			pnAdultsChild.add(getLblChildren());
			pnAdultsChild.add(getSpChilds());
		}
		return pnAdultsChild;
	}
	
	private JPanel getPnAddons() {
		if (pnAddons == null) {
			pnAddons = new JPanel();
			pnAddons.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), localizar("14"), TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
			pnAddons.add(getChBreakfast());
			pnAddons.add(getChLiftAvailable());
			pnAddons.add(getChPool());
		}
		return pnAddons;
	}
	
	private JPanel getPnStars() {
		if (pnStars == null) {
			pnStars = new JPanel();
			pnStars.setBorder(new TitledBorder(null, localizar("147"), TitledBorder.LEADING, TitledBorder.TOP, null, null));
			pnStars.add(getLblStar());
			pnStars.add(getRdbtnAll());
			pnStars.add(getRdOne());
			pnStars.add(getRdTwo());
			pnStars.add(getRdThree());
			pnStars.add(getRdFour());
			pnStars.add(getRdFive());
		}
		return pnStars;
	}
	
	private JPanel getPnFilterButton() {
		if (pnFilterButton == null) {
			pnFilterButton = new JPanel();
			pnFilterButton.setBorder(new CompoundBorder(null, new EmptyBorder(10, 0, 0, 0)));
			pnFilterButton.add(getBtnLetsGetStarted());
		}
		return pnFilterButton;
	}
	
	/**
	 * This method return a combo box with the possible countries destinations
	 * @return JCombobox
	 */
	private JComboBox getCbDestinations() {
		if (cbDestinations == null) {
			cbDestinations = new JComboBox();
			cbDestinations.setToolTipText(localizar("205"));
			cbDestinations.addItem(localizar("30"));
			for(String country: parkReader.getArrayOfCountries())
			{
				cbDestinations.addItem(country);
			}
			
		}
		return cbDestinations;
	}
	
	private JPanel getPnCheckIn() {
		if (pnCheckIn == null) {
			pnCheckIn = new JPanel();
			pnCheckIn.setBorder(null);
			pnCheckIn.setLayout(new GridLayout(2, 1, 0, 0));
			pnCheckIn.add(getLblCheckin());
			pnCheckIn.add(getPnCheckInSpiner());
		}
		return pnCheckIn;
	}
	
	private JPanel getPnCheckOut() {
		if (pnCheckOut == null) {
			pnCheckOut = new JPanel();
			pnCheckOut.setBorder(null);
			pnCheckOut.setLayout(new GridLayout(2, 1, 0, 0));
			pnCheckOut.add(getLblCheckout());
			pnCheckOut.add(getPnCheckOutSpinner());
		}
		return pnCheckOut;
	}
	
	private JLabel getLblAdults() {
		if (lblAdults == null) {
			lblAdults = new JLabel(localizar("12"));
			lblAdults.setDisplayedMnemonic(localizar("166").charAt(0));
			lblAdults.setLabelFor(getSpAdults());
			lblAdults.setFont(new Font("Tahoma", Font.PLAIN, 14));
		}
		return lblAdults;
	}
	
	/**
	 * This method return the spinner the number of adults
	 * @return JSpinner spAdults
	 */
	private JSpinner getSpAdults() {
		if (spAdults == null) {
			spAdults = new JSpinner();
			spAdults.setModel(new SpinnerNumberModel(new Integer(1), new Integer(0), null, new Integer(1)));
			spAdults.setPreferredSize(new Dimension(60,20));
		}
		return spAdults;
	}
	
	private JLabel getLblChildren() {
		if (lblChildren == null) {
			lblChildren = new JLabel(localizar("13"));
			lblChildren.setDisplayedMnemonic(localizar("167").charAt(0));
			lblChildren.setLabelFor(getSpChilds());
			lblChildren.setFont(new Font("Tahoma", Font.PLAIN, 14));
		}
		return lblChildren;
	}
	/**
	 * This method return the JSpinner that is used to know the number of kids
	 * @return JSpiner spChilds
	 */
	private JSpinner getSpChilds() {
		if (spChilds == null) {
			spChilds = new JSpinner();
			spChilds.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));
			spChilds.setPreferredSize(new Dimension(60,20));
		}
		return spChilds;
	}
	
	/**
	 * This method create or return a JCheckBox that is used to know if the user want to include breakfast
	 * @return JCheckBox chBreakfast
	 */
	protected JCheckBox getChBreakfast() {
		if (chBreakfast == null) {
			chBreakfast = new JCheckBox(localizar("15"));
			chBreakfast.setMnemonic(localizar("168").charAt(0));
			chBreakfast.setFont(new Font("Tahoma", Font.PLAIN, 14));
		}
		return chBreakfast;
	}
	
	/**
	 * This method return a check box to know if the user wants a room near a lift
	 * @return JCheckBox chLiftAvailable
	 */
	private JCheckBox getChLiftAvailable() {
		if (chLiftAvailable == null) {
			chLiftAvailable = new JCheckBox(localizar("16"));
			chLiftAvailable.setMnemonic(localizar("171").charAt(0));
			chLiftAvailable.setFont(new Font("Tahoma", Font.PLAIN, 14));
		}
		return chLiftAvailable;
	}
	
	/**
	 * This method return or create JCheckBox to know if the user want a facility with pool 
	 * @return JCheckBox chPool
	 */
	private JCheckBox getChPool() {
		if (chPool == null) {
			chPool = new JCheckBox(localizar("17"));
			chPool.setMnemonic(localizar("172").charAt(0));
			chPool.setFont(new Font("Tahoma", Font.PLAIN, 14));
		}
		return chPool;
	}
	
	/**
	 * This method return or create a JButton that if you clicked the tables are updated with the filter data
	 * @return JButton btnLetsGetStarted
	 */
	private JButton getBtnLetsGetStarted() {
		if (btnLetsGetStarted == null) {
			btnLetsGetStarted = new JButton(localizar("18"));
			btnLetsGetStarted.setToolTipText(localizar("204"));
			btnLetsGetStarted.setMnemonic(localizar("169").charAt(0));
			btnLetsGetStarted.setFont(new Font("Tahoma", Font.PLAIN, 13));
			btnLetsGetStarted.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) 
				{
					
				
					long difference = ((Date) spCheckOut.getValue()).getTime() - ((Date) spCheckIn.getValue()).getTime();
					nights = (int) (difference / (24 * 60 * 60 * 1000));
					
					
					if(nights>0 || tbTables.getSelectedIndex()!= 0)
					{
						
						checkInDate = spCheckIn.getValue().toString();
						updateTables();
					}
					else
					{
						JOptionPane.showMessageDialog(getMainWindow(), localizar("140"));
						spCheckIn.grabFocus();
						spCheckIn.setValue(getCurrentDay());

						Calendar c = Calendar.getInstance();
						c.setTime((Date)spCheckIn.getValue());
						c.add(Calendar.DATE, 1);
						Date out = c.getTime();
						spCheckOut.setValue(out);
					}
					
				}

				
			});
		}
		return btnLetsGetStarted;
	}
	
	/**
	 * This method update the attributes of child's and adult, and it also call the method to update the tables
	 */
	private void updateTables() 
	{
		childs = Integer.valueOf(spChilds.getValue().toString());
		adults = Integer.valueOf(spAdults.getValue().toString());
		
		fillAccomodationTable();
		fillPackageTable();
		fillParkTable();
		if(childs + adults <=0)
		{
			JOptionPane.showMessageDialog(this, localizar("143"));
		}
		
	}
	
	
	private JPanel getPnControlAcccomPackButtons() {
		if (pnControlAcccomPackButtons == null) {
			pnControlAcccomPackButtons = new JPanel();
			pnControlAcccomPackButtons.setLayout(new GridLayout(0, 2, 0, 0));
			pnControlAcccomPackButtons.add(getPnGoBack());
			pnControlAcccomPackButtons.add(getPnGoToCard());
		}
		return pnControlAcccomPackButtons;
	}
	
	/**
	 * This method return the JButton that if you click in it change the view of the cardlayout to the one of the shopping cart
	 * @return JButton btnShoppingCart
	 */
	private JButton getBtnShoppingCart() {
		if (btnShoppingCart == null) {
			btnShoppingCart = new JButton(localizar("32"));
			btnShoppingCart.setMnemonic(localizar("174").charAt(0));;
			btnShoppingCart.setFont(new Font("Tahoma", Font.PLAIN, 13));
			btnShoppingCart.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) 
				{
					changeView("cart");
					fillCartTable();
					if(cart.isEmpty())
					{
						btnProceed.setEnabled(false);
					}
					else
					{
						btnProceed.setEnabled(true);
					}
				}
			});
			btnShoppingCart.setHorizontalAlignment(SwingConstants.RIGHT);
		}
		return btnShoppingCart;
	}
	
	/**
	 * This method return the JSpinner used to know when the user want to do the check in
	 * @return JSpinner spCheckIn
	 */
	private JSpinner getSpCheckIn() {
		if (spCheckIn == null) {
			spCheckIn = new JSpinner();
			spCheckIn.setPreferredSize(new Dimension(85, 20));
			Date y = getCurrentDay();
			
			Calendar c = Calendar.getInstance();
			c.setTime(y);
			c.add(Calendar.DATE, -1);
			Date min = c.getTime();
			
			spCheckIn.setModel(new SpinnerDateModel(y, min, null, Calendar.DAY_OF_YEAR));
			
			spCheckIn.setEditor(new JSpinner.DateEditor(spCheckIn, "dd/MM/yyyy"));
			
		}
		return spCheckIn;
	}

	/**
	 * This method calculate the current date
	 * @return Date, the current date
	 */
	public Date getCurrentDay() {
		Calendar c = Calendar.getInstance();
		Date x = c.getTime();
		return x;
	}
	
	/**
	 * This method scaled a photo with the correspondent component
	 * @param comp, component
	 * @param path, path of the photo
	 */
	public void scaledImage(JLabel  comp, String path) {
		Image originalImage = (new ImageIcon(getClass().getResource(path))).getImage();
		
		Image finalImage = originalImage.getScaledInstance((int)comp.getPreferredSize().getWidth(),  (int)comp.getPreferredSize().getHeight(), Image.SCALE_SMOOTH);
		
		ImageIcon scaledIcon = new ImageIcon();
		scaledIcon.setImage(finalImage);
		
		comp.setIcon(scaledIcon);
	}
	
	
	public ImageIcon scaledTable(JTable  table, String path) {
		Image originalImage = (new ImageIcon(getClass().getResource(path))).getImage();
		
		Image finalImage = originalImage.getScaledInstance(table.getColumnModel().getColumn(1).getWidth(),  table.getRowHeight(), Image.SCALE_SMOOTH);
		
		ImageIcon scaledIcon = new ImageIcon();
		scaledIcon.setImage(finalImage);
		
		return scaledIcon;
	}
	
	
	public AccommodationReader getAccomodationReader() {
		return accomodationReader;
	}

	public ParksReader getParkReader() {
		return parkReader;
	}

	public PackagesReader getPackagesReader() {
		return packagesReader;
	}

	public TicketsReader getTicketsReader() {
		return ticketsReader;
	}

	/**
	 * Method that return the JSpinner used to know when user want to do the check out
	 * @return JSpinner spCheckOut
	 */
	private JSpinner getSpCheckOut() {
		if (spCheckOut == null) {
			spCheckOut = new JSpinner();
			spCheckOut.setPreferredSize(new Dimension(85, 20));
			spCheckOut.setToolTipText(localizar("213"));
			Date y = getCurrentDay();
			
			Calendar c = Calendar.getInstance();
			c.setTime(y);
			c.add(Calendar.DATE, 1);
			Date out = c.getTime();
			
			spCheckOut.setModel(new SpinnerDateModel(out, y, new Date(1545260400000L), Calendar.DAY_OF_YEAR));
			spCheckOut.setEditor(new JSpinner.DateEditor(spCheckOut, "dd/MM/yyyy"));
		}
		return spCheckOut;
	}
	
	
	public int getNights() {
		return nights;
	}

	/**
	 * this method creates and return the table of accommodation
	 * @return JTable tableAcccomodatons
	 */
	private JTable getTableAccomodations() {
		if (tableAccomodations == null) {
			tableAccomodationModel = new NonEditableAndPhoto();
			
			tableAccomodationModel.addColumn(localizar("22"));
			tableAccomodationModel.addColumn(localizar("23"));
			tableAccomodationModel.addColumn(localizar("24"));
			tableAccomodationModel.addColumn(localizar("25"));
			tableAccomodationModel.addColumn(localizar("26"));
			tableAccomodationModel.addColumn("");
			
			tableAccomodations = new JTable(tableAccomodationModel);
			
			
			tableAccomodations.setCellSelectionEnabled(true);
			tableAccomodations.addMouseListener(myTableListener);			
			
			tableAccomodations.getColumnModel().getColumn(3).setPreferredWidth(100);
			tableAccomodations.setRowHeight(tableAccomodations.getRowHeight()*4);
			
			tableAccomodations.getColumnModel().getColumn(0).setCellRenderer( multipleLineRender);
			tableAccomodations.getColumnModel().getColumn(2).setCellRenderer( multipleLineRender);
			tableAccomodations.getColumnModel().getColumn(3).setCellRenderer( multipleLineRender);
			tableAccomodations.getColumnModel().getColumn(4).setCellRenderer( multipleLineRender);
			tableAccomodations.getColumnModel().getColumn(5).setCellRenderer( buttonRender);
			tableAccomodations.setFillsViewportHeight(true);
			
		}
		fillAccomodationTable();
		
		return tableAccomodations;
		
	}
	
	/**
	 * This method fill the accommodation table with the information in the accommodaion file
	 */
	private void fillAccomodationTable() {
		
		
		tableAccomodationModel.getDataVector().clear();
		tableAccomodationModel.fireTableDataChanged();
		
		for (int i = 0; i< accomodationReader.getListOfAccomodations().size(); i++)
		{
			String additionaInfo = "";
			
			MyClickedButton reserve = new MyClickedButton(localizar("28"), this);
			reserve.setToolTipText(localizar("206"));
			reserve.addActionListener(myButtonListener);
			
			
			Accommodation x = accomodationReader.getListOfAccomodations().get(i);
			Park y = parkReader.searchPark(x.getThemeParkCode());
			
			if(y!= null)
			{
				 additionaInfo = y.accomodationAdditionalInformation(local);
			}
			
			
			checkMinPerson(reserve, adults, childs);
			
			if(chBreakfast.isSelected() && !x.getType().equals("HO"))
			{
				continue;
			}
					
			
			
			if(x.getCapacity() > (adults+childs) && (rdbtnAll.isSelected()|| getSelectedCategory() == x.getCategory() )
					&& (cbDestinations.getSelectedItem().equals(localizar("30")) || cbDestinations.getSelectedItem().equals(y.getCountry()) )  )
			{
				double finalPrice = x.getFinalDialogPrice(childs+adults);
				if ( chBreakfast.isSelected())
				{
					finalPrice+= (10* finalPrice)/100;
				}
				reserve.setClicked(x);
				
				
				Object[] myRow = { x.getFacilityName(), scaledTable(tableAccomodations, x.getPhotoPath()) , x.getDataInfo(local), additionaInfo, String.valueOf(finalPrice)+ localizar("203")  , reserve};
				tableAccomodationModel.addRow(myRow);
				
			}
		}
		
	}

	/**
	 * This method detects the star category of the radio-buttons and transform it into a integer
	 * @return int category
	 */
	private int getSelectedCategory() {
		if(rdOne.isSelected())
			return 1;
		else if(rdTwo.isSelected())
			return 2;
		else if(rdThree.isSelected())
			return 3;
		else if(rdFour.isSelected())
			return 4;
		else if(rdFive.isSelected())
			return 5;
		else
			return 0;
	}

	/**
	 * This method check if there is a minimum quantity of people, if not a told JButton is dissable
	 * @param reserve, the button to enable/dissable
	 * @param adults
	 * @param childs
	 */
	private void checkMinPerson(MyClickedButton reserve, int adults, int childs) {
		if(adults +childs <= 0)
		{
			reserve.setEnabled(false);
		}
		else
		{
			reserve.setEnabled(true);
		}
	}

	private JPanel getPnGoBack() {
		if (pnGoBack == null) {
			pnGoBack = new JPanel();
			pnGoBack.setBorder(null);
			pnGoBack.add(getBtnHome());
		}
		return pnGoBack;
	}
	
	
	private JPanel getPnGoToCard() {
		if (pnGoToCard == null) {
			pnGoToCard = new JPanel();
			pnGoToCard.setBorder(null);
			pnGoToCard.add(getBtnShoppingCart());
		}
		return pnGoToCard;
	}
	
	
	private JTabbedPane getTbTables() {
		if (tbTables == null) {
			tbTables = new JTabbedPane(JTabbedPane.TOP);
			tbTables.addTab(localizar("19"), null, getPnAccommodation(), null);
			tbTables.addTab(localizar("20"), null, getPnPackages(), null);
			tbTables.addTab(localizar("21"), null, getPnParks(), null);
			tbTables.addChangeListener(new ChangeListener() {
		        public void stateChanged(ChangeEvent e) {
		            switch(tbTables.getSelectedIndex())
		            {
		            case(0): enableComponentsAccomodation();
		            		break;
		            case(1): enableComponentsPackages();
		            		break;
		            case(2): enableComponentsParks();
		            		break;
		            }
		        }
		    });
		}
		return tbTables;
	}
	
	/**
	 * This method enable or disable the components of the filter in order to match with the current tab
	 */
	private void enableComponentsAccomodation()
	{
		
	
		spCheckIn.setEnabled(true);
		spCheckOut.setEnabled(true);
		chBreakfast.setEnabled(true);
		chLiftAvailable.setEnabled(true);
		chPool.setEnabled(true);
		rdbtnAll.setEnabled(true);
		rdOne.setEnabled(true);
		rdTwo.setEnabled(true);
		rdThree.setEnabled(true);
		rdFour.setEnabled(true);
		rdFive.setEnabled(true);
		
		
	}

	/**
	 * This method enable or disable the components of the filter in order to match with the current tab
	 */
	private void enableComponentsPackages()
	{
		
		
		spCheckIn.setEnabled(true);
		spCheckOut.setEnabled(false);
		chBreakfast.setEnabled(true);
		chLiftAvailable.setEnabled(true);
		chPool.setEnabled(true);
		rdbtnAll.setEnabled(true);
		rdOne.setEnabled(true);
		rdTwo.setEnabled(true);
		rdThree.setEnabled(true);
		rdFour.setEnabled(true);
		rdFive.setEnabled(true);
		
		
		
	}

	/**
	 * This method enable or disable the components of the filter in order to match with the current tab
	 */
	private void enableComponentsParks()
	{
		spCheckIn.setEnabled(true);
		spCheckOut.setEnabled(false);
		chBreakfast.setEnabled(false);
		chLiftAvailable.setEnabled(false);
		chPool.setEnabled(false);
		rdbtnAll.setEnabled(false);
		rdOne.setEnabled(false);
		rdTwo.setEnabled(false);
		rdThree.setEnabled(false);
		rdFour.setEnabled(false);
		rdFive.setEnabled(false);
		
	}
	
	
	private JPanel getPnPackages() {
		if (pnPackages == null) {
			pnPackages = new JPanel();
			pnPackages.setLayout(new BorderLayout(0, 0));
			pnPackages.add(getScPackages());
		}
		return pnPackages;
	}
	private JScrollPane getScPackages() {
		if (scPackages == null) {
			scPackages = new JScrollPane();
			scPackages.setViewportView(getTablePackages());
		}
		return scPackages;
	}
	
	/**
	 * This method return the table packages 
	 * @return JTable tablePackages
	 */
	private JTable getTablePackages() {
		if (tablePackages == null) {
			
			tablePackagesModel = new NonEditableAndPhoto();
			tablePackagesModel.addColumn(localizar("22"));
			tablePackagesModel.addColumn(localizar("23"));
			tablePackagesModel.addColumn(localizar("24"));
			tablePackagesModel.addColumn(localizar("25"));
			tablePackagesModel.addColumn(localizar("26"));
			tablePackagesModel.addColumn("");
			
			tablePackages = new JTable(tablePackagesModel);
			tablePackages.setCellSelectionEnabled(true);
			tablePackages.addMouseListener(myTableListener);

			tablePackages.getColumnModel().getColumn(3).setPreferredWidth(100);
			
			tablePackages.getColumnModel().getColumn(0).setCellRenderer( multipleLineRender);
			
			tablePackages.getColumnModel().getColumn(2).setCellRenderer( multipleLineRender);
			
			tablePackages.getColumnModel().getColumn(3).setCellRenderer( multipleLineRender);
			tablePackages.getColumnModel().getColumn(4).setCellRenderer( multipleLineRender);
			tablePackages.setRowHeight((int) (tablePackages.getRowHeight()*3.5));
			tablePackages.getColumnModel().getColumn(5).setCellRenderer( buttonRender);
			tablePackages.setFillsViewportHeight(true);
			
		}
		
		
		fillPackageTable();
		return tablePackages;
	}
	
	/**
	 * This method update or fill the table of packages 
	 */
	private void fillPackageTable() {
		
		tablePackagesModel.getDataVector().clear();
		tablePackagesModel.fireTableDataChanged();
		
		for (int i = 0; i< packagesReader.getListOfPackages().size(); i++)
		{	
			MyClickedButton reserve = new MyClickedButton(localizar("28"), this);
			reserve.setToolTipText(localizar("206"));
			reserve.addActionListener(myButtonListener);
			String additionaInfo = "";
			
			Package x = packagesReader.getListOfPackages().get(i);
			Accommodation y = accomodationReader.searchAccomoadtion(x.getAccommodationCode());
			
			if(y!= null)
			{
				 additionaInfo = y.packageAdditionalInformation(local);
			}
			
			
			checkMinPerson(reserve, adults, childs);

			if(chBreakfast.isSelected() && !y.getType().equals("HO"))
			{
				continue;
			}
			
			if(y.getCapacity() >= (adults+childs) && (cbDestinations.getSelectedItem().equals(localizar("30")) || 
					cbDestinations.getSelectedItem().equals(parkReader.getCountry(y.getThemeParkCode())))
					&&(rdbtnAll.isSelected() || getSelectedCategory() == y.getCategory())  )
			{
				double finalPrice = x.getFinalPrice(adults,childs);
				
				if ( chBreakfast.isSelected())
				{
					finalPrice+= (10* finalPrice)/100;
				}
				reserve.setClicked(x);

				
				Object[] myRow = { x.getPackageName(), scaledTable(tablePackages, x.getPhotoPath()), x.getDataInfo(local), additionaInfo, String.valueOf(finalPrice) + localizar("203") , reserve};
				tablePackagesModel.addRow(myRow);
			}
		}
		
	}
	
	
	private JButton getBtnBuyThePark() {
		if (btnBuyThePark == null) {
			btnBuyThePark = new JButton(localizar("5"));
			btnBuyThePark.setMnemonic(localizar("162").charAt(0));
			btnBuyThePark.setFont(new Font("Tahoma", Font.PLAIN, 15));
			btnBuyThePark.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) 
				{
					changeView("menu");
					tbTables.setSelectedIndex(2);
					enableComponentsParks();
					
					
				}
			});
		}
		return btnBuyThePark;
	}
	private JPanel getPnParks() {
		if (pnParks == null) {
			pnParks = new JPanel();
			pnParks.setLayout(new BorderLayout(0, 0));
			pnParks.add(getScParks(), BorderLayout.CENTER);
		}
		return pnParks;
	}
	private JScrollPane getScParks() {
		if (scParks == null) {
			scParks = new JScrollPane();
			scParks.setViewportView(getTableParks());
		}
		return scParks;
	}
	
	/**
	 * This method return a table with park articles
	 * @return Jtable tableParks
	 */
	private JTable getTableParks() {
		if (tableParks == null) {
			
			tableParkModel = new NonEditableAndPhoto();
			tableParkModel.addColumn(localizar("22"));
			tableParkModel.addColumn(localizar("23"));
			tableParkModel.addColumn(localizar("27"));
			tableParkModel.addColumn(localizar("26"));
			tableParkModel.addColumn("");
			
			
			tableParks = new JTable(tableParkModel);
			
			tableParks.setCellSelectionEnabled(true);
			tableParks.addMouseListener(myTableListener);
			
			tableParks.getColumnModel().getColumn(0).setCellRenderer( multipleLineRender);		
			tableParks.getColumnModel().getColumn(2).setCellRenderer(multipleLineRender);
			tableParks.getColumnModel().getColumn(3).setCellRenderer(multipleLineRender);
			tableParks.getColumnModel().getColumn(4).setCellRenderer(buttonRender);
		
			
			
		}
		fillParkTable();
		return tableParks;
	}
	
	/**
	 * This method fill the table of park
	 */
	private void fillParkTable() {
		
		tableParkModel.getDataVector().clear();
		tableParkModel.fireTableDataChanged();
		
		for (int i = 0; i< parkReader.getListOfParks().size(); i++)
		{	
			MyClickedButton buy = new MyClickedButton(localizar("29"), this);
			buy.addActionListener(myButtonListener);
			

			buy.setToolTipText(localizar("206"));
			Park x = parkReader.getListOfParks().get(i);
			
			
			
			checkMinPerson(buy, adults, childs);
			
			if((cbDestinations.getSelectedItem().equals(localizar("30")) || cbDestinations.getSelectedItem().equals(x.getCountry())) )
			{
				try
				{	
					
					buy.setClicked(x);
					ImageIcon photo = new ImageIcon(getClass().getResource(x.getPhotoPath()));
					Object[] myRow = { x.getNamePark(),photo, x.getDescription(), ticketsReader.searchByPark(x.getCode()).getFinalPrice(adults, childs,local) , buy};
					tableParkModel.addRow(myRow);
				}catch(Exception e)
				{
					System.out.println("Error  while finding the ticket");
				}
			}
		}
		
	}
	
	
	public int getChilds() {
		return childs;
	}

	public int getAdults() {
		return adults;
	}



	
	
	
	private JLabel getLblCalendar1() {
		if (lblCalendar1 == null) {
			lblCalendar1 = new JLabel("");
			lblCalendar1.setPreferredSize(new Dimension(15,15));
			scaledImage(lblCalendar1,"/img/calendar.jpg" );
			
		}
		return lblCalendar1;
	}
	private JLabel getLblCalendar2() {
		if (lblCalendar2 == null) {
			lblCalendar2 = new JLabel("");
			lblCalendar2.setPreferredSize(new Dimension(15,15));
			scaledImage(lblCalendar2,"/img/calendar.jpg" );
		}
		return lblCalendar2;
	}
	private JPanel getPnGeneralCart() {
		if (pnGeneralCart == null) {
			pnGeneralCart = new JPanel();
			pnGeneralCart.setLayout(new BorderLayout(0, 0));
			pnGeneralCart.add(getPnControlCartButtons(), BorderLayout.SOUTH);
			pnGeneralCart.add(getPnCurrentCart(), BorderLayout.CENTER);
			pnGeneralCart.add(getPnHomeAccPack(), BorderLayout.NORTH);
		}
		return pnGeneralCart;
	}
	private JPanel getPnControlCartButtons() {
		if (pnControlCartButtons == null) {
			pnControlCartButtons = new JPanel();
			pnControlCartButtons.add(getBtnContinue());
			pnControlCartButtons.add(getBtnProceed());
		}
		return pnControlCartButtons;
	}
	private JButton getBtnContinue() {
		if (btnContinue == null) {
			btnContinue = new JButton(localizar("53"));
			btnContinue.setMnemonic(localizar("173").charAt(0));
			btnContinue.setFont(new Font("Tahoma", Font.PLAIN, 14));
			btnContinue.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) 
				{

					changeView("menu");
				}
			});
		}
		return btnContinue;
	}
	
	private MainWindow getMainWindow()
	{
		return this;
	}
	private JButton getBtnProceed() {
		if (btnProceed == null) {
			btnProceed = new JButton(localizar("54"));
			btnProceed.setMnemonic(localizar("172").charAt(0));
			btnProceed.setFont(new Font("Tahoma", Font.PLAIN, 14));
			btnProceed.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) 
				{
					ConfirmationDialog cD = new ConfirmationDialog(getMainWindow());
					cD.setLocationRelativeTo(getMainWindow());
					cD.setModal(true);
					cD.setVisible(true);
				}
			});
		}
		return btnProceed;
	}
	private JPanel getPnCurrentCart() {
		if (pnCurrentCart == null) {
			pnCurrentCart = new JPanel();
			pnCurrentCart.setLayout(new BorderLayout(0, 0));
			pnCurrentCart.add(getPnCartTable(), BorderLayout.CENTER);
			pnCurrentCart.add(getPnTittle(), BorderLayout.NORTH);
		}
		return pnCurrentCart;
	}
	private JPanel getPnHomeAccPack() {
		if (pnHomeAccPack == null) {
			pnHomeAccPack = new JPanel();
		}
		return pnHomeAccPack;
	}
	private JPanel getPnCartTable() {
		if (pnCartTable == null) {
			pnCartTable = new JPanel();
			pnCartTable.setLayout(new BorderLayout(0, 0));
			pnCartTable.add(getScPane(), BorderLayout.CENTER);
		}
		return pnCartTable;
	}
	private JScrollPane getScPane() {
		if (scPane == null) {
			scPane = new JScrollPane();
			scPane.setViewportView(getTableCart());
		}
		return scPane;
	}
	/**
	 * This method return the cart table
	 * @return JTable tableCart
	 */
	private JTable getTableCart() {
		if (tableCart == null) {
				tableCartModel = new NonEditableAndPhoto();
				
				tableCartModel.addColumn("");
				tableCartModel.addColumn(localizar("23"));
				tableCartModel.addColumn(localizar("24"));
				tableCartModel.addColumn(localizar("51"));
				tableCartModel.addColumn(localizar("26"));
				tableCartModel.addColumn("");
				

				tableCart = new JTable(tableCartModel);
				
				
				tableCart.setCellSelectionEnabled(true);
				tableCart.addMouseListener(myTableListener);			
				tableCart.getColumnModel().getColumn(3).setPreferredWidth((int)(tableCart.getColumnModel().getColumn(3).getPreferredWidth()*1.8));
				tableCart.setRowHeight((int) (tableCart.getRowHeight()*3.5));
				tableCart.getColumnModel().getColumn(0).setCellRenderer( buttonRender);
				tableCart.getColumnModel().getColumn(2).setCellRenderer( multipleLineRender);
				tableCart.getColumnModel().getColumn(3).setCellRenderer( multipleLineRender);
				tableCart.getColumnModel().getColumn(5).setCellRenderer( buttonRender);
				
				tableCart.setFillsViewportHeight(true);
				
			
		}
		
		fillCartTable();
		return tableCart;
	}
	
	/**
	 * This method fill the cart table
	 */
	private void fillCartTable() 
	{
		tableCartModel.getDataVector().clear();
		tableCartModel.fireTableDataChanged();
		
		for(CartObject x : cart)
		{
			MyClickedButton remove = new MyClickedButton(localizar("52"),this);
			MyClickedButton edit = new MyClickedButton(localizar("7"),this);
			remove.setCartObject(x);
			edit.setCartObject(x);
			edit.setToolTipText(localizar("207"));
			
			edit.addActionListener(new MyEditButtonListener());
			remove.setToolTipText(localizar("208"));
			remove.addActionListener(new MyRemoveButtonListener());
			Object[] rowInfo = {remove,"", x.getDescription(), x.getUserData(local), x.getPrice(), edit};
			tableCartModel.addRow(rowInfo);
		}
		
		if (cart.isEmpty())
		{
			btnProceed.setEnabled(false);
		}
		else
		{
			btnProceed.setEnabled(true);
		}

		
	}

	private JPanel getPnTittle() {
		if (pnTittle == null) {
			pnTittle = new JPanel();
			pnTittle.add(getPnBorderTitle());
		}
		return pnTittle;
	}
	private JLabel getLblTittle() {
		if (lblTittle == null) {
			lblTittle = new JLabel(localizar("55"));
			lblTittle.setFont(new Font("Tahoma", Font.PLAIN, 24));
		}
		return lblTittle;
	}
	
	/**
	 * Method that return a listener depending on the a button pressed, this work passing a string
	 * @param selector
	 * @param currentDialog
	 * @return
	 * @throws Exception
	 */
	public ActionListener getMyCartButtonListner(String selector, MyCreatedDialogs currentDialog) throws Exception
	{
		switch(selector) 
		{
			case "continue": return new MyBuyContinueButton(currentDialog);
			case "cart": return new MyBuyGoCartGoButton(currentDialog);
			default: throw new Exception(localizar("142"));
		}
	}
	
	
	/**
	 * This inner class goal is to open a dialog form depending on the type of object. 
	 * To use this class is needed a special JButton class extended
	 * @author Hp
	 *
	 */
	private class MyAccomodationReserveListener implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent e) 
		{
			MyClickedButton myButton = (MyClickedButton)e.getSource();
			TypesOfBuy theClickedObject = myButton.getClicked();
			
			if(theClickedObject instanceof Accommodation)
			{
				
				AccommodationDialog aD =new AccommodationDialog( myButton.getMainWindow(), (Accommodation) theClickedObject);
				aD.setLocationRelativeTo(myButton.getMainWindow());
				aD.setModal(true);
				aD.setVisible(true);
			}
			else if (theClickedObject instanceof Package)
			{
				PackageDialog pD = new PackageDialog(myButton.getMainWindow(), (Package) theClickedObject);
				pD.setLocationRelativeTo(myButton.getMainWindow());
				pD.setModal(true);
				pD.setVisible(true);
						
			}
			else if(theClickedObject instanceof Park)
			{
				ParkDialog rD = new ParkDialog(myButton.getMainWindow(), (Park) theClickedObject);
				rD.setLocationRelativeTo(myButton.getMainWindow());
				rD.setModal(true);
				rD.setVisible(true);
			}
			
			
		}
		
	}
	
	/**
	 * this inner class is used to adapt a mouse listener used on the Jtables to click on the buttons
	 * @author Hp
	 *
	 */
	private class MyTableListener extends MouseAdapter
	{

		@Override
		public void mouseClicked(MouseEvent arg0) {
			JTable theTable = (JTable) arg0.getComponent();
			
			int column = theTable.getColumnCount()-1;
			int row = theTable.getSelectedRow();
			
			if(theTable.isCellSelected(row, column))
			{
				if( theTable.getValueAt(row, column) instanceof JButton)
				{
					JButton x = (JButton)theTable.getValueAt(row, column);
					x.doClick();
				}
				
			}
			
			int removeButtonRow = theTable.getSelectedRow();
			int removeButtonColumn = 0;
			if(theTable.isCellSelected(removeButtonRow, removeButtonColumn))
			{
				if ( theTable.getValueAt(removeButtonRow, removeButtonColumn) instanceof JButton  )
				{
					JButton y = (JButton)theTable.getValueAt(removeButtonRow, removeButtonColumn);
					y.doClick();
				}
			}
		}
		
	}
	
	/**
	 * This inner class , is for adding the shopping cart an article and continue shopping in the table menus
	 * @author Hp
	 *
	 */
	private class MyBuyContinueButton implements ActionListener
	{


		private MyCreatedDialogs currentDialog;
		
		public MyBuyContinueButton(MyCreatedDialogs currentDialog)
		{

			this.currentDialog = currentDialog;
		}
		public void actionPerformed(ActionEvent e) 
		{
			cart.add(currentDialog.createCartObject());
			currentDialog.closeDialog();
		}
		
	}
	/**
	 * This inner class add to the cart an article and the load the shopping cart
	 * @author Hp
	 *
	 */
	private class MyBuyGoCartGoButton implements ActionListener
	{

		private MyCreatedDialogs currentDialog;
		
		public MyBuyGoCartGoButton(MyCreatedDialogs currentDialog)
		{
			this.currentDialog = currentDialog;
		}
		public void actionPerformed(ActionEvent e) 
		{
			cart.add(currentDialog.createCartObject());
			currentDialog.closeDialog();
			fillCartTable();
			changeView("cart");
		}
		
	}
	
	/**
	 * This inner class is used to implements an actionListener in order to remove from the cart
	 * @author Hp
	 *
	 */
	private class MyRemoveButtonListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) 
		{
			MyClickedButton button = (MyClickedButton)e.getSource();
			
			CartObject uselessObject = button.getCartObject();
			
			cart.remove(uselessObject);
			fillCartTable();
			
		}
	}
	
	/**
	 * This inner class is used to edit re-open a form in order to edit the article bought3
	 * 
	 * @author Hp
	 *
	 */
	private class MyEditButtonListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) 
		{
			MyClickedButton button = (MyClickedButton)e.getSource();
			
			CartObject editObject = button.getCartObject();
			
			if(editObject.getShoppedObject() instanceof Accommodation)
			{
				
				AccommodationDialog aD =new AccommodationDialog(button.getMainWindow(), editObject);
				aD.setLocationRelativeTo(button.getMainWindow());
				aD.setModal(true);
				aD.setVisible(true);
			}
			else if (editObject.getShoppedObject() instanceof Package)
			{
				PackageDialog pD = new PackageDialog(button.getMainWindow(),  editObject);
				pD.setLocationRelativeTo(button.getMainWindow());
				pD.setModal(true);
				pD.setVisible(true);
						
			}
			else if(editObject.getShoppedObject() instanceof Park)
			{
				ParkDialog rD = new ParkDialog(button.getMainWindow(),  editObject);
				rD.setLocationRelativeTo(button.getMainWindow());
				rD.setModal(true);
				rD.setVisible(true);

			}
			
			cart.remove(editObject);
			fillCartTable();
			
		}
	}
	
	
	
	
	
	private JLabel getLblCartIcon() {
		if (lblCartIcon == null) {
			lblCartIcon = new JLabel("");
			lblCartIcon.setPreferredSize(new Dimension(30, 30));
			scaledImage(lblCartIcon, "/img/shopping-cart.jpg");
		}
		return lblCartIcon;
	}
	private JPanel getPnBorderTitle() {
		if (pnBorderTitle == null) {
			pnBorderTitle = new JPanel();
			pnBorderTitle.setBorder(new CompoundBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null), new LineBorder(new Color(0, 0, 0), 4, true)));
			pnBorderTitle.add(getLblCartIcon());
			pnBorderTitle.add(getLblTittle());
		}
		return pnBorderTitle;
	}

	/**
	 * It reset the application in order to be ready to be used again
	 */
	public void reset() {
		cart.clear();
		prepareFilter();
		updateTables();
		fillCartTable();
		
	}

	/**
	 * This method write the initial values in the filter
	 */
	private void prepareFilter() 
	{
		cbDestinations.setSelectedIndex(0);
		spAdults.setValue(1);
		spChilds.setValue(0);
		spCheckIn.setValue(getCurrentDay());

		Calendar c = Calendar.getInstance();
		c.setTime(getCurrentDay());
		c.add(Calendar.DATE, 1);
		spCheckOut.setValue(c.getTime());		
		chBreakfast.setSelected(false);
		chLiftAvailable.setSelected(false);
		chPool.setSelected(false);
		
	}
	
	/**
	 * this method set the discount according to one discount applied in the park
	 */
	private void updateDiscount()
	{
		accomodationReader.setDiscountByPark(parkReader.getDisountPark().getCode());

		packagesReader.setDiscountByPark(parkReader.getDisountPark().getCode());
		
		ticketsReader.setDiscountByPark(parkReader.getDisountPark().getCode());
	}

	public ArrayList<CartObject> getCart() {
		return cart;
	}
	private JMenuBar getMenuBar_1() {
		if (menuBar == null) {
			menuBar = new JMenuBar();
			menuBar.add(getMnSystem());
			menuBar.add(getMnOption());
			menuBar.add(getMnHelp());
		}
		return menuBar;
	}
	private JMenu getMnSystem() {
		if (mnSystem == null) {
			mnSystem = new JMenu(localizar("6"));
			mnSystem.setMnemonic(localizar("157").charAt(0));
			mnSystem.add(getMntmExit());
		}
		return mnSystem;
	}
	private JMenu getMnOption() {
		if (mnOptions == null) {
			mnOptions = new JMenu(localizar("154"));
			mnOptions.setMnemonic(localizar("158").charAt(0));
			mnOptions.add(getMntmSpanish());
			mnOptions.add(getMntmEnglish());
		}
		return mnOptions;
	}
	private JMenu getMnHelp() {
		if (mnHelp == null) {
			mnHelp = new JMenu(localizar("8"));
			mnHelp.setMnemonic(localizar("159").charAt(0));
			mnHelp.add(getMntmContents());
			mnHelp.add(getSeparator());
			mnHelp.add(getMntmAbout());
		}
		return mnHelp;
	}
	private JMenuItem getMntmExit() {
		if (mntmExit == null) {
			mntmExit = new JMenuItem(localizar("33"));
			mntmExit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0));
			mntmExit.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) 
				{
					System.exit(0);
				}
			});
		}
		return mntmExit;
	}
	private JMenuItem getMntmAbout() {
		if (mntmAbout == null) {
			mntmAbout = new JMenuItem(localizar("35"));
			mntmAbout.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F5, InputEvent.ALT_MASK));
			mntmAbout.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) 
				{
					AboutDialog aD = new AboutDialog(getMainWindow());
					aD.setModal(true);
					aD.setLocationRelativeTo(getMainWindow());
					aD.setVisible(true);
				}
			});
		}
		return mntmAbout;
	}
	private JRadioButton getRdOne() {
		if (rdOne == null) {
			rdOne = new JRadioButton("1");
			rdOne.setFont(new Font("Tahoma", Font.PLAIN, 12));
			buttonGroup.add(rdOne);
		}
		return rdOne;
	}
	private JRadioButton getRdTwo() {
		if (rdTwo == null) {
			rdTwo = new JRadioButton("2");
			rdTwo.setFont(new Font("Tahoma", Font.PLAIN, 12));
			buttonGroup.add(rdTwo);
		}
		return rdTwo;
	}
	private JRadioButton getRdThree() {
		if (rdThree == null) {
			rdThree = new JRadioButton("3");
			rdThree.setFont(new Font("Tahoma", Font.PLAIN, 12));
			buttonGroup.add(rdThree);
		}
		return rdThree;
	}
	private JRadioButton getRdFour() {
		if (rdFour == null) {
			rdFour = new JRadioButton("4");
			rdFour.setFont(new Font("Tahoma", Font.PLAIN, 12));
			buttonGroup.add(rdFour);
		}
		return rdFour;
	}
	private JRadioButton getRdFive() {
		if (rdFive == null) {
			rdFive = new JRadioButton("5");
			rdFive.setFont(new Font("Tahoma", Font.PLAIN, 12));
			buttonGroup.add(rdFive);
		}
		return rdFive;
	}
	private JRadioButton getRdbtnAll() {
		if (rdbtnAll == null) {
			rdbtnAll = new JRadioButton(localizar("30"));
			rdbtnAll.setFont(new Font("Tahoma", Font.PLAIN, 12));
			rdbtnAll.setSelected(true);
			buttonGroup.add(rdbtnAll);
		}
		return rdbtnAll;
	}
	private JLabel getLblStar() {
		if (lblStar == null) {
			lblStar = new JLabel("");
			lblStar.setPreferredSize(new Dimension(30, 30));
			scaledImage(lblStar, "/img/star.jpg");
		}
		return lblStar;
	}
	private JLabel getLblCheckin() {
		if (lblCheckin == null) {
			lblCheckin = new JLabel(localizar("10"));
			lblCheckin.setDisplayedMnemonic(localizar("164").charAt(0));
			lblCheckin.setLabelFor(getSpCheckIn());
			lblCheckin.setFont(new Font("Tahoma", Font.PLAIN, 14));
		}
		return lblCheckin;
	}
	private JLabel getLblCheckout() {
		if (lblCheckout == null) {
			lblCheckout = new JLabel(localizar("11"));
			lblCheckout.setDisplayedMnemonic(localizar("165").charAt(0));
			lblCheckout.setLabelFor(getSpCheckOut());
			lblCheckout.setFont(new Font("Tahoma", Font.PLAIN, 14));
		}
		return lblCheckout;
	}
	private JPanel getPnCheckInSpiner() {
		if (pnCheckInSpiner == null) {
			pnCheckInSpiner = new JPanel();
			pnCheckInSpiner.add(getLblCalendar1());
			pnCheckInSpiner.add(getSpCheckIn());
		}
		return pnCheckInSpiner;
	}
	private JPanel getPnCheckOutSpinner() {
		if (pnCheckOutSpinner == null) {
			pnCheckOutSpinner = new JPanel();
			pnCheckOutSpinner.add(getLblCalendar2());
			pnCheckOutSpinner.add(getSpCheckOut());
		}
		return pnCheckOutSpinner;
	}
	private JLabel getLblDestinations() {
		if (lblDestinations == null) {
			lblDestinations = new JLabel(localizar("9"));
			lblDestinations.setDisplayedMnemonic(localizar("163").charAt(0));
			lblDestinations.setLabelFor(getCbDestinations());
			
			lblDestinations.setFont(new Font("Tahoma", Font.PLAIN, 14));
		}
		return lblDestinations;
	}
	private JPanel getPnBlockTitle() {
		if (pnBlockTitle == null) {
			pnBlockTitle = new JPanel();
			pnBlockTitle.setLayout(new GridLayout(0, 1, 0, 0));
			pnBlockTitle.add(getPnTitle());
			pnBlockTitle.add(getPnSubTitle());
		}
		return pnBlockTitle;
	}
	private JPanel getPnPhotoNorth() {
		if (pnPhotoNorth == null) {
			pnPhotoNorth = new JPanel();
			pnPhotoNorth.add(getLblPhotoMenuNorth());
		}
		return pnPhotoNorth;
	}
	private JPanel getPnPhotoSouth() {
		if (pnPhotoSouth == null) {
			pnPhotoSouth = new JPanel();
			pnPhotoSouth.add(getLblPhotoMenuSouth());
		}
		return pnPhotoSouth;
	}
	private JLabel getLblPhotoMenuNorth() {
		if (lblPhotoMenuNorth == null) {
			lblPhotoMenuNorth = new JLabel("");
			lblPhotoMenuNorth.setPreferredSize(new Dimension(500, 100));
			scaledImage(lblPhotoMenuNorth, "/img/mainNorth.jpg");
		}
		return lblPhotoMenuNorth;
	}
	private JLabel getLblPhotoMenuSouth() {
		if (lblPhotoMenuSouth == null) {
			lblPhotoMenuSouth = new JLabel("");
			lblPhotoMenuSouth.setPreferredSize(new Dimension(500, 100));
			scaledImage(lblPhotoMenuSouth, "/img/mainSouth.jpg");
		}
		return lblPhotoMenuSouth;
	}
	private JPanel getPnCenterTitle() {
		if (pnCenterTitle == null) {
			pnCenterTitle = new JPanel();
			FlowLayout fl_pnCenterTitle = (FlowLayout) pnCenterTitle.getLayout();
			fl_pnCenterTitle.setVgap(10);
			pnCenterTitle.add(getPnBlockTitle());
		}
		return pnCenterTitle;
	}
	
	
	private void loadHelp(){

		 

		    try {
			    	File fichero = new File("help/help.hs");
			    	hsURL = fichero.toURI().toURL();
			        hs = new HelpSet(null, hsURL);
			      }

		    catch (Exception e){
		      System.out.println(localizar("148"));
		      return;
		   }

		   hb = hs.createHelpBroker();

		   hb.enableHelpKey(getRootPane(),"intro", hs);
		   hb.enableHelpOnButton(getMntmContents(),"intro",hs );
		   sensitivyHelp(getBtnLetsGetStarted(), "filter");
		   sensitivyHelp(getBtnBookAnAccomodation(), "accommodation");
		   sensitivyHelp(getBtnBookAPackage(), "package");
		   sensitivyHelp(getBtnBuyThePark(), "park");
		   sensitivyHelp(getBtnShoppingCart(), "cart");
		   sensitivyHelp(getBtnProceed(), "confirm");
		   
}
	
	public void sensitivyHelp(Component comp, String key)
	{
		hb.enableHelp(comp, key , hs);
	}
	
	
	private JMenuItem getMntmContents() {
		if (mntmContents == null) {
			mntmContents = new JMenuItem(localizar("34"));
			mntmContents.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F1, 0));
		}
		return mntmContents;
	}
	public void enableHelp(JDialog x, String key)
	{

		   hb.enableHelpKey(x.getRootPane(),key, hs);
	}
	
	private JSeparator getSeparator() {
		if (separator == null) {
			separator = new JSeparator();
		}
		return separator;
	}
	
	
	
public void updateText(Locale x) 
		{
		
		textos = ResourceBundle.getBundle("rcs/textos",x);
	
		}


public String localizar(String number)
{
	return textos.getString("texto"+number);
}

public Locale getMyLocale() {
	return local;
}
	











	/**
	 * This menu item is used to change from the current language to Spanish
	 * @return JMenuITem mntmSpanish
	 */
	private JMenuItem getMntmSpanish() {
		if (mntmSpanish == null) {
			mntmSpanish = new JMenuItem(localizar("156"));
			mntmSpanish.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F2, InputEvent.ALT_MASK));
			mntmSpanish.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) 
				{
					int input = JOptionPane.showConfirmDialog(getMainWindow(), "If you continue the app will be restart and all the cart and information introduced will be deleted");
					if(input == 0)
					{
						MainWindow x = new MainWindow(new Locale("es"));
						x.getMntmSpanish().setEnabled(false);
						x.getMntmEnglish().setEnabled(true);
						x.setVisible(true);
						x.setLocationRelativeTo(null);
						getMainWindow().dispose();
					}
				}
			});
		}
		return mntmSpanish;
	}
	
	/**
	 * A menu item used to change the current language to English
	 * @return JMenuItem mntmEnglish
	 */
	private JMenuItem getMntmEnglish() {
		if (mntmEnglish == null)
		{
			mntmEnglish = new JMenuItem(localizar("155"));
			mntmEnglish.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F3, InputEvent.ALT_MASK));
			mntmEnglish.setEnabled(false);
			mntmEnglish.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) 
				{
					int input = JOptionPane.showConfirmDialog(getMainWindow(), "Si continuas adelante la aplicacion sera reiniciada y todo el carrito y la informacion introducida perdida");
					if(input == 0)
					{
						MainWindow x = new MainWindow(new Locale("en"));
						x.getMntmEnglish().setEnabled(false);
						x.getMntmSpanish().setEnabled(true);
						x.setVisible(true);
						x.setLocationRelativeTo(null);
						getMainWindow().dispose();
					}
				}
			});
		
		}
		
		return mntmEnglish;
	}
}