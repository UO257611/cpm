package logic;

import java.awt.Component;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;

import javax.swing.JOptionPane;

public class ParksReader 
{
	private ArrayList<Park> listOfParks;
	private Park discountPark;
	private Locale local;
	
	public ParksReader(Locale x)
	{
		local = x;
		listOfParks = new ArrayList<Park>();
		readFile();
		makeDiscount();
	}
	

	private void readFile() 
	{
		
		 String line = "";
		    try {
		    	BufferedReader myFile = new BufferedReader(new FileReader("files/parks.dat"));
		    	while (myFile.ready()) 
		    	{
		    		line = myFile.readLine();
		    		String[] pieces = line.split("@");
		    		listOfParks.add(new Park(pieces[0], pieces[1], pieces[2],pieces[3],
		    				pieces[4],local));
		    	}
		    	myFile.close();
		    	}
		    	catch (FileNotFoundException fnfe) {
		    		JOptionPane.showMessageDialog(null,"We couldnt find the accomodation.dat file");
		    	}
		    	catch (IOException ioe) {
		    		new RuntimeException("Input/Output error");
		    	}
		  	
		
	}
	
	public Park searchPark(String code)
	{
		for(int i = 0; i< listOfParks.size();i++)
		{
			if(code.equals(listOfParks.get(i).getCode()))
			{
				return listOfParks.get(i);
			}
		}
		return null;
	}

	public ArrayList<Park> getListOfParks() 
	{
		return listOfParks;
	}
	
	public ArrayList<String> getArrayOfCountries()
	{
		ArrayList<String> countries = new ArrayList<String>();
		for(Park x : listOfParks)
		{
			if(countries.isEmpty())
			{
				countries.add(x.getCountry());
			}
			
			if(!countries.contains(x.getCountry()))
			{
				countries.add(x.getCountry());
			}
			
		}
		return countries;
	}
	
	public String getCountry(String code)
	{
		return searchPark(code).getCountry();
	}
	

	private void makeDiscount() {
		discountPark = listOfParks.get((int)(Math.random()*(listOfParks.size()-1)));
		discountPark.setDiscount(true);
	}
	
	public Park getDisountPark()
	{
		return discountPark;
	}
	



	

}
