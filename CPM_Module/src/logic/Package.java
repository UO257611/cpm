package logic;

import java.util.Locale;
import java.util.ResourceBundle;

public class Package implements TypesOfBuy
{
	private String packageCode;
	private String packageName;
	private String accommodationCode;
	private String days;
	private String adultPrice;
	private String childrenPrice;
	private String themeParkCode;
	private boolean discount;
	private Locale local;
	private ResourceBundle textos;
	
	public Package(String packageCode,String packageName,String parkCode, String accomodationCode, String days, String adultprice,String childrenPrice, Locale x)
	{
		this.local = x;
		this.textos = ResourceBundle.getBundle("rcs/textos",local);
		this.packageCode = packageCode;
		this.packageName = packageName;
		this.accommodationCode = accomodationCode;
		this.days = days;
		this.adultPrice = adultprice;
		this.childrenPrice = childrenPrice;
		this.themeParkCode = parkCode;
		discount = false;
		
	}

	public boolean isDiscount() {
		return discount;
	}

	public void setDiscount(boolean discount) {
		this.discount = discount;
	}

	public String getThemeParkCode() {
		return themeParkCode;
	}

	public String getPackageCode() {
		return packageCode;
	}

	public void setPackageCode(String packageCode) {
		this.packageCode = packageCode;
	}

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public String getAccommodationCode() {
		return accommodationCode;
	}

	public void setAccommodationCode(String accommodationCode) {
		this.accommodationCode = accommodationCode;
	}

	public int getDays() {
		return Integer.valueOf(days);
	}

	public void setDays(int days) {
		this.days = String.valueOf(days);
	}

	public int getAdultPrice() {
		return Integer.valueOf(adultPrice);
	}

	public void setAdultPrice(int adultPrice) {
		this.adultPrice = String.valueOf(adultPrice);
	}

	public int getChildrenPrice() {
		return Integer.valueOf(childrenPrice);
	}

	public String getDataInfo(Locale lang) 
	{
		this.local = lang;
		StringBuilder info = new StringBuilder();
		
		info.append(localizar("101")+ " " + days+ " "+ localizar("102"));
		if(discount)
		{
			info.append(localizar("103"));
		}
		
		
		return info.toString();
	}

	public String getPhotoPath()
	{
		return "/img/" + getPackageCode()+".jpg";
	}
	public String getPrice(int adults, int childs, Locale lang) 
	{
		this.local = lang;
		StringBuilder price = new StringBuilder();
		
		price.append(localizar("104") + " "+ adultPrice + localizar("203") +" \n");

		price.append(localizar("105")+" "+ childrenPrice + localizar("203") +" \n");
		
		price.append(localizar("106")+" "+ (getChildrenPrice()*childs + getAdultPrice()*adults)+ localizar("203") +"\n");
		
		
		return price.toString();
	}
	
	
	public double getFinalPrice(int adults, int childs)
	{
		double finalPrice = getChildrenPrice()*childs + getAdultPrice()*adults;
		if(discount)
		{
			finalPrice=  finalPrice-(finalPrice*20)/100;
		}
		return finalPrice;
	}
	
	private String localizar(String number) 
	{
		return textos.getString("texto"+number);
	}

}
