package logic;

import java.util.Locale;
import java.util.ResourceBundle;
import java.util.ResourceBundle.Control;

import javax.swing.JLabel;

public class Accommodation implements TypesOfBuy{
	private String accommodationCode;
	private String type;
	private String category;
	private String facilityName;
	private String themeParkCode;
	private String capacity;
	private String price;
	private boolean discount;
	private ResourceBundle textos;
	private Locale local;
	
	
	public Accommodation(String accommodationCode, String type, String category, String facilityName, String themeParkCode, String capacity, String price, Locale x)
	{
		Locale local = x;
		textos = ResourceBundle.getBundle("rcs/textos",local);
		
		this.accommodationCode = accommodationCode;
		this.type = type;
		this.category = category;
		this.facilityName = facilityName;
		this.themeParkCode = themeParkCode;
		this.capacity = capacity;
		this.price = price;
		discount= false;
	}


	public boolean isDiscount() {
		return discount;
	}


	public void setDiscount(boolean discount) {
		this.discount = discount;
	}


	public String getAccommodationCode() {
		return accommodationCode;
	}


	public void setAccommodationCode(String accommodationCode) {
		this.accommodationCode = accommodationCode;
	}

	public String getPhotoPath()
	{
		
			return "/img/"+getAccommodationCode()+".jpg";
		
	}
		
	
		
	

	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public int getCategory() {
		return Integer.valueOf(category);
	}


	public void setCategory(int category) {
		this.category = String.valueOf(category);
	}


	public String getFacilityName() {
		return facilityName;
	}


	public void setFacilityName(String facilityName) {
		this.facilityName = facilityName;
	}


	public String getThemeParkCode() {
		return themeParkCode;
	}


	public void setThemeParkCode(String themeParkCode) {
		this.themeParkCode = themeParkCode;
	}


	public int  getCapacity() {
		return Integer.valueOf(capacity);
	}


	public void setCapacity(int capacity) {
		this.capacity = String.valueOf(capacity);
	}


	public int getPrice() {
		return Integer.valueOf(price);
	}


	public void setPrice(int price) {
		this.price = String.valueOf(price);
	}
	
	public String getDataInfo(Locale lang)
	{
		this.local = lang;
		StringBuilder info = new StringBuilder();
		
		
		
		if (type.equals("AP"))
		{
			info.append(localizar("75") );
		}
		else if(type.equals("HO"))
		{
			info.append(localizar("76"));
		}
		else
		{
			info.append(localizar("77"));
		}
		
		info.append(localizar("78"));
		
		info.append(capacity);
		
		if(discount)
		{
			info.append(localizar("79"));
		}
		
		
		
		return info.toString();
		
		
	}


	public String packageAdditionalInformation(Locale lang) {
		this.local = lang;
		StringBuilder additional = new StringBuilder();
		additional.append(localizar("80")+" "+getFacilityName()+"\n ");
		additional.append(localizar("82")+" "+capacity+"\n ");
		
		return additional.toString();
	}

	public String getFinalPrice(int people, Locale lang)
	{
		this.local = lang;
		StringBuilder price = new StringBuilder();
		if(type.equals("HO"))
				price.append(localizar("83") +" "+ people* getPrice()+ localizar("203") +"\n");
		else
				price.append(localizar("83") +" "+ getPrice() + localizar("203") +"\n");
		
		return price.toString();
	}
	public double getFinalDialogPrice(int people)
	{
		double finalPrice = 0.0;
		if(type.equals("HO"))
		{
				finalPrice += people* getPrice();
		}
		else
		{
				finalPrice += getPrice();
		}
		
		if(discount)
		{
			finalPrice = finalPrice- (finalPrice*20)/100;
		}
		
		return finalPrice;
	}
	
	public String getDataDialogPrepared(TicketsReader ticketReader, ParksReader parkReader, Locale lang) 
	{
		this.local = lang;
		StringBuilder info = new StringBuilder();
		
		info.append(localizar("84"));
		if (getType().equals("HO"))
			info.append(localizar("85"));
		else if ( getType().equals("AP"))
			info.append(localizar("86"));
		else
			info.append(localizar("87"));
		info.append(" " + localizar("88") +" "+ getFacilityName()+" "+localizar("89")+" "+parkReader.searchPark(getThemeParkCode()).getCountry()+ localizar("90")+" "+ getCategory()+ 
				" "+localizar("91")+" " + parkReader.searchPark(getThemeParkCode()).getNamePark());
		
		if(discount)
		{
			info.append(localizar("92"));
		}
		
		try {
			info.append(localizar("93")+" " + ticketReader.searchByPark(getThemeParkCode()).getAdultPrice() + localizar("203"));
			info.append(localizar("94")+" "+ticketReader.searchByPark(getThemeParkCode()).getChildrenPrice()+ localizar("203"));
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		
		return info.toString();
	}
	
	private String localizar(String number) 
	{
		return textos.getString("texto"+number);
	}
}
