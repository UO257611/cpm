package logic;

import java.util.Locale;
import java.util.ResourceBundle;

public class Ticket 
{
	private String ticketCode;
	private String parkCode;
	private String adultPrice;
	private String childrenPrice;
	private boolean discount;
	private Locale local;
	private ResourceBundle textos;
	
	public Ticket(String ticketCode, String parkCode, String adultPrice, String childrenPrice, Locale x)
	{
		this.local = x;
		this.textos = ResourceBundle.getBundle("rcs/textos",local);
		
		this.ticketCode = ticketCode ;
		this.parkCode = parkCode;
		this.adultPrice = adultPrice;
		this.childrenPrice = childrenPrice;
		discount = false;
	}

	public String getTicketCode() {
		return ticketCode;
	}

	public void setTicketCode(String ticketCode) {
		this.ticketCode = ticketCode;
	}

	public String getParkCode() {
		return parkCode;
	}

	public void setParkCode(String parkCode) {
		this.parkCode = parkCode;
	}

	public int getAdultPrice() {
		return Integer.valueOf(adultPrice);
	}

	public void setAdultPrice(int adultPrice) {
		this.adultPrice = String.valueOf(adultPrice);
	}

	public int getChildrenPrice() {
		return Integer.valueOf(childrenPrice);
	}

	public void setChildrenPrice(int childrenPrice) {
		this.childrenPrice = String.valueOf(childrenPrice);
	}
	
	/**
	 * Return a price but as a string for a table or a dialog info
	 * @param adults
	 * @param childs
	 * @param lang
	 * @return
	 */
	public String getFinalPrice(int adults, int childs, Locale lang)
	{
		this.local = lang;
		StringBuilder price = new StringBuilder();
		if(discount)
		{
			price.append(localizar("153"));
		}
		price.append(localizar("104")+" "+getAdultPrice() + localizar("203") +"\n");
		price.append(localizar("105") +" "+getChildrenPrice() + localizar("203") +"\n");
		price.append(localizar("106") +" "+(getAdultPrice()*adults + getChildrenPrice()*childs) + localizar("203") +"\n");
		return price.toString();
		
	}
	/**
	 * this method caluculate the fianl price for the text fields but it retourned as a double
	 * @param adults
	 * @param childs
	 * @return
	 */
	public double getFinalPriceDialog(int adults, int childs)
	{
		double finalPrice = 0.0;
		finalPrice = getAdultPrice()*adults + getChildrenPrice()*childs;
		if(discount)
		{
			finalPrice = finalPrice- (finalPrice*20)/100;
		}
		return finalPrice;
		
	}
	
	/**
	 * Similar to getFinalPrice
	 * @param lang
	 * @return
	 */
	public String getChildAdultPrice( Locale lang)
	{
		this.local = lang;
		StringBuilder price = new StringBuilder();
		price.append(localizar("104") + " "+getAdultPrice() + localizar("203") +"\n");
		price.append(localizar("105") +" "+getChildrenPrice() + localizar("203") +"\n");
		return price.toString();
	}
	
	public void setDiscount(boolean discount) 
	{
		this.discount = discount;
	}
	
	private String localizar(String number) 
	{
		return textos.getString("texto"+number);
	}
	

}
