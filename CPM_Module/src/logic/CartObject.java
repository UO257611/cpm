package logic;

import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;

public class CartObject implements TypesOfBuy 
{
	
	private String adults;
	private String childs;
	private TypesOfBuy shoppedObject;
	private String dateIn;
	private String dateOut;
	private String price;
	private String description;
	private String type;
	private Date spInValue;
	private Date spOutValue;
	private boolean breakfast;
	private Locale local;
	private ResourceBundle textos;
	
	public CartObject ( String adults, String childs, TypesOfBuy shoppedObject , String dateIn, String dateOut, 
			String price, String description, String type, Date spInValue, Date spOutValue, boolean breakfast)
	{
		this.local = new Locale("en");
		textos = ResourceBundle.getBundle("rcs/textos",local);
		this.adults = adults;
		this.childs = childs;
		this.shoppedObject = shoppedObject;
		this.dateIn = dateIn;
		this.dateOut = dateOut;
		this.price = price;
		this.description = description;
		this.type = type;
		this.spInValue = spInValue;
		this.spOutValue = spOutValue;
		this.breakfast = breakfast;
	}
	
	
	
	public boolean isBreakfast() {
		return breakfast;
	}



	public Date getSpInValue() {
		return spInValue;
	}



	public Date getSpOutValue() {
		return spOutValue;
	}



	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getAdults() {
		return adults;
	}
	public void setAdults(String adults) {
		this.adults = adults;
	}
	public String getChilds() {
		return childs;
	}
	public void setChilds(String childs) {
		this.childs = childs;
	}
	public TypesOfBuy getShoppedObject() {
		return shoppedObject;
	}
	public void setShoppedObject(TypesOfBuy shoppedObject) {
		this.shoppedObject = shoppedObject;
	}
	public String getDateIn() {
		return dateIn;
	}
	public void setDateIn(String dateIn) {
		this.dateIn = dateIn;
	}
	public String getDateOut() {
		return dateOut;
	}
	public void setDateOut(String dateOut) {
		this.dateOut = dateOut;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	/**
	 * Create a useer data string
	 * @param lang
	 * @return
	 */
	public String getUserData( Locale lang)
	{
		this.local = lang;
		StringBuilder info = new StringBuilder();
		
		info.append(localizar("107")+" " + getAdults()+". \n");
		info.append(localizar("108")+" "+ getChilds()+". \n");
		if(getShoppedObject() instanceof Park)
		{
			info.append(localizar("109")+" "+ getDateIn()+"\n");
		}
		else
		{
			info.append(localizar("110")+" " +getDateIn()+"\n");

			info.append(localizar("111")+" " +getDateOut()+"\n");
		}
		return info.toString();
	}
	
	/**
	 * calculate the number of nights
	 * @return
	 */
	public int getNights()
	{
		long difference =  spOutValue.getTime() - spInValue.getTime();
		int nights = (int) (difference / (24 * 60 * 60 * 1000));
		return nights;
	}
	
	private String localizar(String number) 
	{
	

	return textos.getString("texto"+number);
	}
}
