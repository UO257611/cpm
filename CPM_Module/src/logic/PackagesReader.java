package logic;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;

import javax.swing.JOptionPane;

public class PackagesReader {

	private ArrayList<Package> listOfPackages;
	private Locale local;
	
	public PackagesReader(Locale x)
	{
		local = x;
		listOfPackages = new ArrayList<Package>();
		readFile();
	}
	
	
	private void readFile() 
	{
		
		 String line = "";
		    try {
		    	BufferedReader myFile = new BufferedReader(new FileReader("files/packages.dat"));
		    	while (myFile.ready()) 
		    	{
		    		line = myFile.readLine();
		    		String[] pieces = line.split("@");
		    		listOfPackages.add(new Package(pieces[0], pieces[1], pieces[2],pieces[3],
		    				pieces[4], pieces[5], pieces[6], local));
		    	}
		    	myFile.close();
		    	}
		    	catch (FileNotFoundException fnfe) {
		    		JOptionPane.showMessageDialog(null,"We couldnt find the accomodation.dat file");
		    	}
		    	catch (IOException ioe) {
		    		new RuntimeException("Input/Output error");
		    	}
		  	
		
	}


	public ArrayList<Package> getListOfPackages() {
		return listOfPackages;
	}
	
	public void setDiscountByPark(String code)
	{
		for(Package x : listOfPackages)
		{
			if ( x.getThemeParkCode().equals(code))
			{
				x.setDiscount(true);
			}
		}
	}
	

}
