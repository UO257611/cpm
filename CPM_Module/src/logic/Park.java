package logic;

import java.util.Locale;
import java.util.ResourceBundle;

public class Park implements TypesOfBuy {

	private String code;
	private String namePark;
	private String country;
	private String city;
	private String description;
	private boolean discount;
	private Locale local;
	private ResourceBundle textos;
	
	public Park(String code, String namePark, String country, String city, String description, Locale x)
	{
		this.local = x;
		textos = ResourceBundle.getBundle("rcs/textos",local);
		this.code = code;
		this.namePark = namePark;
		this.country = country;
		this.city = city;
		this.description = description;
		discount = false;
	}
	
	
	
	public boolean isDiscount() {
		return discount;
	}



	public void setDiscount(boolean descuento) {
		this.discount = descuento;
	}



	public String getPhotoPath()
	{
		return "/img/"+getCode()+".jpg";
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getNamePark() {
		return namePark;
	}

	public void setNamePark(String namePark) {
		this.namePark = namePark;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * Information for the table
	 * @param lang
	 * @return
	 */
	public String accomodationAdditionalInformation(Locale lang)
	{
		this.local = lang;
				
		StringBuilder info = new StringBuilder();
		
		info.append(localizar("95")+ " ");
		info.append(country);
		info.append("\n");
		
		info.append(localizar("96")+" ");
		info.append(namePark);
		info.append("\n");
		
		
		
		return info.toString();
		
		
	}
	
	/**
	 * Information for the dialog
	 * @param reader
	 * @param lang
	 * @return
	 */
	public String getDialogInfo(TicketsReader reader, Locale lang)
	{
		this.local = lang;
		StringBuilder info = new StringBuilder();
		
		info.append(localizar("95")+" ");
		info.append(country);
		info.append(localizar("97")+" "+ city+ ". \n");
		info.append(getDescription()+ "\n ");
		
	
		
		try 
		{
			info.append("\n");
			info.append(localizar("99")+" \n");
			info.append(reader.searchByPark(getCode()).getChildAdultPrice(lang) );
		}
		catch (Exception e)
		{
			System.out.println(localizar("100"));
		}
		return info.toString();
	}
	
	/**
	 * find a string in the properties field
	 * @param number
	 * @return
	 */
	private String localizar(String number) 
	{
		return textos.getString("texto"+number);
	}
	

	

}
