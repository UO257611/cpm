package logic;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;

import javax.swing.JOptionPane;

public class TicketsReader 
{
	 private ArrayList<Ticket> listOfTickets;
	 private Locale local;
	
	public TicketsReader(Locale x)
	{
		local = x;
		listOfTickets = new ArrayList<Ticket>();
		fileReader();
	}
	
	private void fileReader()
	{
	 String line = "";
	    try {
	    	BufferedReader myFile = new BufferedReader(new FileReader("files/tickets.dat"));
	    	while (myFile.ready()) 
	    	{
	    		line = myFile.readLine();
	    		String[] pieces = line.split("@");
	    		listOfTickets.add(new Ticket(pieces[0], pieces[1], pieces[2],pieces[3], local));
	    	}
	    	myFile.close();
	    	}
	    	catch (FileNotFoundException fnfe) {
	    		JOptionPane.showMessageDialog(null,"We couldnt find the accomodation.dat file");
	    	}
	    	catch (IOException ioe) {
	    		new RuntimeException("Input/Output error");
	    	}
	}
	
	
	public Ticket searchByPark(String code) throws Exception
	{
		for(Ticket x : listOfTickets)
		{
			if(x.getParkCode().equals(code))
			{
				return x;
			}
		}
		
		throw new Exception("Error  while finding the ticket");
	}
	
	public void setDiscountByPark(String code)
	{
		for(Ticket x : listOfTickets)
		{
			if(x.getParkCode().equals(code))
			{
				x.setDiscount(true);
			}
		}
	}
	

}
