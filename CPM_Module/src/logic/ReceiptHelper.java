package logic;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

import com.itextpdf.text.Document;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import gui.MainWindow;

public class ReceiptHelper 
{
	private JFileChooser selector;
	private ArrayList<String> userData;
	private ArrayList<CartObject> cart;
	private MainWindow mainWindow;
	
	public ReceiptHelper(MainWindow mainWindow ,ArrayList<String> userData)
	{
		this.mainWindow = mainWindow;
		this.userData = userData;
		this.cart = mainWindow.getCart();
	}
	
	/**
	 * This method return a string with the format desired for the ticket
	 * @return String
	 */
	public String showInfo()
	{
		StringBuilder info = new StringBuilder();
		info.append(mainWindow.localizar("112"));
		info.append(mainWindow.localizar("113") + " " + mainWindow.getCurrentDay()+"\n");
		
		info.append("--------------- \n");
		info.append(userData.get(2) +" - "+ userData.get(1).toUpperCase() +" "+
		userData.get(0).toUpperCase() +"\n");
		
		info.append(mainWindow.localizar("114"));
		
		info.append(mainWindow.localizar("115"));
		info.append(getParksData());
		
		info.append(mainWindow.localizar("116"));
		info.append(getAccomodationsData());
		
		info.append(mainWindow.localizar("117"));
		info.append(getTicketsData());
		
		info.append(mainWindow.localizar("118"));
		info.append(getPaymentData());
		return info.toString();
		
		
	}
	
	/**
	 * Get the park data prepared
	 * @return String
	 */
	private String getParksData() 
	{
		StringBuilder data = new StringBuilder();
		for(CartObject x : cart)
		{
			if(x.getShoppedObject() instanceof Package)
			{
				Package currentPackage =(Package) x.getShoppedObject();
				data.append(mainWindow.localizar("119"));
				data.append(currentPackage.getPackageCode()+"/ "+currentPackage.getPackageName()+" / ");
				data.append(mainWindow.getParkReader().searchPark(currentPackage.getThemeParkCode()).getNamePark());
				data.append(" / "+ currentPackage.getDays()+" "+mainWindow.localizar("121"));
				
				data.append(mainWindow.localizar("122") + " " +x.getDateIn()+ "\n");
				data.append(mainWindow.localizar("123") + " "+ x.getAdults() + " "+ mainWindow.localizar("124")+" "+ x.getChilds()+" \n \n");
			}
		}
		return data.toString();
	}
	

	/**
	 * Get the Accommodation data prepared
	 * @return String
	 */
	private String getAccomodationsData()
	{
		StringBuilder data = new StringBuilder();
		for(CartObject x : cart)
		{
			if(x.getShoppedObject() instanceof Accommodation)
			{
				
				Accommodation currentAccomodation = (Accommodation)x.getShoppedObject();
				data.append(mainWindow.localizar("126") + " "+ currentAccomodation.getAccommodationCode()+ "/ ");
				data.append(currentAccomodation.getType()+" / "+ currentAccomodation.getFacilityName()+" / "+ currentAccomodation.getCategory()+" / ");
				data.append(mainWindow.getParkReader().searchPark(currentAccomodation.getThemeParkCode()).getNamePark() + " / ");
				data.append(mainWindow.localizar("127") + " ");
				if(x.isBreakfast())
				{
					data.append(mainWindow.localizar("128"));
				}
				else
				{
					data.append(mainWindow.localizar("129"));
				}
				
				data.append(mainWindow.localizar("122") + " "+ x.getDateIn()+" "+ mainWindow.localizar("130")+ " " + x.getNights()+" \n");
				data.append(mainWindow.localizar("131") + " "+ (Integer.valueOf(x.getAdults()) + Integer.valueOf(x.getChilds())+ "\n \n"));
				
				
				
			
			}
		}
		return data.toString();
	}
	
	

	/**
	 * Get the Ticket data prepared
	 * @return String
	 */
	private String getTicketsData()
	{
		StringBuilder data = new StringBuilder();
		for(CartObject x : cart)
		{
			if(x.getShoppedObject() instanceof Park)
			{
				Park currentPark = (Park) x.getShoppedObject();
				try {
					data.append(mainWindow.localizar("132") + " " + mainWindow.getTicketsReader().searchByPark(currentPark.getCode()).getTicketCode()+" / ");
					data.append(currentPark.getNamePark() + " /n");
					data.append(mainWindow.localizar("122") + " "+ x.getDateIn()+ " / ");
					data.append(mainWindow.localizar("133") + " " + (x.getNights()+1)+ "\n \n");
					
				} catch (Exception e) {
					System.out.println(mainWindow.localizar("100"));
				}
							
				
			
			}
		}
		return data.toString();
	}
	

	/**
	 * Get the Payment data prepared
	 * @return String
	 */
	private String getPaymentData()
	{
		double ticketPrice = 0.0;
		double accomPrice = 0.0;
		double packPrice = 0.0;
		StringBuilder data = new StringBuilder();
		for(CartObject x : cart)
		{
			if(x.getShoppedObject() instanceof Accommodation)
			{
				accomPrice += ((Accommodation) x.getShoppedObject()).getFinalDialogPrice( Integer.valueOf(x.getAdults()) + Integer.valueOf(x.getChilds()));
				
			}
			else if(x.getShoppedObject() instanceof Package)
			{
				packPrice += ((Package)x.getShoppedObject()).getFinalPrice(Integer.valueOf(x.getAdults()), Integer.valueOf(x.getChilds()));
			}
			else if(x.getShoppedObject() instanceof Park)
			{
				try {
					ticketPrice += mainWindow.getTicketsReader().searchByPark(
							((Park)x.getShoppedObject()).getCode()).getFinalPriceDialog(Integer.valueOf(x.getAdults()), Integer.valueOf(x.getChilds()));
				} catch (NumberFormatException e) {
					
					e.printStackTrace();
				} catch (Exception e) {
					System.out.println(mainWindow.localizar("100"));
				}
			}
		}
		data.append(mainWindow.localizar("134") + "    "+ packPrice+ mainWindow.localizar("203") + "\n");
		data.append(mainWindow.localizar("136") + "    "+ accomPrice+ mainWindow.localizar("203")  +"\n");
		data.append(mainWindow.localizar("137") + "    " + ticketPrice+ mainWindow.localizar("203") + "\n \n \n");
		
		data.append(mainWindow.localizar("138") + "    " + (packPrice + accomPrice + ticketPrice)+ mainWindow.localizar("203") );
		return data.toString();
	}
	
	/**
	 * This method do all the I/O operation for saving the receipt in a .txt format
	 * @param path
	 */
	public void saveTxt(String path)
	{
		try
		{
		
			File archivo = new File (path+"/ReceiptOf_"+ userData.get(2)+".txt");
			FileWriter fr = new FileWriter (archivo);
			BufferedWriter br = new BufferedWriter(fr);
			br.write(showInfo());
			br.close();
			fr.close();
		}catch (Exception e)
		{
			System.out.println(mainWindow.localizar("139"));
		}
	}
	
	

	/**
	 * This method do all the I/O operation for saving the receipt in a .pdf format, using a library
	 * @param path
	 */
	public void savePdf(String root)
	{
		
		Document myPdf= null;
		
		try {
		 myPdf = new Document();
       
        PdfWriter.getInstance(myPdf, new FileOutputStream(root+"/ReceiptOf_"+ userData.get(2)+".pdf"));
    
        myPdf.open();
      
        myPdf.add(new Paragraph(showInfo()));
		  
		
        ;
		}catch(Exception e)
		{}
		finally
		{
			myPdf.close();
		}
		
		
		
	}
	
	
	

}
