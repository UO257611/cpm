package logic;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;

import javax.swing.JOptionPane;

public class AccommodationReader
{
	private ArrayList<Accommodation> listOfAccommodations;
	private Locale local;
	
	public AccommodationReader(Locale x)
	{
		local = x;
		listOfAccommodations = new ArrayList<Accommodation>();
		readFile();
	}
	
	
	
	private void readFile() {
	    String line = "";
	    try {
	    	BufferedReader myFile = new BufferedReader(new FileReader("files/accomodation.dat"));
	    	while (myFile.ready()) {
	    		line = myFile.readLine();
	    		String[] pieces = line.split("@");
	    		listOfAccommodations.add(new Accommodation(pieces[0], pieces[1], pieces[2],pieces[3],
	    				pieces[4],pieces[5],pieces[6], local));
	    	}
	    	myFile.close();
	    	}
	    	catch (FileNotFoundException fnfe) {
	    		JOptionPane.showMessageDialog(null,"We couldnt find the accomodation.dat file");
	    	}
	    	catch (IOException ioe) {
	    		new RuntimeException("Input/Output error");
	    	}
	  	}



	public ArrayList<Accommodation> getListOfAccomodations() {
		return listOfAccommodations;
	}



	public Accommodation searchAccomoadtion(String accommodationCode) {
		for(int i = 0; i < listOfAccommodations.size();i++)
		{
			if (listOfAccommodations.get(i).getAccommodationCode().equals(accommodationCode))
			{
				return listOfAccommodations.get(i);
			}
		}
		return null;
	}
	
	public void setDiscountByPark(String code)
	{
		for(Accommodation x : listOfAccommodations)
		{
			if ( x.getThemeParkCode().equals(code))
			{
				x.setDiscount(true);
			}
		}
	}


}
