package logica;

public class Calle {

	
	
	private static int POSICION_META = 0;
	private Casilla[] casillas = null;

	public static int getPOSICION_META() {
	    return POSICION_META;
	}

	public Calle (int DIM) {
	   casillas= new Casilla[DIM];
	   POSICION_META=DIM-1;
		for (int i = 0; i < DIM; i++) {
			casillas[i] = new Casilla();
		}
	}

	public int puntosCasilla(int posicion) {
		return casillas[posicion].getValor();
	}
	
	public Casilla getCasilla(int posicion) {
		return casillas[posicion];
	}

	
	
	
}
