package logica;

import java.util.Random;

import javax.swing.ImageIcon;

public class Power {
	private int casilla;
	private Random random = new Random();
	
	public Power() {
		casilla= random.nextInt(9)+1;
	}

	public int getCasilla() {
		return casilla;
	}

	public void setCasilla(int casilla) {
		this.casilla = casilla;
	}

}
