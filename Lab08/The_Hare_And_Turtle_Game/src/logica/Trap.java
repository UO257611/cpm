package logica;

import javax.swing.ImageIcon;
import java.util.Random;

public class Trap {
	private ImageIcon foto;
	private int casilla;
	private Random random = new Random();
	
	public Trap() {
		casilla = random.nextInt(9)+1;
		foto= new ImageIcon((getClass().getResource("/img/trampa.jpg")));
	}

	public ImageIcon getFoto() {
		return foto;
	}

	public void setFoto(ImageIcon foto) {
		this.foto = foto;
	}

	public int getCasilla() {
		return casilla;
	}

	public void setCasilla(int casilla) {
		this.casilla = casilla;
	}
	
	
}
