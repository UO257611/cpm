package igu;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Toolkit;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Point;
import java.awt.Color;

import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;

import org.jvnet.substance.SubstanceLookAndFeel;

import gui.ModeloNoEditable;
import logica.Inmobiliaria;
import logica.Mansion;

import javax.swing.UIManager;
import javax.swing.JScrollPane;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.border.BevelBorder;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JTabbedPane;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JTable;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.SpinnerNumberModel;
import javax.swing.ListSelectionModel;

public class MainWindow extends JFrame {

	private JPanel contentPane;
	private JLabel lblEiiRealState;
	private JLabel lblIcon;
	private JPanel pnVisits;
	private JScrollPane scVisits;
	private JList visitList;
	private JButton btnAdd;
	private JButton btnDelete;
	private JPanel pnPayment;
	private JSpinner spPayment;
	private JLabel lblPercentaje;
	private JButton btnCalculate;
	private JTextField txtPayment;
	private JButton btnOk;
	private JButton btnCancel;
	private JScrollPane scDescription;
	private JTextArea taDescription;
	private JTabbedPane tpHouses;
	private JPanel pnOnSale;
	private JPanel pnOnRenting;
	private JPanel pnFilter;
	private JLabel lblSelectFilter;
	private JCheckBox chckbxNorth;
	private JCheckBox chckbxCenter;
	private JCheckBox chckbxSouth;
	private JScrollPane spHouses;
	private JTable tHouses;
	
	private Inmobiliaria agency = new Inmobiliaria();
	private DefaultTableModel tableModel;
	private DefaultListModel listModel;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try 
				{
					JFrame.setDefaultLookAndFeelDecorated(true);
					JDialog.setDefaultLookAndFeelDecorated(true);
					
					SubstanceLookAndFeel.setSkin("org.jvnet.substance.skin.MistSilverSkin");
					
					MainWindow frame = new MainWindow();
					frame.setVisible(true);
				} 
				catch (Exception e) 
				{
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainWindow() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(MainWindow.class.getResource("/img/llave.JPG")));
		setTitle("EII Real State Agency");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 835, 550);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.add(getLblEiiRealState());
		contentPane.add(getLblIcon());
		contentPane.add(getPnVisits());
		contentPane.add(getPnPayment());
		contentPane.add(getBtnOk());
		contentPane.add(getBtnCancel());
		contentPane.add(getScDescription());
		contentPane.add(getTpHouses());
		addRows(chckbxNorth.isSelected(),chckbxCenter.isSelected(),chckbxSouth.isSelected());
	}
	private JLabel getLblEiiRealState() {
		if (lblEiiRealState == null) {
			lblEiiRealState = new JLabel("EII Real State Agency");
			lblEiiRealState.setForeground(Color.ORANGE);
			lblEiiRealState.setFont(new Font("Felix Titling", Font.BOLD, 36));
			lblEiiRealState.setBounds(85, 27, 502, 53);
		}
		return lblEiiRealState;
	}
	private JLabel getLblIcon() {
		if (lblIcon == null) {
			lblIcon = new JLabel("");
			lblIcon.setIcon(new ImageIcon(MainWindow.class.getResource("/img/llave.JPG")));
			lblIcon.setBounds(565, 11, 95, 80);
		}
		return lblIcon;
	}
	private JPanel getPnVisits() {
		if (pnVisits == null) {
			pnVisits = new JPanel();
			pnVisits.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Schedule Visits", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
			pnVisits.setBounds(591, 100, 218, 246);
			pnVisits.setLayout(null);
			pnVisits.add(getScVisits());
			pnVisits.add(getBtnAdd());
			pnVisits.add(getBtnDelete());
		}
		return pnVisits;
	}
	private JScrollPane getScVisits() {
		if (scVisits == null) {
			scVisits = new JScrollPane();
			scVisits.setBounds(10, 21, 189, 173);
			scVisits.setViewportView(getVisitList());
		}
		return scVisits;
	}
	private JList getVisitList() {
		if (visitList == null) {
			listModel = new DefaultListModel();
			visitList = new JList(listModel);
			visitList.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		}
		return visitList;
	}
	private JButton getBtnAdd() {
		if (btnAdd == null) {
			btnAdd = new JButton("Add");
			btnAdd.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					listModel.addElement(getSelectedCode());
				}
			});
			btnAdd.setBounds(10, 205, 89, 23);
		}
		return btnAdd;
	}
	private JButton getBtnDelete() {
		if (btnDelete == null) {
			btnDelete = new JButton("Delete");
			btnDelete.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
				}
			});
			btnDelete.setBounds(110, 205, 89, 23);
		}
		return btnDelete;
	}
	private JPanel getPnPayment() {
		if (pnPayment == null) {
			pnPayment = new JPanel();
			pnPayment.setBorder(new TitledBorder(null, "Entry Payment", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			pnPayment.setBounds(591, 357, 218, 101);
			pnPayment.setLayout(null);
			pnPayment.add(getSpPayment());
			pnPayment.add(getLblPercentaje());
			pnPayment.add(getBtnCalculate());
			pnPayment.add(getTxtPayment());
		}
		return pnPayment;
	}
	private JSpinner getSpPayment() {
		if (spPayment == null) {
			spPayment = new JSpinner();
			spPayment.setModel(new SpinnerNumberModel(15, 0, 100, 1));
			spPayment.setBounds(10, 35, 72, 20);
		}
		return spPayment;
	}
	private JLabel getLblPercentaje() {
		if (lblPercentaje == null) {
			lblPercentaje = new JLabel("%");
			lblPercentaje.setBounds(87, 38, 21, 14);
		}
		return lblPercentaje;
	}
	private JButton getBtnCalculate() {
		if (btnCalculate == null) {
			btnCalculate = new JButton("Calculate");
			btnCalculate.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					int x = tHouses.getSelectedRow();
					if(x != -1)
					{
						float price = (float) tHouses.getValueAt(x, 3);
						Integer percentage = (Integer)spPayment.getValue();
						txtPayment.setText(String.valueOf(agency.calcularEntrada(percentage, price)));
					}
					}
			});
			btnCalculate.setBounds(118, 34, 89, 23);
		}
		return btnCalculate;
	}
	private JTextField getTxtPayment() {
		if (txtPayment == null) {
			txtPayment = new JTextField();
			txtPayment.setEditable(false);
			txtPayment.setBounds(10, 66, 197, 24);
			txtPayment.setColumns(10);
		}
		return txtPayment;
	}
	private JButton getBtnOk() {
		if (btnOk == null) {
			btnOk = new JButton("Ok");
			btnOk.setFont(new Font("Tahoma", Font.PLAIN, 15));
			btnOk.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) 
				{
					StringBuffer sb = new StringBuffer();

					for(Object o : listModel.toArray())
					{
						sb.append(o.toString());
						sb.append("\n");
						
					}
					agency.grabarFichero(sb.toString());
					initialize();
					
				}
			});
			btnOk.setBounds(569, 469, 115, 31);
		}
		return btnOk;
	}
	private JButton getBtnCancel() {
		if (btnCancel == null) {
			btnCancel = new JButton("Cancel");
			btnCancel.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					initialize();
				}

				
			});
			btnCancel.setFont(new Font("Tahoma", Font.PLAIN, 15));
			btnCancel.setBounds(694, 469, 115, 31);
		}
		return btnCancel;
	}
	
	
	private void initialize() 
	{
		tpHouses.setSelectedIndex(0);
		tHouses.clearSelection();
		listModel.clear();
		taDescription.setText("");
		txtPayment.setText("");
		spPayment.setValue(15);
		chckbxCenter.setSelected(true);
		chckbxSouth.setSelected(true);
		chckbxNorth.setSelected(true);
		spHouses.getViewport().setViewPosition(new Point(0,0));
	
		
	}
	
	
	private JScrollPane getScDescription() {
		if (scDescription == null) {
			scDescription = new JScrollPane();
			scDescription.setBounds(27, 357, 519, 143);
			scDescription.setViewportView(getTaDescription());
		}
		return scDescription;
	}
	private JTextArea getTaDescription() {
		if (taDescription == null) {
			taDescription = new JTextArea();
			taDescription.setEditable(false);
			taDescription.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
			taDescription.setWrapStyleWord(true);
			taDescription.setLineWrap(true);
		}
		return taDescription;
	}
	private JTabbedPane getTpHouses() {
		if (tpHouses == null) {
			tpHouses = new JTabbedPane(JTabbedPane.TOP);
			tpHouses.setBounds(27, 100, 519, 246);
			tpHouses.addTab("On Sale", null, getPnOnSale(), null);
			tpHouses.addTab("On Rent", null, getPnOnRenting(), null);
		}
		return tpHouses;
	}
	private JPanel getPnOnSale() {
		if (pnOnSale == null) {
			pnOnSale = new JPanel();
			pnOnSale.setLayout(new BorderLayout(0, 0));
			pnOnSale.add(getPnFilter(), BorderLayout.NORTH);
			pnOnSale.add(getSpHouses(), BorderLayout.CENTER);
		}
		return pnOnSale;
	}
	private JPanel getPnOnRenting() {
		if (pnOnRenting == null) {
			pnOnRenting = new JPanel();
		}
		return pnOnRenting;
	}
	private JPanel getPnFilter() {
		if (pnFilter == null) {
			pnFilter = new JPanel();
			pnFilter.add(getLblSelectFilter());
			pnFilter.add(getChckbxNorth());
			pnFilter.add(getChckbxCenter());
			pnFilter.add(getChckbxSouth());
		}
		return pnFilter;
	}
	private JLabel getLblSelectFilter() {
		if (lblSelectFilter == null) {
			lblSelectFilter = new JLabel("Select filter: ");
		}
		return lblSelectFilter;
	}
	private JCheckBox getChckbxNorth() {
		if (chckbxNorth == null) {
			chckbxNorth = new JCheckBox("North");
			chckbxNorth.setSelected(true);
			chckbxNorth.addChangeListener(new ChangeListener() {
				public void stateChanged(ChangeEvent arg0) 
				{
					showHousesPerArea();
				}
			});
		}
		return chckbxNorth;
	}
	private JCheckBox getChckbxCenter() {
		if (chckbxCenter == null) {
			chckbxCenter = new JCheckBox("Center");
			chckbxCenter.setSelected(true);
			chckbxCenter.addChangeListener(new ChangeListener() {
				public void stateChanged(ChangeEvent e) {
					showHousesPerArea();
				}
				
			});
			
		}
		return chckbxCenter;
	}
	private JCheckBox getChckbxSouth() {
		if (chckbxSouth == null) {
			chckbxSouth = new JCheckBox("South");
			chckbxSouth.setSelected(true);
			chckbxSouth.addChangeListener(new ChangeListener() {
				public void stateChanged(ChangeEvent e) {
					
					showHousesPerArea();
				}
			});
			
		}
		return chckbxSouth;
	}
	private JScrollPane getSpHouses() {
		if (spHouses == null) {
			spHouses = new JScrollPane();
			spHouses.setViewportView(getTHouses());
		}
		return spHouses;
	}
	private JTable getTHouses() {
		if (tHouses == null) {
			String[] columns = {"Code","Area","City","Price"};
			tableModel = new ModeloNoEditable(columns,0);
			tHouses = new JTable(tableModel);
			tHouses.setDefaultRenderer(Object.class, new RendererSubstance());
			tHouses.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			tHouses.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent arg0)
				{
					
					if(arg0.getClickCount()==2)
					{
						JOptionPane.showMessageDialog(contentPane, "double click");
					}
					taDescription.setText(agency.getDescripcionMansion(getSelectedCode()));
				
				}
			});
			tHouses.getTableHeader().setReorderingAllowed(false);
		}
		
		return tHouses;
	}
	
	private void addRows(boolean north, boolean center, boolean south)
	{
		
		Object[] newRow = new Object[4];
		for(Mansion m: agency.getRelacionMansiones())
		{
			String area = m.getZona();
			if ( (area.equals("Norte") && north) || (area.equals("Centro") && center) || (area.equals("Sur") && south) )
			{
				newRow[0] = m.getCodigo();
				newRow[1] = m.getZona();
				newRow[2] = m.getLocalidad();
				newRow[3] = m.getPrecio();
				tableModel.addRow(newRow);
			}
		}
		
	}
	
	
	private void showHousesPerArea()
	{
		tableModel.getDataVector().clear();
		tableModel.fireTableDataChanged();
		addRows(chckbxNorth.isSelected(),chckbxCenter.isSelected(),chckbxSouth.isSelected());
	}
	
	
	private String getSelectedCode()
	{
		int row = tHouses.getSelectedRow();
		if ( row != -1 )
		{
			return (String) tHouses.getValueAt(row, 0);
		}
		return "";
	}
	
	
}
