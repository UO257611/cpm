package igu;

import javax.help.*;
import java.net.*;
import java.io.*;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Toolkit;
import java.util.Properties;

import javax.swing.JLabel;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import java.awt.GridLayout;
import javax.swing.JSlider;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JScrollPane;
import javax.swing.JList;
import javax.swing.border.LineBorder;

import com.jtattoo.plaf.hifi.HiFiLookAndFeel;

import player.MusicPlayer;

import java.awt.Color;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JSeparator;
import java.awt.event.ActionListener;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.awt.event.ActionEvent;
import javax.swing.ListSelectionModel;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.event.ChangeEvent;

public class MainWindow extends JFrame {

	private JPanel contentPane;
	private JPanel pnNorth;
	private JLabel lblLogo;
	private JSlider slVolume;
	private JPanel pnVol;
	private JLabel lblVol;
	private JTextField txtVol;
	private JPanel pnCentre;
	private JPanel pnLibrary;
	private JPanel pnPlaylist;
	private JLabel lblLibrary;
	private JPanel pnLibraryButtons;
	private JButton btnAddToPlaylist;
	private JButton btnDelate;
	private JLabel lblPlaylist;
	private JScrollPane scList1;
	private JList list1;
	private JPanel pnPlayButtons;
	private JButton btnRew;
	private JButton btnPlay;
	private JButton btnFor;
	private JButton btnStop;
	private JButton btnDel;
	private JScrollPane scList2;
	private JList list2;
	private JMenuBar menuBar;
	private JMenu mnFile;
	private JMenu mnPlay;
	private JMenu mnOptions;
	private JMenu mnHelp;
	private JMenuItem mntmOpen;
	private JMenuItem mntmExit;
	private JSeparator separator;
	private JMenuItem mntmContents;
	private JSeparator separator_1;
	private JMenuItem mntmAbout;
	
	private DefaultListModel model1 = null;
	private DefaultListModel model2 = null;
	
	private JFileChooser selector = null;
	
	private MusicPlayer mP= new MusicPlayer();
	private boolean random = false;
	private Font digitalFont;
	private JMenuItem mntmClearAll;
	private JButton btnClear;
	private JButton bnClear2;
	private JMenuItem mntmRandom;
	private JPanel pnSouth;
	private JLabel lblCurrentSong;
	private JLabel lblNameSong;


	
	private void loadFont()
	{
	try
	{
		InputStream myStream = new BufferedInputStream(new FileInputStream("ttf/DS-DIGIB.ttf"));
		digitalFont = Font.createFont(Font.TRUETYPE_FONT, myStream);
	}
	catch (Exception e)
	{
		System.out.println("Font dont found");
	}
}
	
	
	private JFileChooser getSelector() {
		if (selector == null) {
			selector = new JFileChooser();
			selector.setMultiSelectionEnabled(true);
			selector.setFileFilter(new FileNameExtensionFilter("Mp3 Files","mp3"));
			selector.setCurrentDirectory(new File(System.getProperty("user.home")+"/Desktop"));
		}
		
		return selector;
	}
	
	/**
	 * Launch the application.
	 *
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					
					Properties props  = new Properties();
					props.put("logoString", "");
					HiFiLookAndFeel.setCurrentTheme(props);
					UIManager.setLookAndFeel("com.jtattoo.plaf.hifi.HiFiLookAndFeel");
					
					MainWindow frame = new MainWindow();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainWindow() {
		loadFont();
		setIconImage(Toolkit.getDefaultToolkit().getImage(MainWindow.class.getResource("/img/logoTitulo.png")));
		setTitle("EII Music player");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 882, 571);
		setJMenuBar(getMenuBar_1());
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		contentPane.add(getPnNorth(), BorderLayout.NORTH);
		contentPane.add(getPnCentre(), BorderLayout.CENTER);
		contentPane.add(getPnSouth(), BorderLayout.SOUTH);
		loadHelp();
	}

	private JPanel getPnNorth() {
		if (pnNorth == null) {
			pnNorth = new JPanel();
			pnNorth.setLayout(new GridLayout(1, 0, 0, 0));
			pnNorth.add(getLblLogo());
			pnNorth.add(getSlVolume());
			pnNorth.add(getPnVol());
		}
		return pnNorth;
	}
	private JLabel getLblLogo() {
		if (lblLogo == null) {
			lblLogo = new JLabel("");
			lblLogo.setIcon(new ImageIcon(MainWindow.class.getResource("/img/logo.png")));
		}
		return lblLogo;
	}
	private JSlider getSlVolume() {
		if (slVolume == null) {
			slVolume = new JSlider();
			slVolume.addChangeListener(new ChangeListener() {
				public void stateChanged(ChangeEvent arg0) 
				{
					txtVol.setText(String.valueOf(slVolume.getValue()));
					mP.setVolume(slVolume.getValue(), slVolume.getMaximum());
				}
			});
			slVolume.setPaintTicks(true);
			slVolume.setPaintLabels(true);
			slVolume.setMinorTickSpacing(10);
			slVolume.setMajorTickSpacing(20);
			//slVolume.setFont(new Font("Dialog", Font.BOLD, 14));
			slVolume.setFont(digitalFont.deriveFont(Font.PLAIN, 20));
			
		}
		return slVolume;
	}
	private JPanel getPnVol() {
		if (pnVol == null) {
			pnVol = new JPanel();
			pnVol.add(getLblVol());
			pnVol.add(getTxtVol());
		}
		return pnVol;
	}
	private JLabel getLblVol() {
		if (lblVol == null) {
			lblVol = new JLabel("Vol :");
			lblVol.setFont(new Font("Dialog", Font.BOLD, 26));
		}
		return lblVol;
	}
	private JTextField getTxtVol() {
		if (txtVol == null) {
			txtVol = new JTextField();
			txtVol.setEditable(false);
			txtVol.setHorizontalAlignment(SwingConstants.CENTER);
			//txtVol.setFont(new Font("Dialog", Font.BOLD, 40));
			txtVol.setFont(digitalFont.deriveFont(Font.PLAIN, 40));
			txtVol.setText("50");
			txtVol.setColumns(3);
		}
		return txtVol;
	}
	private JPanel getPnCentre() {
		if (pnCentre == null) {
			pnCentre = new JPanel();
			pnCentre.setLayout(new GridLayout(1, 0, 0, 0));
			pnCentre.add(getPnLibrary());
			pnCentre.add(getPanel_1());
		}
		return pnCentre;
	}
	private JPanel getPnLibrary() {
		if (pnLibrary == null) {
			pnLibrary = new JPanel();
			pnLibrary.setLayout(new BorderLayout(0, 0));
			pnLibrary.add(getLblLibrary(), BorderLayout.NORTH);
			pnLibrary.add(getPanel_2(), BorderLayout.SOUTH);
			pnLibrary.add(getScList1(), BorderLayout.CENTER);
		}
		return pnLibrary;
	}
	private JPanel getPanel_1() {
		if (pnPlaylist == null) {
			pnPlaylist = new JPanel();
			pnPlaylist.setLayout(new BorderLayout(0, 0));
			pnPlaylist.add(getLblPlaylist(), BorderLayout.NORTH);
			pnPlaylist.add(getPanel_3(), BorderLayout.SOUTH);
			pnPlaylist.add(getScList2(), BorderLayout.CENTER);
		}
		return pnPlaylist;
	}
	private JLabel getLblLibrary() {
		if (lblLibrary == null) {
			lblLibrary = new JLabel("\u266A Library :");
			lblLibrary.setFont(new Font("Dialog", Font.BOLD, 16));
		}
		return lblLibrary;
	}
	private JPanel getPanel_2() {
		if (pnLibraryButtons == null) {
			pnLibraryButtons = new JPanel();
			pnLibraryButtons.setLayout(new GridLayout(1, 0, 0, 0));
			pnLibraryButtons.add(getBtnClear());
			pnLibraryButtons.add(getBtnAddToPlaylist());
			pnLibraryButtons.add(getBtnDelate());
		}
		return pnLibraryButtons;
	}
	private JButton getBtnAddToPlaylist() {
		if (btnAddToPlaylist == null) {
			btnAddToPlaylist = new JButton("Add to Playlist");
			btnAddToPlaylist.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) 
				{
					for(Object o: list1.getSelectedValuesList())
					{
						model2.addElement(o);
						
					}
				}
			});
			btnAddToPlaylist.setFont(new Font("Dialog", Font.BOLD, 16));
		}
		return btnAddToPlaylist;
	}
	private JButton getBtnDelate() {
		if (btnDelate == null) {
			btnDelate = new JButton("Delate");
			btnDelate.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) 
				{
					int[] items = list1.getSelectedIndices();
					for(int i = 0; i<items.length; i++) 
					{
						model1.remove(items[i]-i);
					}
					
				}
			});
			btnDelate.setFont(new Font("Dialog", Font.BOLD, 16));
		}
		return btnDelate;
	}
	private JLabel getLblPlaylist() {
		if (lblPlaylist == null) {
			lblPlaylist = new JLabel("\u266A Playlist :");
			lblPlaylist.setFont(new Font("Dialog", Font.BOLD, 16));
		}
		return lblPlaylist;
	}
	private JScrollPane getScList1() {
		if (scList1 == null) {
			scList1 = new JScrollPane();
			scList1.setBorder(new LineBorder(new Color(0, 255, 0), 3));
			scList1.setViewportView(getList1());
		}
		return scList1;
	}
	private JList getList1() {
		if (list1 == null) {
			model1 = new DefaultListModel();
			list1 = new JList(model1);
			list1.setFont(new Font("Dialog", Font.BOLD, 16));
		}
		return list1;
	}
	private JPanel getPanel_3() {
		if (pnPlayButtons == null) {
			pnPlayButtons = new JPanel();
			pnPlayButtons.setLayout(new GridLayout(1, 0, 0, 0));
			pnPlayButtons.add(getBnClear2());
			pnPlayButtons.add(getBtnRew());
			pnPlayButtons.add(getBtnPlay());
			pnPlayButtons.add(getBtnFor());
			pnPlayButtons.add(getBtnStop());
			pnPlayButtons.add(getBtnDel());
		}
		return pnPlayButtons;
	}
	private JButton getBtnRew() {
		if (btnRew == null) {
			btnRew = new JButton("\u25C4\u25C4");
			btnRew.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) 
				{
					int index = list2.getSelectedIndex();
					if(index > 0 ) 
					{
						list2.setSelectedIndex(index-1);
					}
					mP.play(((MyFile) list2.getSelectedValue()).getF());
					writeName();
					
				}
			});
			btnRew.setFont(new Font("Dialog", Font.BOLD, 16));
		}
		return btnRew;
	}
	private JButton getBtnPlay() {
		if (btnPlay == null) {
			btnPlay = new JButton("\u25BA");
			btnPlay.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) 
				{
					mP.play(((MyFile) list2.getSelectedValue()).getF());
					writeName();
				}
			});
			btnPlay.setFont(new Font("Dialog", Font.BOLD, 16));
		}
		return btnPlay;
	}
	
	private void writeName()
	{
		getLblNameSong().setText(((MyFile)list2.getSelectedValue()).toString());
	}
	
	
	private JButton getBtnFor() {
		if (btnFor == null) {
			btnFor = new JButton("\u25BA\u25BA");
			btnFor.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) 
				{
					int index = list2.getSelectedIndex();
					
					
						if(index < list2.getLastVisibleIndex()) 
						{
							list2.setSelectedIndex(index+1);
						}
						
					
					mP.play(((MyFile) list2.getSelectedValue()).getF());
					writeName();
				}
			});
			btnFor.setFont(new Font("Dialog", Font.BOLD, 16));
		}
		return btnFor;
	}
	

	private JButton getBtnStop() {
		if (btnStop == null) {
			btnStop = new JButton("\u25A0");
			btnStop.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) 
				{
					mP.stop();
					getLblNameSong().setText("");
				}
			});
			btnStop.setFont(new Font("Dialog", Font.BOLD, 16));
		}
		return btnStop;
	}
	private JButton getBtnDel() {
		if (btnDel == null) {
			btnDel = new JButton("Del");
			btnDel.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) 
				{

					int[] items = list2.getSelectedIndices();
					for(int i = 0; i<items.length; i++) 
					{
						model2.remove(items[i]-i);
					}
				}
			});
			btnDel.setFont(new Font("Dialog", Font.BOLD, 16));
		}
		return btnDel;
	}
	private JScrollPane getScList2() {
		if (scList2 == null) {
			scList2 = new JScrollPane();
			scList2.setBorder(new LineBorder(Color.GREEN, 3));
			scList2.setViewportView(getList2());
		}
		return scList2;
	}
	private JList getList2() {
		if (list2 == null) {
			model2 = new DefaultListModel();
			list2 = new JList(model2);
			list2.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			list2.setFont(new Font("Dialog", Font.BOLD, 16));
		}
		return list2;
	}
	private JMenuBar getMenuBar_1() {
		if (menuBar == null) {
			menuBar = new JMenuBar();
			menuBar.add(getMnFile());
			menuBar.add(getMnPlay());
			menuBar.add(getMnOptions());
			menuBar.add(getMnHelp());
		}
		return menuBar;
	}
	private JMenu getMnFile() {
		if (mnFile == null) {
			mnFile = new JMenu("File");
			mnFile.setMnemonic('f');
			mnFile.add(getMntmOpen());
			mnFile.add(getSeparator());
			mnFile.add(getMntmExit());
		}
		return mnFile;
	}
	private JMenu getMnPlay() {
		if (mnPlay == null) {
			mnPlay = new JMenu("Play");
			mnPlay.setMnemonic('l');
			mnPlay.add(getMntmRandom());
		}
		return mnPlay;
	}
	private JMenu getMnOptions() {
		if (mnOptions == null) {
			mnOptions = new JMenu("Options");
			mnOptions.setMnemonic('t');
		}
		return mnOptions;
	}
	private JMenu getMnHelp() {
		if (mnHelp == null) {
			mnHelp = new JMenu("Help");
			mnHelp.setMnemonic('h');
			mnHelp.add(getMntmContents());
			mnHelp.add(getSeparator_1());
			mnHelp.add(getMntmAbout());
		}
		return mnHelp;
	}
	private JMenuItem getMntmOpen() {
		if (mntmOpen == null) {
			mntmOpen = new JMenuItem("Open ...");
			mntmOpen.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					int response = getSelector().showOpenDialog(null);
					
					if ( response == JFileChooser.APPROVE_OPTION) {
						for( File f : getSelector().getSelectedFiles()) {
							model1.addElement(new MyFile(f));
						}
					}
				}
			});
			mntmOpen.setMnemonic('p');
		}
		return mntmOpen;
	}
	private JMenuItem getMntmExit() {
		if (mntmExit == null) {
			mntmExit = new JMenuItem("Exit");
			mntmExit.setMnemonic('x');
		}
		return mntmExit;
	}
	private JSeparator getSeparator() {
		if (separator == null) {
			separator = new JSeparator();
		}
		return separator;
	}
	private JMenuItem getMntmContents() {
		if (mntmContents == null) {
			mntmContents = new JMenuItem("Contents");
			mntmContents.setMnemonic('n');
		}
		return mntmContents;
	}
	private JSeparator getSeparator_1() {
		if (separator_1 == null) {
			separator_1 = new JSeparator();
		}
		return separator_1;
	}
	private JMenuItem getMntmAbout() {
		if (mntmAbout == null) {
			mntmAbout = new JMenuItem("About");
			mntmAbout.setMnemonic('b');
		}
		return mntmAbout;
	}
	private JButton getBtnClear() {
		if (btnClear == null) {
			btnClear = new JButton("Clear Library");
			btnClear.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) 
				{
					model1.clear();
				}
			});
			btnClear.setFont(new Font("Dialog", Font.BOLD, 16));
		}
		return btnClear;
	}
	private JButton getBnClear2() {
		if (bnClear2 == null) {
			bnClear2 = new JButton("Clear Library");
			bnClear2.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) 
				{
					model2.clear();
				}
			});
			bnClear2.setFont(new Font("Dialog", Font.BOLD, 16));
		}
		return bnClear2;
	}
	private JMenuItem getMntmRandom() {
		if (mntmRandom == null) {
			mntmRandom = new JMenuItem("Random");
			mntmRandom.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) 
				{
					int newIndex= list2.getSelectedIndex();
					while(newIndex == list2.getSelectedIndex())
					{
						newIndex = (int) (Math.random()*(model2.size()));
					}
					list2.setSelectedIndex(newIndex);
					mP.play(((MyFile)list2.getSelectedValue()).getF());
				}		
			});
		}
		return mntmRandom;
	}
	private JPanel getPnSouth() {
		if (pnSouth == null) {
			pnSouth = new JPanel();
			pnSouth.add(getLblCurrentSong());
			pnSouth.add(getLblNameSong());
		}
		return pnSouth;
	}
	private JLabel getLblCurrentSong() {
		if (lblCurrentSong == null) {
			lblCurrentSong = new JLabel("Current Song: ");
			lblCurrentSong.setFont(digitalFont.deriveFont(Font.PLAIN, 20));
		}
		return lblCurrentSong;
	}
	private JLabel getLblNameSong() {
		if (lblNameSong == null) {
			lblNameSong = new JLabel("");
			lblNameSong.setFont(digitalFont.deriveFont(Font.PLAIN, 20));
			
		}
		return lblNameSong;
	}
	
	
	private void loadHelp(){

		   URL hsURL;
		   HelpSet hs;

		    try {
			    	File fichero = new File("help/help.hs");
			    	hsURL = fichero.toURI().toURL();
			        hs = new HelpSet(null, hsURL);
			      }

		    catch (Exception e){
		      System.out.println("Ayuda no encontrada");
		      return;
		   }

		   HelpBroker hb = hs.createHelpBroker();

		   hb.enableHelpKey(getRootPane(),"intro", hs);
		   hb.enableHelpOnButton(getMntmContents(), "intro", hs);
		   hb.enableHelp(getList1(), "add", hs);
		   hb.enableHelp(getSlVolume(), "increase", hs);
		 }

	
	
	
}
	