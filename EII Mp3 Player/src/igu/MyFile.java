package igu;

import java.io.File;

public class MyFile 
{
	private File f;
	
	public MyFile(File file)
	{
		f= file;
	}
	
	public String toString()
	{
		String x =  f.getName();
		String[] name = x.split(".mp3");
		return name[0];
	}
	
	public File getF()
	{
		return f;
	}
	
	
	
	
}
